# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from . import core, parse, communicator, timer

import signal, sys

# Define the signal handler function
def signal_handler(sig, frame):
	print("Program interrupted with CTRL+C.")
	# Perform any necessary actions to handle the situation
	communicator.info("== Notochord main terminating..")
	core.end()
	communicator.info("== Notochord main end ==")
	sys.exit(0)

# Assign the signal handler to SIGINT
signal.signal(signal.SIGINT, signal_handler)

# The rest of the code here
c = parse.parse_config()

core.init()
core.setup(c)
communicator.info("== Notochord main init ==")

s = timer.Node_Scheduler()
if c.scan:
	s.scan()
else:
	h = parse.parse_hierarchy_xml(c.xml.filename, s)

core.start(s)

try:
	# Loop until user inputs "q"
	inpt_str = ""
	while inpt_str != "q":
		inpt_str = input("Write q to stop...")
except KeyboardInterrupt:
	# If a KeyboardInterrupt exception is caught, it means CTRL+C was
	# pressed, so the signal handler function is executed
	signal_handler(signal.SIGINT, None)

# The program ends normally if "q" was received
communicator.info("== Notochord main terminating..")
core.end()
communicator.info("== Notochord main end ==")
