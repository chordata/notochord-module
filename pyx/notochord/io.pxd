# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp.memory cimport shared_ptr
from libcpp cimport bool 
from libc.stdint cimport (uint8_t, uint16_t, uint32_t, uint64_t,
							int8_t, int16_t, int32_t, int64_t)

from .error cimport raise_py_error

cdef extern from "io.h" namespace "Chordata":
	cdef cppclass IO_Base:
		void write_byte(uint8_t address, uint8_t sub_address, uint8_t data) except +raise_py_error

		void write_byte(uint8_t address, uint8_t data) except +raise_py_error

		int32_t read_byte(uint8_t address, uint8_t sub_address) except +raise_py_error

		int32_t read_byte(uint8_t address) except +raise_py_error

		vector[uint8_t] read_bytes(uint8_t address, uint8_t sub_address, uint8_t count) except +raise_py_error

		string get_adapter_str() const
	
	cdef cppclass I2C_io(IO_Base):
		pass

	cdef struct I2C_read:
		uint8_t address
		uint8_t sub_address
		uint8_t count

	cdef struct I2C_write:
		uint8_t address
		uint8_t sub_address
		vector[uint8_t] payload

	cdef cppclass Mock_I2C_io(IO_Base):
		vector[I2C_read] reads
		vector[I2C_write] writes
		void reset()


ctypedef shared_ptr[IO_Base] io_ptr
ctypedef shared_ptr[Mock_I2C_io] mock_ptr

cdef class I2C:
	cdef io_ptr c_ptr
	
