# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 


from libc.stdint cimport (uint8_t, uint16_t, uint32_t, uint64_t,
						  int8_t, int16_t, int32_t, int64_t)

from libcpp.memory cimport shared_ptr
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp cimport bool

from .error cimport raise_py_error

from .node cimport _Node, Node, _Node_ptr, KCeptor

cdef extern from "timer.h" namespace 'Chordata':
	cdef cppclass _Timekeeper:
		_Timekeeper(const string _label) except +raise_py_error
		void reset()
		const string get_label()
		uint64_t micros() const
		uint64_t millis() const
		double seconds() const
		uint64_t total_micros() const
		uint64_t total_millis() const
		double total_seconds() const
		const char *initial_time() const
		const char *now() const
	
	cdef cppclass _Node_Scheduler:
		_Node_Scheduler()
		int get_ODR_modifiers()
		int length()
		void add_node(_Node_ptr n) except +raise_py_error
		uint64_t bang(uint64_t wait) 
		void handlers()
		void stop_sensors()
	
	cdef cppclass _Timer:
		_Timer(shared_ptr[_Node_Scheduler] actuator)

		bool start() except +raise_py_error
		void terminate()

ctypedef shared_ptr[_Timekeeper] timek_ptr
ctypedef shared_ptr[_Node_Scheduler] nodesch_ptr
ctypedef shared_ptr[_Timer] timer_ptr


cdef class Timekeeper:
	cdef timek_ptr c_ptr

cdef class Node_Scheduler:
	cdef nodesch_ptr c_ptr
	cpdef add_node(self, Node)
	cpdef bang(self, uint64_t)	

cdef class Timer:
	cdef timer_ptr c_ptr