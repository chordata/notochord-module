# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from libcpp.memory cimport shared_ptr
from libc.stdint cimport uint8_t
from libcpp.string cimport string
from libcpp cimport bool
from libcpp.unordered_map cimport unordered_map
from .node cimport Link, _Hierarchy
from .math._types cimport _Quaternion, Quaternion, quat_ptr, Matrix4, _Matrix4, matrix4_ptr, _Vector3, Vector


cdef extern from "armature.h" namespace "Chordata":
    cdef cppclass _Bone(Link):
        _Bone(shared_ptr[_Bone] _parent, string _label, matrix4_ptr _local_transform, matrix4_ptr _global_transform, uint8_t max_childs)

        # ==================== Local Space ====================
        void set_local_transform(_Matrix4 _local_transform)
        void set_local_translation(_Vector3 _local_translation)
        void translate_local(_Vector3 _translation)   
        void set_local_rotation(_Quaternion _rotation)     
        void rotate_local(_Quaternion _rotation)

        # ==================== Global Space ====================
        void set_global_transform(_Matrix4 _global_transform)
        void set_global_translation(_Vector3 _global_translation)
        void translate_global(_Vector3 _translation)
        void set_global_rotation(_Quaternion _rotation)
        void rotate_global(_Quaternion _rotation)

        _Matrix4 get_local_transform()
        _Vector3 get_local_translation()
        _Quaternion get_local_rotation()
        _Quaternion get_global_rotation()

        void update()
        void add_child(shared_ptr[_Bone] _child)

        void set_pre_quaternion(_Quaternion)
        void set_post_quaternion(_Quaternion)

        string get_label()
    
    cdef cppclass _Armature(_Hierarchy):
        _Armature(string _label, shared_ptr[_Bone] _root)

        string get_label()
        void update(_Bone* _root = NULL)
        const unordered_map[string, shared_ptr[_Bone]]& get_bones()
        


ctypedef shared_ptr[_Bone] bone_ptr
ctypedef shared_ptr[_Armature] armature_ptr
        
    
cdef class Bone:
    cdef bone_ptr c_ptr
    cdef Matrix4 local_transform
    cdef Matrix4 global_transform
    cdef Bone parent

cdef class Armature:
    cdef armature_ptr c_ptr
    cdef Bone root
    cdef gltf 
    cdef bones