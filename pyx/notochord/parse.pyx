# -- START OF COPYRIGHT & LICENSE NOTICES --
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can
# redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES --

from . import messages
from . import defaults
from .core import use_armature as core_use_armature
from .error import Parse_Error
from .node import KCeptor, KCeptorPP, Mux, Hierarchy
from .types import (Mux_Channel, Accel_Scale_Options,
			Gyro_Scale_Options, Mag_Scale_Options,
			Output_Sink, Output_Data_Rate, Kc_Values)
from .config cimport Notochord_Config
from .armature cimport Armature
import argparse, lxml
from lxml import etree
from os import path
import sys, tempfile

class RaiseArgumentParser(argparse.ArgumentParser):
	def error(self, message):
		self.print_help(sys.stderr)
		raise Parse_Error('%s: error: %s\n' % (self.prog, message))

def to_Output_Sink(arg):
	option = 0
	if isinstance(arg, str):
		if "stdout" in arg:
			option |= Output_Sink.STDOUT
		if "stderr" in arg:
			option |= Output_Sink.STDERR
		if "file" in arg:
			option |= Output_Sink.FILE
		if "osc" in arg:
			option |= Output_Sink.OSC
		if "python" in arg:
			option |= Output_Sink.PYTHON
		if "websocket" in arg:
			option |= Output_Sink.WEBSOCKET
	else:
		option = arg
	
	return Output_Sink(option)

def verbosity_validator(string):
	verbose_min = 0
	verbose_max = 4
	val = int(string)
	if val >= verbose_min and val <= verbose_max:
		return val
	else:
		raise ValueError("The requested verbose level ({}) is out of range".format(val))

def odr_validator(string):
	val = int(string)
	if val == 50 or val == 119 or val == 238:
		return val
	else:
		raise ValueError("ODR has to be on of these values: 50, 119, 238. {} given".format(val))
	pass

def create_parser():
	parser = RaiseArgumentParser(epilog=messages.NOTE, description=messages.DESCRIPTION)

	parser.add_argument("ip", help=messages.IP,type=str, nargs='?', default="127.0.0.1")
	parser.add_argument("port", help=messages.PORT,type=int, nargs='?', default=6565)

	parser.add_argument("-V", "--version", help=messages.VERSION, action='store_true')
	parser.add_argument("--credits", help=messages.CREDITS, action='store_true')
	parser.add_argument("-y", "--yes", help=messages.WAIT, action='store_true')
	parser.add_argument("-r", "--raw", help=messages.RAW, action='store_true')

	parser.add_argument("--madgwick", help=messages.MADGWICK, action='store_true')
	parser.add_argument("--live_mag_correction", help=messages.LIVE_MAG_CORRECTION, action='store_true')
	parser.add_argument("--scan", help=messages.SCAN, action='store_true')
	parser.add_argument("-x", "--calibrate", "--calib", help=messages.CALIB, action='store_true')

	parser.add_argument("--odr", help=messages.ODR,type=odr_validator)

	parser.add_argument("-c","--config", help=messages.XML_FILENAME,type=str)
	parser.add_argument("--schema", help=messages.XML_SCHEMA,type=str)

	parser.add_argument("-l","--log", help=messages.LOG,type=to_Output_Sink)
	parser.add_argument("-e","--errors", help=messages.ERROR,type=to_Output_Sink)
	parser.add_argument("-t","--transmit", help=messages.TRANSMIT,type=to_Output_Sink)

	parser.add_argument("--no_bundles", help=messages.NO_BUNDLES, action='store_true')

	parser.add_argument("-a","--i2c_adapter", help=messages.I2C,type=str)
	parser.add_argument("-f","--log_file", help=messages.FILENAME,type=str)

	parser.add_argument("-v","--verbose", help=messages.VERBOSE,type=verbosity_validator)
	parser.add_argument("-s","--send_rate", help=messages.SEND_RATE,type=float)

	parser.add_argument("-b", "--base_osc", help=messages.OSC_BASE,type=str)
	parser.add_argument("--error_osc", help=messages.OSC_ERR,type=str)
	parser.add_argument("--comm_osc", help=messages.OSC_COMM,type=str)

	parser.add_argument("--websocket_port", help=messages.WEBSOCKET_PORT, type=int)


	return parser


def parse_cmd_line(argv = None):
	parser = create_parser()
	args = parser.parse_args(argv)
	return args

def read_xml(str xml_path):
	if path.isabs(xml_path):
		the_file = xml_path
	else:
		the_file = path.realpath(xml_path)
	with open(the_file) as f:
		return f.read()

def get_xml(str xml_path):
	return lxml.etree.fromstring(read_xml(xml_path))

def get_xml_schema(str schema_path = None):
	if schema_path is None:
		schema_path = path.join(path.dirname(path.realpath(__file__)), "Chordata.xsd")
	schema_xml = lxml.etree.fromstring(read_xml(schema_path))
	return lxml.etree.XMLSchema(schema_xml)

def validate_xml(xml, str schema_path = None):
	schema = get_xml_schema(schema_path)
	return schema.validate(xml), schema.error_log

def recursive_kceptor(list_kceptor, parent_xml, parent_kc, s, kc_revision):
	for child in parent_xml:
		if kc_revision == Kc_Values.Two:
			kc = KCeptor(parent_kc, int(child.text.strip()), child.get("Name"))
		else:
			kc = KCeptorPP(parent_kc, int(child.text.strip(), 16), child.get("Name"))

		s.add_node(kc)
		list_kceptor.append(kc)
		recursive_kceptor(list_kceptor, child, kc, s, kc_revision)

	return list_kceptor

def parse_hierarchy_xml(xml, s):
	if isinstance(xml, str):
		xml = get_xml(xml)

	if not isinstance(xml, lxml.etree._Element):
		raise Parse_Error("lxml.etree._Element or filepath as str required to parse hierarchy")

	#parse nodes
	hierarchy = xml.find("Hierarchy")
	if hierarchy is None:
		hierarchy = xml.find("hierarchy")

	mux = None
	kc = None
	list_kceptor = []
	# Get KC_revision text
	kc_revision = xml.find(".//KC_revision")
	if kc_revision is None:
		kc_revision = xml.find(".//kc_revision")
	kc_revision = kc_revision.text.strip()

	for child in hierarchy:
		if child.tag == "mux" or child.tag == "Mux":
			if child.text.strip() == "0x73":
				kc_revision = Kc_Values.Two
			elif child.text.strip() == "0x70":
				kc_revision = Kc_Values.PlusPlus

			if kc_revision == Kc_Values.Two:
				mux = Mux(None, 0x73, child.get("Name"))
			else:
				mux = Mux(None, 0x70, child.get("Name"))
			n = 1
			for branch_xml in child:
				branch = mux.get_branch(Mux_Channel[branch_xml.text.strip()])
				list_kceptor.append(recursive_kceptor([], branch_xml, branch, s, kc_revision))
				s.add_node(branch)
				n += 1
		elif child.tag == "K_Ceptor" or child.tag == "k_ceptor":
			if kc_revision == Kc_Values.Two:
				kc = KCeptor(None, int(child.text.strip()), child.get("Name"))
			else:
				kc = KCeptorPP(None, int(child.text.strip(), 16), child.get("Name"))

			list_kceptor.append(recursive_kceptor([], child, kc, s, kc_revision))
			s.add_node(kc)

	#build hierarchy of nodes
	if mux is not None:
		h = Hierarchy(mux)
	else:
		h = Hierarchy(kc)

	return h

def parse_config(argv = None):
	c = Notochord_Config()

	args = parse_cmd_line(argv)

	if args.config is not None:
		if path.isabs(args.config):
			xml_path = args.config
		else:
			xml_path = path.realpath(path.join(path.dirname(__file__), "../" + path.basename(args.config)))
	else:
		xml_path = c.xml.filename


	xml = get_xml(xml_path)
	c.xml.filename = xml_path

	if args.schema is not None:
		if path.isabs(args.schema):
			xsd_path = args.schema
		else:
			xsd_path = path.realpath(path.join(path.dirname(__file__), "../" + path.basename(args.schema)))
	else:
		xsd_path = c.xml.schema

	c.xml.schema = xsd_path
	schema = get_xml_schema(xsd_path)
	try:
		schema.assertValid(xml)
	except lxml.etree.DocumentInvalid as e:
		raise Parse_Error(e)

	version = tuple(map(int, xml.attrib.get("version").split(".")))
	if version < (1,0,0):
		raise Parse_Error("Configuration XML Version must be at least 1.0.0, found {}".format(version))
	parse(xml, c)

	# parse rest of command line
	if args.ip is not None:
		c.comm.ip = args.ip
	if args.port is not None:
		c.comm.port = args.port
	if args.version:
		# Display program's version and exit
		print(c.version)
		exit()
	if args.credits:
		# Information about people contributing to this program, libraries, etc
		pass
	if args.yes: # no_wait
		# Disable confirmation to start the program. By default Notochord will wait for a confirmation from the user.
		c.wait = False
	if args.raw:
		# Transmit the raw lectures fron the K-Ceptors
		c.raw = True
	if args.madgwick:
		# Use Madgwick's sensor fusion algorithm (legacy)
		raise Parse_Error("The legacy madgwick fusion algorithm is not implemented currently")
	if args.live_mag_correction:
		# Enable live magnetic correction in Kalman filter
		c.sensor.live_mag_correction = args.live_mag_correction
	if args.scan:
		# Scan the i2c port to create the Hierarchy, instead of reading the configuration file
		c.scan = True
	if args.calibrate:
		# Sensor calibration procedure. Notochord will perform a --scan, and calibrate the found K-Ceptor. It is adviced to plug just one KC to the Hub with this flag!!
		c.calib = True
	if args.odr is not None:
		# Read sensors at this frequency (default=50Hz, max=100Hz)
		if  50 == args.odr:
			c.sensor.output_rate = Output_Data_Rate.OUTPUT_RATE_50
		if 119 == args.odr:
			c.sensor.output_rate = Output_Data_Rate.OUTPUT_RATE_119
		if 238 == args.odr:
			c.sensor.output_rate = Output_Data_Rate.OUTPUT_RATE_238
	if args.log is not None:
		c.comm.msg = args.log
	if args.errors is not None:
		c.comm.error = args.errors
	if args.transmit is not None:
		c.comm.transmit = args.transmit
	if args.no_bundles:
		# Don't use COPP bundles (send plain OSC messages)
		c.comm.use_bundles = False
	if args.i2c_adapter is not None:
		c.comm.adapter = args.i2c_adapter
	if args.log_file is not None:
		c.comm.filepath = args.log_file
	if args.verbose is not None:
		c.comm.log_level = args.verbose
	if args.send_rate is not None:
		c.comm.send_rate = args.send_rate
	if args.base_osc is not None:
		c.osc.base_address = args.base_osc
		c.osc.error_address = c.osc.base_address + "/error"
		c.osc.msg_address = c.osc.base_address + "/comm"
	if args.error_osc is not None:
		c.osc.error_address = c.osc.base_address + args.error_osc
	if args.comm_osc is not None:
		c.osc.msg_address = c.osc.base_address + args.comm_osc
	if args.websocket_port is not None:
		c.comm.websocket_port = int(args.websocket_port)

	return c

# parsing
cpdef parse(xml, Notochord_Config conf):
	config = xml.find("Configuration")
	if config is None:
		config = xml.find("configuration")
	kc_revision = config.find(".//KC_revision")
	if kc_revision is None:
		kc_revision = config.find(".//kc_revision")
	if kc_revision is not None:
		kc = kc_revision.text.strip()
		if kc == '2':
			conf.kc_revision = Kc_Values.Two
		elif kc == '++':
			conf.kc_revision = Kc_Values.PlusPlus

	ip = config.find(".//Ip")
	if ip is None:
		ip = config.find(".//ip")
	if ip is not None:
		conf.comm.ip = ip.text.strip()

	port = config.find(".//Port")
	if port is None:
		port = config.find(".//port")
	if port is not None:
		conf.comm.port = int(port.text.strip())

	adapter = config.find(".//Adapter")
	if adapter is None:
		adapter = config.find(".//adapter")
	if adapter is not None:
		conf.comm.adapter = adapter.text.strip()

	send_rate = config.find(".//Send_rate")
	if send_rate is None:
		send_rate = config.find(".//send_rate")
	if send_rate is not None:
		conf.comm.send_rate = float(send_rate.text.strip())

	transmit = config.find(".//Transmit")
	if transmit is None:
		transmit = config.find(".//transmit")
	if transmit is not None:
		transmit = transmit.text.strip()
		if "stdout" in transmit.lower():
			conf.comm.transmit |= Output_Sink.STDOUT
		if "stderr" in transmit.lower():
			conf.comm.transmit |= Output_Sink.STDERR
		if "file" in transmit.lower():
			conf.comm.transmit |= Output_Sink.FILE
		if "osc" in transmit.lower():
			conf.comm.transmit |= Output_Sink.OSC
		if "python" in transmit.lower():
			conf.comm.transmit |= Output_Sink.PYTHON
		if "websocket" in transmit.lower():
			conf.com.transmit |= Output_Sink.WEBSOCKET
	else:
		conf.comm.transmit = Output_Sink.NONE

	error = config.find(".//Error")
	if error is None:
		error = config.find(".//error")
	if error is not None:
		error = error.text.strip()
		if "stdout" in error.lower():
			conf.comm.error |= Output_Sink.STDOUT
		if "stderr" in error.lower():
			conf.comm.error |= Output_Sink.STDERR
		if "file" in error.lower():
			conf.comm.error |= Output_Sink.FILE
		if "osc" in error.lower():
			conf.comm.error |= Output_Sink.OSC
		if "python" in error.lower():
			conf.comm.error |= Output_Sink.PYTHON
		if "websocket" in error.lower():
			conf.com.error |= Output_Sink.WEBSOCKET
	else:
		conf.comm.error = Output_Sink.NONE

	msg = config.find(".//Msg")
	if msg is None:
		msg = config.find(".//msg")
	if msg is None:
		msg = config.find(".//log")
	if msg is None:
		msg = config.find(".//Log")
	if msg is not None:
		msg = msg.text.strip()
		if "stdout" in msg.lower():
			conf.comm.msg |= Output_Sink.STDOUT
		if "stderr" in msg.lower():
			conf.comm.msg |= Output_Sink.STDERR
		if "file" in msg.lower():
			conf.comm.msg |= Output_Sink.FILE
		if "osc" in msg.lower():
			conf.comm.msg |= Output_Sink.OSC
		if "python" in msg.lower():
			conf.comm.msg |= Output_Sink.PYTHON
		if "websocket" in msg.lower():
			conf.com.msg |= Output_Sink.WEBSOCKET
	else:
		conf.comm.msg = Output_Sink.NONE

	gyro_scale = config.find(".//Gyro_scale")
	if gyro_scale is None:
		gyro_scale = config.find(".//gyro_scale")
	if gyro_scale is not None:
		gyro_scale = gyro_scale.text.strip()
		if "245" == gyro_scale:
			conf.sensor.gyro_scale = Gyro_Scale_Options.gyro_245
		if "500" == gyro_scale:
			conf.sensor.gyro_scale = Gyro_Scale_Options.gyro_500
		if "2000" == gyro_scale:
			conf.sensor.gyro_scale = Gyro_Scale_Options.gyro_2000

	accel_scale = config.find(".//Accel_scale")
	if accel_scale is None:
		accel_scale = config.find(".//accel_scale")
	if accel_scale is not None:
		accel_scale = accel_scale.text.strip()
		if "2" == accel_scale:
			conf.sensor.accel_scale = Accel_Scale_Options.accel_2
		if "4" == accel_scale:
			conf.sensor.accel_scale = Accel_Scale_Options.accel_4
		if "8" == accel_scale:
			conf.sensor.accel_scale = Accel_Scale_Options.accel_8
		if "16" == accel_scale:
			conf.sensor.accel_scale = Accel_Scale_Options.accel_16

	mag_scale = config.find(".//Mag_scale")
	if mag_scale is None:
		mag_scale = config.find(".//mag_scale")
	if mag_scale is not None:
		mag_scale = mag_scale.text.strip()
		if "4" == mag_scale:
			conf.sensor.mag_scale = Mag_Scale_Options.mag_4
		if "8" == mag_scale:
			conf.sensor.mag_scale = Mag_Scale_Options.mag_8
		if "12" == mag_scale:
			conf.sensor.mag_scale = Mag_Scale_Options.mag_12
		if "16" == mag_scale:
			conf.sensor.mag_scale = Mag_Scale_Options.mag_16

	data_rate = config.find(".//Output_Data_Rate")
	if data_rate is None:
		data_rate = config.find(".//output_data_rate")
	if data_rate is not None:
		data_rate = data_rate.text.strip()
		if  50 == data_rate:
			conf.sensor.output_rate = Output_Data_Rate.OUTPUT_RATE_50
		if 119 == data_rate:
			conf.sensor.output_rate = Output_Data_Rate.OUTPUT_RATE_119
		if 238 == data_rate:
			conf.sensor.output_rate = Output_Data_Rate.OUTPUT_RATE_238

	bundles = config.find(".//Bundles")
	if bundles is None:
		bundles = config.find(".//bundles")
	if bundles is not None:
		conf.comm.use_bundles = bundles.text.strip().capitalize() == "True"

	madgwick = config.find(".//Madgwick")
	if madgwick is None:
		madgwick = config.find(".//madgwick")
	if madgwick is not None:
		conf.sensor.madgwick = madgwick.text.strip().capitalize() == "True"

	live_mag_correction = config.find(".//Live_mag_correction")
	if live_mag_correction is None:
		live_mag_correction = config.find(".//live_mag_correction")
	if live_mag_correction is not None:
		conf.sensor.live_mag_correction = live_mag_correction.text.strip().capitalize() == "True"

	base_address = config.find(".//Base")
	if base_address is None:
		base_address = config.find(".//Base")
	if base_address is not None:
		conf.osc.base_address = base_address.text.strip()
		conf.osc.error_address = conf.osc.base_address + defaults.CHORDATA_DEF_ERROR_SADDR
		conf.osc.msg_address = conf.osc.base_address + defaults.CHORDATA_DEF_MSG_SADDR

	avatar = xml.find(".//Avatar")
	armature = None
	if avatar is None:
		avatar = config.find(".//avatar")
	if avatar is not None:
		print("Trying to load avatar from config file")
		avatar = avatar.text.strip()
		
		temp = tempfile.NamedTemporaryFile(suffix='.gltf', delete=False)
		temp.write(avatar.encode())
		temp_path = temp.name
		temp.close()

		armature = Armature("Avatar", temp_path)

	use_armature = config.find(".//use_armature")
	if use_armature is None:
		use_armature = config.find(".//Use_armature")
	if use_armature is not None:
		conf.use_armature = use_armature.text.strip().capitalize() == "True"

		if conf.use_armature and not avatar:
			raise Exception("No armature found in config file")
		else :
			print("Using armature: " + armature.label)
			core_use_armature(armature)

	websocket_port = config.find(".//websocket_port")
	if websocket_port is None:
		websocket_port = config.find(".//Websocket_Port")
	if websocket_port is not None:
		conf.comm.websocket_port = int(websocket_port.text.strip())

	
		
