# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

# distutils: language = c++

from libcpp.string cimport string

cdef extern from "def.h":
	cdef int _NOTOCHORD_CRC32 

	cdef string _CHORDATA_CONF_SCHEMA
	cdef string _CHORDATA_CONF

	cdef string _CHORDATA_I2C_DEFAULT_ADAPTER_

	cdef string _CHORDATA_DEF_PROMPT
	cdef string _CHORDATA_DEF_EOM

	cdef string _CHORDATA_DEF_OSC_ADDR
	cdef string _CHORDATA_DEF_ERROR_SADDR
	cdef string _CHORDATA_DEF_MSG_SADDR
	cdef string _CHORDATA_BUNDLE_REPL_MARK
	cdef string _CHORDATA_BUNDLE_PAYLOAD_MARK

	cdef string _CHORDATA_PATTERN_FORMAT
	cdef string _CHORDATA_LOGGING_PATTERN

	cdef int _CHORDATA_DEF_ODR
	cdef float _MADGWICK_BETA
	cdef int _KALMAN_OVERSAMPLE_RATIO
	cdef float _MAX_AG_CALIB_SLOPE


cdef extern from "LSM9DS1/LSM9DS1.h":
	cdef float SENSITIVITY_ACCELEROMETER_2
	cdef float SENSITIVITY_ACCELEROMETER_4
	cdef float SENSITIVITY_ACCELEROMETER_8
	cdef float SENSITIVITY_ACCELEROMETER_16
	cdef float SENSITIVITY_GYROSCOPE_245
	cdef float SENSITIVITY_GYROSCOPE_500
	cdef float SENSITIVITY_GYROSCOPE_2000
	cdef float SENSITIVITY_MAGNETOMETER_4
	cdef float SENSITIVITY_MAGNETOMETER_8
	cdef float SENSITIVITY_MAGNETOMETER_12
	cdef float SENSITIVITY_MAGNETOMETER_16

# this is the string "chordata", used for checking presence of data in EEPROM 
NOTOCHORD_CRC32 = _NOTOCHORD_CRC32 

# XML deaults
CHORDATA_CONF_SCHEMA = _CHORDATA_CONF_SCHEMA.decode('utf-8')
CHORDATA_CONF = _CHORDATA_CONF.decode('utf-8')

CHORDATA_I2C_DEFAULT_ADAPTER_ = _CHORDATA_I2C_DEFAULT_ADAPTER_.decode('utf-8')

CHORDATA_DEF_OSC_ADDR = _CHORDATA_DEF_OSC_ADDR.decode('utf-8')
CHORDATA_DEF_ERROR_SADDR = _CHORDATA_DEF_ERROR_SADDR.decode('utf-8')
CHORDATA_DEF_MSG_SADDR = _CHORDATA_DEF_MSG_SADDR.decode('utf-8')
CHORDATA_BUNDLE_REPL_MARK = _CHORDATA_BUNDLE_REPL_MARK.decode('utf-8')
CHORDATA_BUNDLE_PAYLOAD_MARK = _CHORDATA_BUNDLE_PAYLOAD_MARK.decode('utf-8')

CHORDATA_PATTERN_FORMAT = _CHORDATA_PATTERN_FORMAT.decode('utf-8')
CHORDATA_LOGGING_PATTERN = _CHORDATA_LOGGING_PATTERN.decode('utf-8')

# Sensor fusion defaults
CHORDATA_DEF_ODR = _CHORDATA_DEF_ODR
MADGWICK_BETA = _MADGWICK_BETA

KALMAN_OVERSAMPLE_RATIO = _KALMAN_OVERSAMPLE_RATIO

MAX_OFFSET_CALIB_SLOPE = _MAX_AG_CALIB_SLOPE

SENSOR_SENSITIVITY_ACCELEROMETER_2 = SENSITIVITY_ACCELEROMETER_2
SENSOR_SENSITIVITY_ACCELEROMETER_4 = SENSITIVITY_ACCELEROMETER_4
SENSOR_SENSITIVITY_ACCELEROMETER_8 = SENSITIVITY_ACCELEROMETER_8
SENSOR_SENSITIVITY_ACCELEROMETER_16 = SENSITIVITY_ACCELEROMETER_16
SENSOR_SENSITIVITY_GYROSCOPE_245 = SENSITIVITY_GYROSCOPE_245
SENSOR_SENSITIVITY_GYROSCOPE_500 = SENSITIVITY_GYROSCOPE_500
SENSOR_SENSITIVITY_GYROSCOPE_2000 = SENSITIVITY_GYROSCOPE_2000
SENSOR_SENSITIVITY_MAGNETOMETER_4 = SENSITIVITY_MAGNETOMETER_4
SENSOR_SENSITIVITY_MAGNETOMETER_8 = SENSITIVITY_MAGNETOMETER_8
SENSOR_SENSITIVITY_MAGNETOMETER_12 = SENSITIVITY_MAGNETOMETER_12
SENSOR_SENSITIVITY_MAGNETOMETER_16 = SENSITIVITY_MAGNETOMETER_16