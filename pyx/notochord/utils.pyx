# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from . import defaults

from .utils cimport _Offset_Calibrator, throw_for_test, _run_test_websocket
from libc.math cimport M_PI
from libc.stdint cimport (uint8_t, uint16_t, uint32_t, uint64_t,
                          int8_t, int16_t, int32_t, int64_t)


cpdef float deg_to_rads(float num):
	return num * M_PI / 180;


cpdef tuple convert_LSM9DS1_ENU_rads(float gx, float gy, float gz, 
				float ax, float ay, float az, 
				float mx, float my, float mz):
	return (deg_to_rads(gy), deg_to_rads(gx), deg_to_rads(gz),
			ay, ax, az,  my, -mx, mz)

cpdef tuple convert_LSM9DS1_ENU_degs(float gx, float gy, float gz, 
				float ax, float ay, float az, 
				float mx, float my, float mz):
	return (gy, gx, gz,	ay, ax, az,  my, -mx, mz)


cdef class Offset_Calibrator:
	def __cinit__(self, max_slope = defaults.MAX_OFFSET_CALIB_SLOPE):
		self.c_ptr = new _Offset_Calibrator(max_slope)
		self.c_ptr.result[0] = 0
		self.c_ptr.result[1] = 0
		self.c_ptr.result[2] = 0

	def __dealloc__(self):
		if self.c_ptr != NULL:
			del self.c_ptr

	def add_reading(self, int16_t x, int16_t y, int16_t z):
		return self.c_ptr.add_reading(x,y,z)

	property result:
		def __get__(self):
			return tuple(self.c_ptr.result)

	property samples:
		def __get__(self):
			return self.c_ptr.samples


cpdef void _throw_for_test(msg) except *:
	'''This function is to be used on the test suite only'''
	throw_for_test(msg.encode('utf-8'))

def get_version_tuple_from_int(num):
	major = num // 10000
	minor = (num // 100) % 100
	patch = num % 100
	return major, minor, patch

def get_int_from_version_tuple(t):
	major = t[0]
	minor = t[1]
	patch = t[2]
	num = t[0] * 10000 + minor * 100 + patch
	return num

def run_test_websocket():
	_run_test_websocket()
