# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

# distutils: language = c++

from libcpp cimport bool
from libcpp.string cimport string
from .types cimport (Output_Redirect, _Accel_Scale_Options, _Gyro_Scale_Options, _Mag_Scale_Options,
					_Output_Data_Rate, _Kc_Values)

cdef extern from "config.h" namespace "Chordata":
	cdef cppclass Configuration_Data:
		Configuration_Data()
		int hardware_concurrency
		bool wait
		bool raw
		bool calib
		bool scan	
		bool use_armature
		
		_Kc_Values kc_revision
		#int odr
		string version
		long int version_number

		Conf_comm comm
		Conf_XML xml
		Conf_osc osc
		Conf_sensor sensor

		int odr()


cdef extern from "config.h" namespace "Chordata::Configuration_Data":
	cdef cppclass Conf_XML:
		string filename
		string schema


cdef extern from "config.h" namespace "Chordata::Configuration_Data":
	cdef cppclass Conf_comm:
		string adapter
		string ip
		int port
		Output_Redirect msg, error, transmit
		string filepath
		char log_level
		float send_rate
		bool use_bundles

		short websocket_port;

cdef extern from "config.h" namespace "Chordata::Configuration_Data":
	cdef cppclass Conf_osc:
		string base_address
		string error_address
		string msg_address

cdef extern from "config.h" namespace "Chordata::Configuration_Data":
	cdef cppclass Conf_sensor:
		_Gyro_Scale_Options gyro_scale
		_Accel_Scale_Options accel_scale
		_Mag_Scale_Options mag_scale
		_Output_Data_Rate output_rate
		bool madgwick
		bool live_mag_correction

cdef class Notochord_Config:
	# C++ type
	cdef Configuration_Data c_ptr
	
	# Cython types, each one holds a ptr of the member structs on Configuration_Data 
	cdef Communication_Config comm
	cdef XML_Config xml
	cdef osc_Config osc
	cdef sensor_Config sensor

cdef class Communication_Config:
	cdef Conf_comm *c_ptr

cdef class XML_Config:
	cdef Conf_XML *c_ptr

cdef class osc_Config:
	cdef Conf_osc *c_ptr

cdef class sensor_Config:
	cdef Conf_sensor *c_ptr