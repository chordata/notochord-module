# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

# distutils: language = c++
# 
from .core cimport Notochord_Runtime, get_runtime, terminate_runtime, _print_int_info
from .config cimport Configuration_Data, Notochord_Config
from .communicator cimport Communicator_Runtime
from .io cimport I2C
from .timer cimport Timekeeper, Node_Scheduler
from .armature cimport Armature

from collections import namedtuple
from . import io
from . import timer
from .config import Notochord_Config
from . import config
from .communicator import info, Output_Sink
from datetime import datetime

from enum import IntEnum

class Notochord_Status(IntEnum):
	IDLE = 0,
	RUNNING = 1,
	STOPPED = 2

def init():
	get_runtime()
	timer.global_timekeeper = Timekeeper("global")


def setup(Notochord_Config config = Notochord_Config(), bool reconfigure_communicator = True ):

	cdef Notochord_Runtime *n = get_runtime()
	n.setup(&config.c_ptr, reconfigure_communicator)
	cdef I2C _i2c = I2C()
	_i2c.c_ptr = n.get_i2c()
	io.i2c = _i2c 
	info("I2C adapter initialied on: {}", _i2c.adapter)
	info("== Notochord setup done == ")
	msg_flags = [flag.name for flag in Output_Sink if flag in config.comm.msg and flag != Output_Sink.NONE]
	err_flags = [flag.name for flag in Output_Sink if flag in config.comm.error and flag != Output_Sink.NONE]
	tra_flags = [flag.name for flag in Output_Sink if flag in config.comm.transmit and flag != Output_Sink.NONE]
	info("{:.<26}: {}".format("Conf file", config.xml.filename))
	info("{:.<26}: {}".format("Target msg", ', '.join(msg_flags)))
	info("{:.<26}: {}".format("Target error", ', '.join(err_flags)))
	info("{:.<26}: {}".format("Target transmit", ', '.join(tra_flags)))

	info("{:.<26}: {}:{}".format("OSC ip:port", config.comm.ip, config.comm.port))
	info("{:.<26}: {}".format("Log file", config.comm.filepath))

	info("{:.<26}: {}".format("Verbosity", config.comm.log_level))
	info("{:.<26}: {}".format("Send Raw", config.raw))
	info("{:.<26}: {}".format("Send Bundles", config.comm.use_bundles))

	# info("{:.<26}: {}".format("KCeptor revision", config.sensor.gyro_scale.name))
	info("{:.<26}: {}".format("Gyro scale", config.sensor.gyro_scale.name))
	info("{:.<26}: {}".format("Accel scale", config.sensor.accel_scale.name))
	info("{:.<26}: {}".format("Mag scale", config.sensor.mag_scale.name))
	# info("{:.<26}: {}".format("Sampling rate", config.sensor.output_rate))
	# info("{:.<26}: {}".format("Sending rate", config.comm.send_rate))


def terminate(terminate_communicator = True):
	terminate_runtime(terminate_communicator)
	io.i2c = None
	timer.global_timekeeper = None

def start(Node_Scheduler scheduler):
	info("Notochord start @ {}".format(datetime.now()))
	return get_runtime().start(scheduler.c_ptr)

def end():
	return get_runtime().end()

def use_armature(Armature armature):
	return get_runtime().set_armature(armature.c_ptr)

def status():
	return Notochord_Status(get_status()).name

def print_int_info():
	_print_int_info()

def get_config_as_xml():
	pass
	#return config.to_xml()