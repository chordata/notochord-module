# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from ._kalman cimport fquaternion


cdef extern from "MadgwickAHRS/MadgwickAHRS.h":
	cdef cppclass Fusion_Madgwick:
		Fusion_Madgwick(float hz, float _beta)
		float q0, q1, q2, q3;
		float sampleFreq, beta

		void MadgwickAHRSupdate(float gx, float gy, float gz, 
							float ax, float ay, float az, 
							float mx, float my, float mz)

		const fquaternion *get_quat()

cdef class Madgwick:
	cdef Fusion_Madgwick *c_ptr