from libc.stdint cimport (uint8_t, uint16_t, uint32_t, uint64_t,
						  int8_t, int16_t, int32_t, int64_t)
from libcpp.memory cimport shared_ptr

cdef extern from "kalman.h" namespace 'FSsf':
	# Warning! these DEFs have to be known at compile time so cannot be imported by cython
	# Should match those on FS_sensor_fusion/magnetic.h
	DEF MAX_OVERSAMPLE_RATIO = 8
	DEF FSsf_MAGBUFFSIZEX = 14					
	DEF FSsf_MAGBUFFSIZEY = (2 * FSsf_MAGBUFFSIZEX)

	cdef struct fquaternion:
		float q0;	# scalar component
		float q1;	# x vector component
		float q2;	# y vector component
		float q3;	# z vector component

	# accelerometer sensor structure definition
	cdef struct AccelSensor:
		float fGsAvg[3];							# averaged measurement (g)
		float fgPerCount;							# g per count
		int16_t iGsBuffer[MAX_OVERSAMPLE_RATIO][3];	# buffered measurements (counts)
		int16_t iGs[3];								# most recent unaveraged measurement (counts)
		int16_t iGsAvg[3];							# averaged measurement (counts)
		int16_t iCountsPerg;						# counts per g
		uint8_t iWhoAmI;							# sensor whoami
		float fQvGQa;								# accelerometer noise covariance to 1g sphere

	# gyro sensor structure definition
	cdef struct GyroSensor:
		float fDegPerSecPerCount;                 	# deg/s per count
		int16_t iYsBuffer[MAX_OVERSAMPLE_RATIO][3]; # buffered sensor frame measurements (counts)
		int16_t iCountsPerDegPerSec;                # counts per deg/s
		int16_t iYs[3];                             # most recent sensor frame measurement (counts)
		uint8_t iWhoAmI;                            # sensor whoami

	# magnetometer sensor structure definition
	cdef struct MagSensor:
		float fBsAvg[3];							# averaged un-calibrated measurement (uT)
		float fBcAvg[3];							# averaged calibrated measurement (uT)
		float fuTPerCount;							# uT per count
		float fCountsPeruT;							# counts per uT
		int16_t iBsBuffer[MAX_OVERSAMPLE_RATIO][3];	# buffered uncalibrated measurement (counts)
		int16_t iBs[3];								# most recent unaveraged uncalibrated measurement (counts)
		int16_t iBsAvg[3];							# averaged uncalibrated measurement (counts)
		int16_t iBcAvg[3];							# averaged calibrated measurement (counts)
		int16_t iCountsPeruT;						# counts per uT
		uint8_t iWhoAmI;							# sensor whoami
		float fQvBQd;								# magnetometer noise covariance to geomagnetic sphere

	# magnetic calibration structur
	cdef struct MagCalibration:
		float fV[3];					# current hard iron offset x, y, z, (uT)
		float finvW[3][3];				# current inverse soft iron matrix
		float fB;						# current geomagnetic field magnitude (uT)
		float fFitErrorpc;				# current fit error %
		float ftrV[3];					# trial value of hard iron offset z, y, z (uT)
		float ftrinvW[3][3];			# trial inverse soft iron matrix size
		float ftrB;						# trial value of geomagnetic field magnitude in uT
		float ftrFitErrorpc;			# trial value of fit error %
		float fA[3][3];					# ellipsoid matrix A
		float finvA[3][3];				# inverse of ellipsoid matrix A
		float fmatA[10][10];			# scratch 10x10 matrix used by calibration algorithms
		float fmatB[10][10];			# scratch 10x10 matrix used by calibration algorithms
		float fvecA[10];				# scratch 10x1 vector used by calibration algorithms
		float fvecB[4];					# scratch 4x1 vector used by calibration algorithms
		int8_t iCalInProgress;			# flag denoting that a calibration is in progress
		int8_t iMagCalHasRun;				# flag denoting that at least one calibration has been launched
		int8_t iValidMagCal;				# integer value 0, 4, 7, 10 denoting both valid calibration and solver used
		
	# magnetometer measurement buffer
	cdef struct MagneticBuffer:
		int16_t iBs[3][FSsf_MAGBUFFSIZEX][FSsf_MAGBUFFSIZEY];		# uncalibrated magnetometer readings
		int32_t index[FSsf_MAGBUFFSIZEX][FSsf_MAGBUFFSIZEY];		# array of time indices
		int16_t tanarray[FSsf_MAGBUFFSIZEX - 1];					# array of tangents of (100 * angle)
		int16_t iMagBufferCount;									# number of magnetometer readings

	# 9DOF Kalman filter accelerometer, magnetometer and gyroscope state vector structure
	cdef struct SV_9DOF_GBY_KALMAN:			
		# start: elements common to all motion state vectors
		float fPhiPl;						# roll (deg)
		float fThePl;						# pitch (deg)
		float fPsiPl;						# yaw (deg)
		float fRhoPl;						# compass (deg)
		float fChiPl;						# tilt from vertical (deg)
		float fRPl[3][3];					# a posteriori orientation matrix
		fquaternion fqPl;					# a posteriori orientation quaternion
		float fRVecPl[3];					# rotation vector
		float fOmega[3];					# average angular velocity (deg/s)
		int32_t systick;					# systick timer;
		# end: elements common to all motion state vectors
		float fQw10x10[10][10];				# covariance matrix Qw
		float fK10x7[10][7];				# kalman filter gain matrix K
		float fQwCT10x7[10][7];				# Qw.C^T matrix
		float fZErr[7];						# measurement error vector
		float fQv7x1[7];					# measurement noise covariance matrix leading diagonal
		float fDeltaPl;						# a posteriori inclination angle from Kalman filter (deg)
		float fsinDeltaPl;					# sin(fDeltaPl)
		float fcosDeltaPl;					# cos(fDeltaPl)
		float fqgErrPl[3];					# gravity vector tilt orientation quaternion error (dimensionless)
		float fqmErrPl[3];					# geomagnetic vector tilt orientation quaternion error (dimensionless)
		float fbPl[3];						# gyro offset (deg/s)
		float fbErrPl[3];					# gyro offset error (deg/s)
		float fDeltaErrPl;					# a priori inclination angle error correction
		float fAccGl[3];					# linear acceleration (g) in global frame
		float fVelGl[3];					# velocity (m/s) in global frame
		float fDisGl[3];					# displacement (m) in global frame	
		float fGyrodeltat;					# gyro sampling interval (s) = 1 / SENSORFS
		float fKalmandeltat;				# Kalman filter interval (s) = OVERSAMPLE_RATIO / SENSORFS	
		float fgKalmandeltat;				# g (m/s2) * Kalman filter interval (s) = 9.80665 * OVERSAMPLE_RATIO / SENSORFS	
		float fAlphaOver2;					# PI/360 * fKalmandeltat
		float fAlphaOver2Sq;				# (PI/360 * fKalmandeltat)^2
		float fAlphaOver2SqQvYQwb;			# (PI/360 * fKalmandeltat)^2 * (QvY + Qwb)
		float fAlphaOver2Qwb;				# PI/360 * fKalmandeltat * FQWB_9DOF_GBY_KALMAN;
		int8_t iFirstAccelMagLock;			# denotes that 9DOF orientation has locked to 6DOF eCompass
		int8_t resetflag;					# flag to request re-initialization on next pass



cdef class AccelSensorView:
	cdef AccelSensor *c_ptr # hold a C++ instance which we're wrapping

	@staticmethod
	cdef AccelSensorView create(AccelSensor *c_ptr)

cdef class GyroSensorView:
	cdef GyroSensor *c_ptr # hold a C++ instance which we're wrapping

	@staticmethod
	cdef GyroSensorView create(GyroSensor *c_ptr)

cdef class MagSensorView:
	cdef MagSensor *c_ptr # hold a C++ instance which we're wrapping

	@staticmethod
	cdef MagSensorView create(MagSensor *c_ptr)

cdef class MagCalibrationView:
	cdef MagCalibration *c_ptr # hold a C++ instance which we're wrapping

	@staticmethod
	cdef MagCalibrationView create(MagCalibration *c_ptr)
	
cdef class MagneticBufferView:
	cdef MagneticBuffer *c_ptr # hold a C++ instance which we're wrapping

	@staticmethod
	cdef MagneticBufferView create(MagneticBuffer *c_ptr)

cdef class StateVectorView:
	cdef SV_9DOF_GBY_KALMAN *c_ptr # hold a C++ instance which we're wrapping

	@staticmethod
	cdef StateVectorView create(SV_9DOF_GBY_KALMAN *c_ptr)
