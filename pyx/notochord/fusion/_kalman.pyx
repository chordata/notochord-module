# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 
from .. import defaults

from ._kalman cimport (Fusion_Kalman_Base, Fusion_Kalman_Raw, Fusion_Kalman_LSM9DS1, 
						fquaternion, MagneticBuffer, kalman_ptr)
from ..math._types cimport Quaternion, _Vector3, Vector

from libcpp.memory cimport shared_ptr, static_pointer_cast, make_shared
from libc.stdint cimport (uint8_t, uint16_t, uint32_t, uint64_t,
						  int8_t, int16_t, int32_t, int64_t)

# Warning! these DEFs have to be known at compile time so cannot be imported by cython
# Should match those on FS_sensor_fusion/magnetic.h
DEF FSsf_MAGBUFFSIZEX = 14					
DEF FSsf_MAGBUFFSIZEY = (2 * FSsf_MAGBUFFSIZEX)
cdef tuple sensor_axis_LSM9DS1 = ('LSM9DS1', 'KCEPTOR', 'KCEPTOR++')

# ctypedef fused vector_or_matrix:
# 	tuple
# 	list

cdef class Kalman:
	@staticmethod
	cdef Kalman create(kalman_ptr c_ptr):
		'''Create a Kalman extension type with an external C++ Kalman ob'''
		cdef Kalman new_kalman = Kalman.__new__(Kalman, 0, 0)
		new_kalman.c_ptr = c_ptr

		# Create Py*Sensors with the same underlying C++ objects
		new_kalman.accel_sensor = AccelSensorView.create(c_ptr.get().get_accel_sensor())
		new_kalman.gyro_sensor = GyroSensorView.create(c_ptr.get().get_gyro_sensor())
		new_kalman.mag_sensor = MagSensorView.create(c_ptr.get().get_mag_sensor())
		new_kalman.mag_cal = MagCalibrationView.create(c_ptr.get().get_mag_calib())
		new_kalman.mag_buf = MagneticBufferView.create(c_ptr.get().get_mag_calib_buf())
		new_kalman.sv = StateVectorView.create(c_ptr.get().get_sv())

		return new_kalman

	def __cinit__(self, uint16_t sensor_fs = defaults.CHORDATA_DEF_ODR, uint8_t oversample_ratio = defaults.KALMAN_OVERSAMPLE_RATIO, 
					str sensor_axis = 'RAW', bool enable_mag_correct = False ):
		cdef Fusion_Kalman_Base* ptr_raw = NULL
		if sensor_fs != 0: #Don't build underlying C++ object if FS == 0
			if sensor_axis == 'RAW':
				ptr_raw = new Fusion_Kalman_Raw(sensor_fs, oversample_ratio, enable_mag_correct)
				self.c_ptr = shared_ptr[Fusion_Kalman_Base](ptr_raw)
			elif sensor_axis in sensor_axis_LSM9DS1:
				ptr_raw = new Fusion_Kalman_LSM9DS1(sensor_fs, oversample_ratio, enable_mag_correct)
				self.c_ptr = shared_ptr[Fusion_Kalman_Base](ptr_raw)
			else:
				raise ValueError("Invalid `sensor_axis` value, it should be one of these strings: RAW, " + ", ".join(sensor_axis_LSM9DS1))

			self.c_ptr.get().set_sensor_res(defaults.SENSOR_SENSITIVITY_GYROSCOPE_2000, 
											defaults.SENSOR_SENSITIVITY_ACCELEROMETER_8,
											defaults.SENSOR_SENSITIVITY_MAGNETOMETER_4 )
			# Create Py*Sensors with the same underlying C++ objects
			self.accel_sensor = AccelSensorView.create(self.c_ptr.get().get_accel_sensor())
			self.gyro_sensor = GyroSensorView.create(self.c_ptr.get().get_gyro_sensor())
			self.mag_sensor = MagSensorView.create(self.c_ptr.get().get_mag_sensor())
			self.mag_cal = MagCalibrationView.create(self.c_ptr.get().get_mag_calib())
			self.mag_buf = MagneticBufferView.create(self.c_ptr.get().get_mag_calib_buf())
			self.sv = StateVectorView.create(self.c_ptr.get().get_sv())

		self.mag_buf_size_x = FSsf_MAGBUFFSIZEX
		self.mag_buf_size_y = FSsf_MAGBUFFSIZEY

	def __dealloc__(self):
		pass
		# if self.c_ptr != NULL:
		# 	del self.c_ptr

	# -----------  General props  -----------
		
	property loopcounter:
		def __get__(self):
			return self.c_ptr.get().get_loopcounter()

	property iCounter:
		def __get__(self):
			return self.c_ptr.get().get_icounter()

	property q:
		def __get__(self):
			cdef const fquaternion *result = self.c_ptr.get().get_quat()
			return Quaternion(result.q0, result.q1, result.q2, result.q3)

	property gyro_res:
		def __get__(self):
			return self.c_ptr.get().get_gyro_res()

		def __set__(self, n):
			self.c_ptr.get().set_sensor_res(n,-1,-1)

	property accel_res:
		def __get__(self):
			return self.c_ptr.get().get_accel_res()

		def __set__(self, n):
			self.c_ptr.get().set_sensor_res(-1, n, -1)

	property mag_res:
		def __get__(self):
			return self.c_ptr.get().get_mag_res()

		def __set__(self, n):
			self.c_ptr.get().set_sensor_res(-1,-1, n)

	def set_sensor_res(self, float g_res = -1, float a_res = -1, float m_res = -1 ):
		if g_res == -1:
			g_res = self.gyro_res
		elif 0 >= g_res or g_res >= 1:
			raise ValueError("The sensor resolution should be 0 < res < 1")

		if a_res == -1:
			a_res = self.accel_res
		elif 0 >= a_res or a_res >= 1:
			raise ValueError("The sensor resolution should be 0 < res < 1")

		if m_res == -1:
			m_res = self.mag_res
		elif 0 >= m_res or m_res >= 1:
			raise ValueError("The sensor resolution should be 0 < res < 1")

		self.c_ptr.get().set_sensor_res( g_res, a_res, m_res )
		
	# -----------  Filter run  -----------

	def set_gyro(self, int16_t x, int16_t y, int16_t z):
		self.c_ptr.get().set_gyro(x,y,z)

	def set_accel(self, int16_t x, int16_t y, int16_t z):
		self.c_ptr.get().set_accel(x,y,z)

	def set_mag(self, int16_t x, int16_t y, int16_t z):
		self.c_ptr.get().set_mag(x,y,z)

	def process(self):
		return self.c_ptr.get().process_and_run()

	def run(self, int16_t gx, int16_t gy, int16_t gz,
					int16_t ax, int16_t ay, int16_t az,
					int16_t mx, int16_t my, int16_t mz):
		self.c_ptr.get().set_sensors(gx,gy,gz,  ax,ay,az,  mx,my,mz)
		return self.c_ptr.get().process_and_run()

	# -----------  Accel and gyro buffers  -----------
	# TODO: review the use of buf_size, is it correct to use iCounter?
	property gyro_buffer:
		def __get__(self):
			cdef uint8_t buf_size = self.c_ptr.get().get_icounter()
			cdef const Fusion_Kalman_Base.xyz_array *gyro_buf = self.c_ptr.get().get_gyro_buf()
			for i in range(buf_size):
				yield (gyro_buf[i][0], gyro_buf[i][1], gyro_buf[i][2])

	property accel_buffer:
		def __get__(self):
			cdef uint8_t buf_size = self.c_ptr.get().get_icounter()
			cdef const Fusion_Kalman_Base.xyz_array *accel_buf = self.c_ptr.get().get_accel_buf()
			for i in range(buf_size):
				yield (accel_buf[i][0], accel_buf[i][1], accel_buf[i][2])

	property mag_buffer:
		def __get__(self):
			cdef uint8_t buf_size = self.c_ptr.get().get_icounter()
			cdef const Fusion_Kalman_Base.xyz_array *mag_buf = self.c_ptr.get().get_mag_buf()
			for i in range(buf_size):
				yield (mag_buf[i][0], mag_buf[i][1], mag_buf[i][2])
	
	property accel_sensor:
		def __get__(self):
			return self.accel_sensor

	property gyro_sensor:
		def __get__(self):
			return self.gyro_sensor
	
	property mag_sensor:
		def __get__(self):
			return self.mag_sensor
	
	property mag_cal:
		def __get__(self):
			return self.mag_cal
	
	property mag_buf:
		def __get__(self):
			return self.mag_buf
		
	property sv:
		def __get__(self):
			return self.sv

	# -----------  Magnetic calibration  -----------
	
	def mag_calib(self, int x, int y, int z):
		return self.c_ptr.get().set_mag_run_calib(x,y,z)

	property mag_calib_buf_size:
		def __get__(self):
			return self.mag_buf_size_x * self.mag_buf_size_y

	property m_offset:
		def __get__(self):
			cdef vector3_ptr offset = make_shared[_Vector3](0,0,0)
			self.c_ptr.get().get_mag_offset_for_EEPROM(offset)

			vec = Vector.create(offset)

			return vec

		def __set__(self, tuple offset):
			cdef int16_t poffset[3]
			poffset = offset
			self.c_ptr.get().set_mag_offset_from_EEPROM(<int16_t*>poffset)


	property m_matrix:
		def __get__(self):
			cdef float matrix[3][3] 
			self.c_ptr.get().get_mag_matrix_for_EEPROM(matrix)
			return matrix

		def __set__(self, tuple matrix):
			cdef float pmatrix[3][3]
			pmatrix = matrix 
			self.c_ptr.get().set_mag_matrix_from_EEPROM(pmatrix)