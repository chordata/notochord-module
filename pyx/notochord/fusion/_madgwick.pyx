# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from .. import defaults

from ._madgwick cimport Fusion_Madgwick
from ._kalman cimport fquaternion
from ..math._types cimport Quaternion

cdef class Madgwick:
	def __cinit__(self, float hz = defaults.CHORDATA_DEF_ODR, float _beta = defaults.MADGWICK_BETA):
		self.c_ptr = new Fusion_Madgwick(hz, _beta)

	def __dealloc__(self):
		if self.c_ptr != NULL:
			del self.c_ptr

	property beta:
		def __get__(self):
			return self.c_ptr.beta

		def __set__(self, n):
			self.c_ptr.beta = n

	property sample_freq:
		def __get__(self):
			return self.c_ptr.sampleFreq

		def __set__(self, n):
			self.c_ptr.sampleFreq = n

	property q:
		def __get__(self):
			cdef const fquaternion* quat = self.c_ptr.get_quat()  
			return Quaternion(quat.q0, quat.q1, quat.q2, quat.q3)


	def update(self, float gx, float gy, float gz, 
				float ax, float ay, float az, 
				float mx, float my, float mz):
		self.c_ptr.MadgwickAHRSupdate(gx, gy, gz,  ax, ay, az,  mx, my, mz)


	def __repr__(self):
		return "Madgwick ODR={:.0f} Beta={:.2f} Q={}".format(self.c_ptr.sampleFreq, self.c_ptr.beta, self.q)

	