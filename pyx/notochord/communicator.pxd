# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

# distutils: language = c++

from .math._types cimport _Quaternion
from .node cimport _Node, _KCeptor_Base
from .config cimport Output_Redirect
from libcpp.string cimport string
from libcpp.memory cimport shared_ptr
from libcpp.vector cimport vector


cdef extern from "logger.h" namespace "Chordata::Communicator":
	cdef cppclass Logger:
		Output_Redirect get_sinks() const

cdef extern from "communicator.h" namespace "Chordata":
	cdef cppclass Communicator_Runtime:
		pass

	Communicator_Runtime* get_communicator()
	Communicator_Runtime* terminate_communicator()
		
cdef extern from "communicator.h" namespace "Chordata::Communicator":
	void flush_loggers()
	string get_trasmit_packet_hex()

	shared_ptr[Logger] get_logger_msg()
	shared_ptr[Logger] get_logger_err()
	shared_ptr[Logger] get_logger_tra()

	void _info(...)
	void _transmit[Payload](const string *subtarget, const Payload *payload)
	void _transmit[Payload](const _Node *n, const Payload *payload)
	void _transmit(const _KCeptor_Base *kc)
	void _error(...)
	void _debug(...)
	void _trace(...)
	void _warn(...)
	void _critical(...)
	vector[string]& _get_registry(int level)
	void _clear_registry(int level)	

cdef extern from "cython_sink.h" namespace "Chordata":
	cdef cppclass Cython_Buffer:
		vector[string]& get_registry(...)
	

cdef class CyLogger:
	cdef shared_ptr[Logger] c_ptr

cdef class CyBuffer:
	cdef Cython_Buffer *c_ptr

	@staticmethod
	cdef CyBuffer create(Cython_Buffer *c_ptr)
