# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp.memory cimport shared_ptr, weak_ptr
from libcpp cimport bool, nullptr
from libc.stdint cimport (uint8_t, uint16_t, uint32_t, uint64_t,
							int8_t, int16_t, int32_t, int64_t)
from libc.stddef cimport size_t

from .math._types cimport _Vector3, vector3_ptr, Vector, _Matrix3, Matrix3, matrix3_ptr
from .io cimport IO_Base, I2C, I2C_io, io_ptr
from .error cimport raise_py_error
from .utils cimport _Offset_Calibrator
from .types cimport _Mux_Channel, EEPROM_info
from .fusion._kalman cimport Kalman, Fusion_Kalman_Base

cdef extern from "hierarchy.h" namespace "Chordata":
	void peek_destruction(shared_ptr[Link] ptr)

	cdef cppclass Link:
		Link(shared_ptr[Link] _parent, size_t max) except +raise_py_error
		weak_ptr[Link] get_parent() const
		shared_ptr[Link] get_child( size_t nChild ) const 

	ctypedef shared_ptr[Link] link_ptr
	ctypedef weak_ptr[Link] link_wptr
	
	cdef cppclass Link_Info:
		string label
		string parent_label
		link_wptr parent
		vector[link_wptr]children 
		vector[string]children_label
		long shared_ptr_count

	cdef cppclass _Hierarchy:
		_Hierarchy(link_ptr _parent)
		void build_index()
		const Link_Info& get_link_info(const string label)


# ----------------- Physical Nodes C++ ----------------- 

cdef extern from "LSM9DS1.h":
	cdef cppclass LSM9DS1[IO]:
		int16_t gx, gy, gz
		int16_t ax, ay, az
		int16_t mx, my, mz

		float calcGyro(int16_t gyro)
		float calcAccel(int16_t gyro)
		float calcMag(int16_t gyro)


cdef extern from "phy_nodes.h" namespace "Chordata":
	cdef cppclass _Node(Link):
		string get_label() const
		uint8_t get_address() const
		void bang() except +raise_py_error
		
	# // -------- MUX ------- //
	cdef cppclass _Mux(_Node):
		@staticmethod
		link_ptr Create(link_ptr _parent,
						uint8_t _address, string _label,
						io_ptr _io ) except +raise_py_error

		void bang_branch(_Mux_Channel mch) except +raise_py_error
		_Mux_Channel get_state() const 

	# // -------- BRANCH ------- //
	cdef cppclass _Branch(_Node):
		pass

	# // -------- KCEPTOR ------- //
	cdef enum sensor_axis:
		X_AXIS = 0
		Y_AXIS = 1
		Z_AXIS = 2

	cdef cppclass _KCeptor_Base(_Node):
		# /*-------------- Getters --------------*/
		const _Offset_Calibrator *get_g_calibrator()
		const _Offset_Calibrator *get_a_calibrator()
		
		int16_t get_g_raw_read(sensor_axis axis)
		int16_t get_a_raw_read(sensor_axis axis)
		int16_t get_m_raw_read(sensor_axis axis)

		const vector3_ptr get_g_offset()
		const vector3_ptr get_a_offset() 
		const vector3_ptr get_m_offset() 
		const matrix3_ptr get_m_matrix()

		int get_odr()
		shared_ptr[Fusion_Kalman_Base] get_kalman()

		# /*-------------- Setters --------------*/
		void set_g_offset(int16_t,int16_t,int16_t)
		void set_a_offset(int16_t,int16_t,int16_t)
		void set_m_offset(int16_t,int16_t,int16_t)
		void set_m_matrix(matrix3_ptr)

		# /*-------------- Getters --------------*/
		uint8_t _get_accel_scale()
		uint16_t _get_gyro_scale()
		uint8_t _get_mag_scale()
		uint8_t _get_kcpp_rate()

		# /*-------------- Calibration --------------*/
		float calibrate_xg()
		void reset_xg_calib()
		bool set_state(int _status)

		void stop() except +raise_py_error

	cdef cppclass _KCeptor(_KCeptor_Base):
		_KCeptor(link_ptr _parent,
			uint8_t _address, string _label,
			io_ptr _io) except +raise_py_error
		@staticmethod
		link_ptr Create(link_ptr _parent, 
						uint8_t _address, string _label,
						io_ptr _io) except +raise_py_error

		# /*-------------- Getters --------------*/
		const LSM9DS1[IO_Base] *get_imu()

		float get_a_read(sensor_axis axis)
		float get_m_read(sensor_axis axis)
		float get_g_read(sensor_axis axis)

		

		# /*-------------- IMU Control --------------*/	
		bool setup() except +raise_py_error
		void read_gyro() except +raise_py_error
		void read_accel() except +raise_py_error
		void read_mag(bool calc) except +raise_py_error

		# /*-------------- EEPROM Control --------------*/
		EEPROM_info get_info_from_EEPROM() except +raise_py_error
		void read_calib_from_EEPROM() except +raise_py_error 
		void write_calib_to_EEPROM(const EEPROM_info *) except +raise_py_error

		# /*-------------- Calibration --------------*/
		float calibrate_m() except +raise_py_error

		# /*-------------- Utils --------------*/
		@staticmethod
		bool is_kc_connected(io_ptr _io, uint8_t _address) except +raise_py_error

	cdef cppclass _KCeptorPP(_KCeptor_Base):
		_KCeptorPP(link_ptr _parent,
			uint8_t _address, string _label,
			io_ptr _io)
		@staticmethod
		link_ptr Create(link_ptr _parent, 
						uint8_t _address, string _label,
						io_ptr _io) except +raise_py_error

		bool setup() except +raise_py_error
		void read_sensors_DMA(bool calc) except +raise_py_error
		void change_i2c_addr(uint8_t) except +raise_py_error

		bool save_data() except +raise_py_error
		void read_data() except +raise_py_error

		# /*-------------- Utils --------------*/
		@staticmethod
		bool is_kc_connected(io_ptr _io, uint8_t _address) except +raise_py_error

ctypedef shared_ptr[_Node] _Node_ptr
ctypedef shared_ptr[_Mux] mux_ptr
ctypedef shared_ptr[_Branch] branch_ptr
ctypedef shared_ptr[_KCeptor] kceptor_ptr

# ----------------- Physical Nodes Cython ----------------- 

cdef class Hierarchy:
	cdef _Hierarchy *c_ptr
	cdef bool _index_built

cdef class Node:
	cdef Node _parent
	cdef Node _child
	cdef _Node_ptr c_ptr
	cdef I2C _i2c
	
	cdef link_ptr set_hierarchy(self, Node parent)
	@staticmethod
	cdef I2C set_i2c(I2C _i2c)

cdef class Branch(Node):
	@staticmethod
	cdef Branch create_branch(Mux mux, branch_ptr b_ptr)

cdef class Mux(Node):
	cdef dict branches

cdef class KCeptor_Base(Node):
	cdef Kalman _kalman
	cdef Matrix3 _m_matrix

cdef class KCeptor(KCeptor_Base):
	cdef _KCeptor* kc_ptr(self)
	@staticmethod
	cdef bool is_kc_connected(io_ptr _io, uint8_t _address)

cdef class KCeptorPP(KCeptor_Base):
	cdef _KCeptorPP* kc_ptr(self)
	cpdef change_addr(self, uint8_t)
	@staticmethod
	cdef bool is_kc_connected(io_ptr _io, uint8_t _address)