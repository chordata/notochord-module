from libcpp.vector cimport vector
from libcpp.string cimport string
from libcpp.unordered_map cimport unordered_map
from .math._types cimport _Quaternion, _Vector3i
from libc.stdint cimport (uint64_t)

cdef extern from "calibration_manager.h" namespace "Chordata":
	cdef enum _Calibration_Status:
		CALIB_IDLE  = 0,
		CALIBRATING = 1,
		STATIC      = 2,
		ARMS        = 3,
		TRUNK       = 4,
		LEFT_LEG    = 5,
		RIGHT_LEG   = 6

	cdef cppclass _Sensor_Payload:
		_Sensor_Payload()
		_Sensor_Payload(uint64_t timestamp, string label, _Quaternion q,
                        _Vector3i gyro, _Vector3i accel, _Vector3i mag)
		uint64_t timestamp
		string label
		_Quaternion q
		_Vector3i gyro, accel, mag
	
	cdef cppclass Calibration_Manager:
		Calibration_Manager()
		void set_status(int status)
		const vector[_Sensor_Payload]& get_buffer()
		const unordered_map[string, uint64_t]& get_indexes()
		void _clear_sensor_payload()
		int get_cycle()