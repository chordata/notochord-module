# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

	# distutils: language = c++
from .config cimport Notochord_Config, Configuration_Data #, Output_Redirect

from .error import Parse_Error
from lxml import etree
from os import path

from enum import IntEnum
from .types import (Accel_Scale_Options, Gyro_Scale_Options, 
			Mag_Scale_Options, Output_Data_Rate,
			Output_Sink, Kc_Values)

cdef class Communication_Config:

	def to_xml(self):
		comm = etree.Element("Communication")
		adapter = etree.SubElement(comm, "Adapter")
		adapter.text = self.adapter
		log_file = etree.SubElement(comm, "Log_file")
		log_file.text = self.filepath
		ip = etree.SubElement(comm, "Ip")
		ip.text = self.ip
		port = etree.SubElement(comm, "Port")
		port.text = str(self.port)
		msg = etree.SubElement(comm, "Msg")
		msg_sinks = [flag.name for flag in Output_Sink if flag in self.msg and flag != Output_Sink.NONE]
		msg.text = ','.join(msg_sinks)
		error = etree.SubElement(comm, "Error")
		error_sinks = [flag.name for flag in Output_Sink if flag in self.error and flag != Output_Sink.NONE]
		error.text = ",".join(error_sinks)
		transmit = etree.SubElement(comm, "Transmit")
		tra_sinks = [flag.name for flag in Output_Sink if flag in self.transmit and flag != Output_Sink.NONE]
		transmit.text = ','.join(tra_sinks)

		send_rate = etree.SubElement(comm, "Send_rate")
		send_rate.text = str(self.send_rate)
		verbosity = etree.SubElement(comm, "Verbosity")
		verbosity.text = str(self.log_level)
		bundles = etree.SubElement(comm, "Bundles")
		bundles.text = str(self.use_bundles)
		ws_port = etree.SubElement(comm, "WS_Port")
		ws_port.text = str(self.websocket_port)
		return comm

	@property
	def adapter(self):
		return self.c_ptr.adapter.decode('utf-8')

	@adapter.setter
	def adapter(self, val):
		if not val:
			self.c_ptr.adapter = "/dev/null".encode('utf-8')	 

		if not isinstance(val, str):
			raise TypeError("Only `str` is supported for adapter, received {}".format(type(val)))

		self.c_ptr.adapter = val.encode('utf-8')

	@property
	def ip(self):
		return self.c_ptr.ip.decode('utf-8')

	@ip.setter
	def ip(self, val):
		if not isinstance(val, str):
			raise TypeError("Only `str` is supported for ip, received {}".format(type(val)))

		self.c_ptr.ip = val.encode('utf-8')

	@property
	def port(self):
		return self.c_ptr.port

	@port.setter
	def port(self, val):
		if not isinstance(val, int):
			raise TypeError("Only `int` is supported for port, received {}".format(type(val)))

		self.c_ptr.port = val

	@property
	def filepath(self):
		return self.c_ptr.filepath.decode('utf-8')

	@filepath.setter
	def filepath(self, val):
		if not isinstance(val, str):
			raise TypeError("Only `str` is supported for filepath, received {}".format(type(val)))

		self.c_ptr.filepath = path.realpath(val).encode('utf-8')

	@property
	def use_bundles(self):
		return self.c_ptr.use_bundles

	@use_bundles.setter
	def use_bundles(self, val):
		self.c_ptr.use_bundles = val

	@property
	def log_level(self):
		return self.c_ptr.log_level

	@log_level.setter
	def log_level(self, val):
		if not isinstance(val, int):
			raise TypeError("Only `int` is supported for log_level, received {}".format(type(val)))

		self.c_ptr.log_level = val

	@property
	def send_rate(self):
		return self.c_ptr.send_rate

	@send_rate.setter
	def send_rate(self, val):
		if not isinstance(val, float):
			raise TypeError("Only `float` is supported for send_rate, received {}".format(type(val)))

		self.c_ptr.send_rate = val

	@property
	def websocket_port(self):
		return self.c_ptr.websocket_port

	@websocket_port.setter
	def websocket_port(self, val):
		if not isinstance(val, int):
			raise TypeError("Only 'int' is supported for websocket_port, received {}".format(type(val)))
		self.c_ptr.websocket_port = val

	# -----------  OUTPUT SINKS  -----------
	
	@property
	def transmit(self):
		return Output_Sink(self.c_ptr.transmit)
	
	@transmit.setter
	def transmit(self, val):
		if not isinstance(val, Output_Sink):
			raise TypeError("Only `Output_Sink Enum` is supported, received {}".format(type(val)))

		self.c_ptr.transmit = val.value

	@property
	def msg(self):
		return Output_Sink(self.c_ptr.msg)
	
	@msg.setter
	def msg(self, val):
		if not isinstance(val, Output_Sink):
			raise TypeError("Only `Output_Sink Enum` is supported, received {}".format(type(val)))

		self.c_ptr.msg = val.value

	@property
	def error(self):
		return Output_Sink(self.c_ptr.error)
	
	@error.setter
	def error(self, val):
		if not isinstance(val, Output_Sink):
			raise TypeError("Only `Output_Sink Enum` is supported, received {}".format(type(val)))

		self.c_ptr.error = val.value


cdef class XML_Config:
	@property
	def filename(self):
		return self.c_ptr.filename.decode('utf-8')

	@filename.setter
	def filename(self, val):
		if not isinstance(val, str):
			raise TypeError("Only `str` is supported for filename, received {}".format(type(val)))

		self.c_ptr.filename = path.realpath(val).encode('utf-8')

	@property
	def schema(self):
		return self.c_ptr.schema.decode('utf-8')

	@schema.setter
	def schema(self, val):
		if not isinstance(val, str):
			raise TypeError("Only `str` is supported for schema, received {}".format(type(val)))

		self.c_ptr.schema = path.realpath(val).encode('utf-8')

cdef class osc_Config:

	def to_xml(self):
		osc = etree.Element("Osc")
		base = etree.SubElement(osc, "Base")
		base.text = self.base_address
		error_addr = etree.SubElement(osc, "Error")
		error_addr.text = self.error_address
		msg_addr = etree.SubElement(osc, "Msg")
		msg_addr.text = self.msg_address
		return osc

	@property
	def base_address(self):
		return self.c_ptr.base_address.decode('utf-8')
	@base_address.setter
	def base_address(self, val):
		if not isinstance(val, str):
			raise TypeError("Only `str` is supported for base_address, received {}".format(type(val)))

		self.c_ptr.base_address = val.encode('utf-8')

	@property
	def error_address(self):
		return self.c_ptr.error_address.decode('utf-8')
	@error_address.setter
	def error_address(self, val):
		if not isinstance(val, str):
			raise TypeError("Only `str` is supported for error_address, received {}".format(type(val)))

		self.c_ptr.error_address = val.encode('utf-8')

	@property
	def msg_address(self):
		return self.c_ptr.msg_address.decode('utf-8')
	@msg_address.setter
	def msg_address(self, val):
		if not isinstance(val, str):
			raise TypeError("Only `str` is supported for msg_address, received {}".format(type(val)))

		self.c_ptr.msg_address = val.encode('utf-8')


cdef class sensor_Config:

	def to_xml(self):
		sensor = etree.Element("Sensor")
		gyro = etree.SubElement(sensor, "gyro_scale")
		gyro.text = self.gyro_scale.name
		accel = etree.SubElement(sensor, "accel_scale")
		accel.text = self.accel_scale.name
		mag = etree.SubElement(sensor, "mag_scale")
		mag.text = self.mag_scale.name
		odr = etree.SubElement(sensor, "Output_Data_Rate")
		int_odr = [50, 119, 238]
		odr.text = str(int_odr[self.output_rate.value])
		mad = etree.SubElement(sensor, "Madgwick")
		mad.text = str(self.madgwick)
		live_mag = etree.SubElement(sensor, "live_mag_correction")
		live_mag.text = str(self.live_mag_correction)
		return sensor

	@property
	def accel_scale(self):
		return Accel_Scale_Options(self.c_ptr.accel_scale)

	@accel_scale.setter
	def accel_scale(self, val):
		if not isinstance(val, Accel_Scale_Options):
			raise TypeError("Only `Accel_Scale_Options Enum` is supported, received {}".format(type(val)))

		self.c_ptr.accel_scale = val.value

	@property
	def gyro_scale(self):
		return Gyro_Scale_Options(self.c_ptr.gyro_scale)

	@gyro_scale.setter
	def gyro_scale(self, val):
		if not isinstance(val, Gyro_Scale_Options):
			raise TypeError("Only `Gyro_Scale_Options Enum` is supported, received {}".format(type(val)))

		self.c_ptr.gyro_scale = val.value

	@property
	def mag_scale(self):
		return Mag_Scale_Options(self.c_ptr.mag_scale)

	@mag_scale.setter
	def mag_scale(self, val):
		if not isinstance(val, Mag_Scale_Options):
			raise TypeError("Only `Mag_Scale_Options Enum` is supported, received {}".format(type(val)))

		self.c_ptr.mag_scale = val.value
	
	@property
	def output_rate(self):
		return Output_Data_Rate(self.c_ptr.output_rate)

	@output_rate.setter
	def output_rate(self, val):
		if not isinstance(val, Output_Data_Rate):
			raise TypeError("Only `Output_Data_Rate Enum` is supported, received {}".format(type(val)))

		self.c_ptr.output_rate = val.value

	@property
	def madgwick(self):
		return self.c_ptr.madgwick

	@madgwick.setter
	def madgwick(self, val):
		self.c_ptr.madgwick = val

	@property
	def live_mag_correction(self):
		return self.c_ptr.live_mag_correction

	@live_mag_correction.setter
	def live_mag_correction(self, val):
		self.c_ptr.live_mag_correction = val



cdef class Notochord_Config:
	def __cinit__(self):
		"""Create the nested extension types and asign the pointer to the
		underlying C++ structs"""
		self.comm = Communication_Config()
		self.comm.c_ptr = &(self.c_ptr.comm)
		self.comm.filepath = path.realpath(self.comm.filepath)
		self.xml = XML_Config()
		self.xml.c_ptr = &(self.c_ptr.xml)
		self.xml.filename = path.realpath(path.join(path.dirname(__file__), 'Chordata.xml'))
		self.xml.schema = path.realpath(path.join(path.dirname(__file__), 'Chordata.xsd'))
		self.osc = osc_Config()
		self.osc.c_ptr = &(self.c_ptr.osc)
		self.sensor = sensor_Config()
		self.sensor.c_ptr = &(self.c_ptr.sensor)
	# -----------  NESTED STRUCT ACCESS  -----------
	def to_xml(self):
		v = self.version_number
		root = etree.Element("Chordata", version="{}.{}.{}".format(v[0],v[1],v[2]))
		config = etree.SubElement(root, "Configuration")
		kc_rev = etree.SubElement(config, "KC_revision")
		kc_rev.text = str(self.kc_revision)
		raw = etree.SubElement(config, "Raw")
		raw.text = str(self.raw)
		calib = etree.SubElement(config, "Calibrate")
		calib.text = str(self.calib)
		scan = etree.SubElement(config, "Scan")
		scan.text = str(self.scan)
		config.append(self.comm.to_xml())
		config.append(self.osc.to_xml())
		config.append(self.sensor.to_xml())
		return root

	@property
	def comm(self):
		return self.comm

	@property
	def xml(self):
		return self.xml

	@property
	def osc(self):
		return self.osc

	@property
	def sensor(self):
		return self.sensor
	
	# ----------- DIRECT ATTRIBUTE ACCESS  -----------

	@property
	def version(self):
		return self.c_ptr.version.decode('utf-8')

	@property
	def version_number(self):
		cdef int number = self.c_ptr.version_number
		cdef int patch = number % 100
		cdef int minor = (int) ((number - patch) % 10000) / 100
		cdef int mayor = (int) (number - minor*100 - patch) / 10000
		return (mayor, minor, patch)

	@property
	def hardware_concurrency(self):
		return self.c_ptr.hardware_concurrency

	@property
	def wait(self):
		return self.c_ptr.wait

	@wait.setter
	def wait(self, val):
		self.c_ptr.wait = val

	@property
	def raw(self):
		return self.c_ptr.raw

	@raw.setter
	def raw(self, val):
		self.c_ptr.raw = val

	@property
	def calib(self):
		return self.c_ptr.calib

	@calib.setter
	def calib(self, val):
		self.c_ptr.calib = val

	@property
	def scan(self):
		return self.c_ptr.scan

	@scan.setter
	def scan(self, val):
		self.c_ptr.scan = val

	@property
	def use_armature(self):
		return self.c_ptr.use_armature
	
	@use_armature.setter
	def use_armature(self, val):
		self.c_ptr.use_armature = val

	@property
	def odr(self):
		return self.c_ptr.odr()

	@odr.setter
	def odr(self, val):
		self.sensor.output_rate = val

	@property
	def kc_revision(self):
		return self.c_ptr.kc_revision

	@kc_revision.setter
	def kc_revision(self, val):
		if not isinstance(val, Kc_Values):
			raise TypeError("Only `Kc_Values Enum` is supported, received {}".format(type(val)))

		self.c_ptr.kc_revision = val.value