# Notochord python module

[![GitLab](https://img.shields.io/gitlab/license/chordata/notochord-module)](https://gitlab.com/chordata/notochord-module/-/blob/master/LICENSE)
[![GitLab Release (latest by date)](https://img.shields.io/gitlab/v/release/chordata/notochord-module?color=blue)](https://gitlab.com/chordata/notochord-module/-/releases)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/chordata/notochord-module?branch=master)](https://gitlab.com/chordata/notochord-module/-/pipelines)
[![download arm64](https://img.shields.io/badge/download-arm64%20module-orange)](https://gitlab.com/chordata/notochord-module/-/jobs/artifacts/master/browse/multiarch?job=arm64)

This python module contains the core of the [Chordata Motion](http://chordata.cc)'s software framework. It is designed to run inside a microcomputer such as the [Raspberry Pi](https://www.raspberrypi.org/) allowing to control mocap hardware peripherals such as Chordata's Hub and KCeptors.

It also handle many signal processing operations required to obtain a moving human skeleton from the raw sensor data. 

Most internal routines are implemented in C/C++ and wrapped using Cython, so the module combines the flexibility of python and the performance of a compiled language.

More info at http://chordata.cc

---


### Development

Even if the module is conceived to run inside system with access to low level peripherals, it can also be compiled in a regular PC for development or testing purpouses.

### Setup dev environment

The main depencies are:

- [Scons](https://scons.org/), the build environment
- [Cython](https://cython.org/), the static compiler for python 
- [Pytest](https://pytest.org/), the testing framework

A Makefile contains targets for the most common operations such as compiling the module and setting up the development environment. The first time you compile the module run the commands below:

```
make setup
make 
make install
make just-test
```

After every change just run `make` from this project root folder and it will compile a debug build. By default Scons will use 2 of the available cores on your PC to perform the compilation, which might take some time. This limit is set to avoid memory issues in low restricted hardware such as the Raspberry pi 3. If you want to change the number of cores being used set the following variable:

`export SCONSFLAGS=-j4 #<-- will use 4 cores`

### Dev notes:

Several targets were added to the Makefile to easily debug, or execute arbitrary pieces of code. Here are some examples, feel free to explore the Makefile for more

#### make test

will compile and run the test suite. You can also use `make just-test` to skip the compilation.

#### make repl

will open a python repl with the notochord module already imported.

#### make scratch

will run the scratchpad which is a section of the repository containing several snippets created by the developers to quickly test different features of the module. This section is intentionally unformal. The code snippets are added when needed without following a particular order or format. Contributors to the notochord module codebase are allowed to add snippets at will.

To run a particular snippet modify the `test/scratchpad/__main__.py file` or run the target assigning to the `SKETCH` variable like this:

```make scratch SKETCH=(snippet_name)```

#### make debug

Will run the test suite inside `gdb` to allow you to navigate the execution, adding breakpoints, etc. This target takes care of importing the symbols from the chordata shared library, so you wil need to tell `gdb` to trust this auto-execution commads:

`echo add-auto-load-safe-path $PWD >> ~/.gdbinit`

once inside `gdb` add breakpoints, as usual and use the command `run` to start execution of the test suite


