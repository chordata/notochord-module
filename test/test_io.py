# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import pytest
from time import sleep

def test_i2c_wrong_adapter(notochord_init, I2C, config, error):
	conf = config.Notochord_Config()
	conf.comm.adapter = '/dev/thisadapterdoesntexists'
	with pytest.raises(error.IO_Error):
		notochord_init.setup(conf)

def test_mock_i2c(I2C, error):
	# A working I2C object can only be gotten from the notochord_init runtime
	# when an instance is created using the class constructor it
	# returns a mock object, useful for testing
	i2c = I2C()
	assert i2c.adapter == '/dev/null'
	
	fake_read = i2c.read_simple_byte(0x70)
	assert fake_read == 0x00
	fake_read = i2c.read_byte(0x71, 0xab)
	assert fake_read == 0x00
	fake_read = i2c.read_bytes(0x72, 0xef, 10)
	assert len(fake_read) == 10
	assert fake_read[0] == 0x00

	assert i2c._mock_reads[0]['address'] == 0x70

	assert i2c._mock_reads[1]['address'] == 0x71
	assert i2c._mock_reads[1]['sub_address'] == 0xab 

	assert i2c._mock_reads[2]['address'] == 0x72 
	assert i2c._mock_reads[2]['sub_address'] == 0xef
	assert i2c._mock_reads[2]['count'] == 10 

	i2c._mock_reset()
	assert len(i2c._mock_reads) == 0
	assert len(i2c._mock_writes) == 0


@pytest.mark.i2c
def test_physical_i2c(i2c):
	MUX_ADDR = 0x73
	KC_ADDR = 0x6b
	KC_WHO_REG = 0x0F
	KC_WHO_ANS = 0b01101000
	i2c.write_simple_byte(MUX_ADDR, 0x0f)
	assert i2c.read_simple_byte(MUX_ADDR) == 0x0f

	kc_who_resp = i2c.read_byte(KC_ADDR, KC_WHO_REG)
	assert kc_who_resp == KC_WHO_ANS
	#print("KC WHOAMI RESPONSE: 0x{:0x}".format(kc_who_resp))

	#INIT ACEL:
	CTRL_REG5_XL = 0x1F
	CTRL_REG6_XL = 0x20
	OUT_X_L_XL = 0x28
	tempRegValue = 0;
	tempRegValue |= (1<<5);
	tempRegValue |= (1<<4);
	tempRegValue |= (1<<3);
	i2c.write_byte(KC_ADDR, CTRL_REG5_XL, tempRegValue);

	tempRegValue = 0;
	tempRegValue |= (2 & 0x07) << 5;
	tempRegValue |= (0x2 << 3);
	i2c.write_byte(KC_ADDR, CTRL_REG6_XL, tempRegValue);

	prev_res = []

	for i in range(10):
		res = i2c.read_bytes(KC_ADDR, OUT_X_L_XL, 6)
		assert res != prev_res
		#print("KC ACEL READINGS", res)
		prev_res = res
		sleep(0.1)

