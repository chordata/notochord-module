from notochord.config import Notochord_Config
from os import path
from lxml import etree
from notochord import utils
from notochord import timer
"""
c = Notochord_Config()
xml_path = path.realpath(path.join(path.dirname(__file__), "../../Chordata.xml"))
with open(xml_path) as f:
	xml_string = f.read()
schema_path = path.realpath(path.join(path.dirname(__file__), "../../Chordata.xsd"))
with open(schema_path) as f:
	schema_string = f.read()
c.parse(xml_string, schema_string)
print("Ip:\t\t",c.comm.ip)
print("Port:\t\t",c.comm.port)
print("Adapter:\t",c.comm.adapter)
print("Send_rate:\t",c.comm.send_rate)
print("Transmit:\t",c.comm.transmit)
print("Error:\t\t",c.comm.error)
print("Msg:\t\t",c.comm.msg)
print("Bundles:\t",c.comm.use_bundles)
print("Osc_base_address:\t",c.osc.base_address)
print("Osc_error_address:\t",c.osc.error_address)
print("Osc_msg_address:\t",c.osc.msg_address)
print("Gyro_Scale:\t",c.sensor.gyro_scale)
print("Accel_Scale:\t",c.sensor.accel_scale)
print("Mag_Scale:\t",c.sensor.mag_scale)
print("Madgwick:\t",c.sensor.madgwick)
print("live_mad_correction:\t",c.sensor.live_mag_correction)



print("-----------------------------------")

from notochord import core
args = ["1.2.3.4", "1234","-c",xml_path,"--schema",schema_path, "--send_rate", "45.5"]
c = core.parse_config(args)

print("Ip:\t\t",c.comm.ip)
print("Port:\t\t",c.comm.port)
print("Adapter:\t",c.comm.adapter)
print("Send_rate:\t",c.comm.send_rate)
print("Transmit:\t",c.comm.transmit)
print("Error:\t\t",c.comm.error)
print("Msg:\t\t",c.comm.msg)
print("Bundles:\t",c.comm.use_bundles)
print("Osc_base_address:\t",c.osc.base_address)
print("Osc_error_address:\t",c.osc.error_address)
print("Osc_msg_address:\t",c.osc.msg_address)
print("Gyro_Scale:\t",c.sensor.gyro_scale)
print("Accel_Scale:\t",c.sensor.accel_scale)
print("Mag_Scale:\t",c.sensor.mag_scale)
print("Madgwick:\t",c.sensor.madgwick)
print("live_mad_correction:\t",c.sensor.live_mag_correction)
"""

xml_path = path.realpath(path.join(path.dirname(__file__), "../default_biped.xml"))
with open(xml_path) as f:
	xml_string = f.read()
schema_path = path.realpath(path.join(path.dirname(__file__), "../../Chordata.xsd"))
with open(schema_path) as f:
	schema_string = f.read()

s = timer.Node_Scheduler()
h = utils.parse_armature_xml(xml_string, schema_string, s)
mux_info = h.get_node_info("r-arm")
print(mux_info)