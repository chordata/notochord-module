from time import sleep
from tracemalloc import stop
import uuid
import notochord
import random

from notochord.config import Notochord_Config, Output_Sink
from notochord.timer import Node_Scheduler, Timer
from notochord.mock_node import Mock_KCeptor
notochord.init()

n = Notochord_Config()
n.comm.msg = Output_Sink.STDOUT
n.comm.adapter = '/dev/null'

n.raw = True
n.calib = False


n.comm.log_level = 1

notochord.setup(n) 
notochord.communicator.info("Starting Notochord Scratch. IP {}".format(n.comm.ip))

s = Node_Scheduler()
t = Timer(s)

# Create a mock kceptor
k = Mock_KCeptor(None, 0x1b, "test_kc")
s.add_node(k)

inpt_str = ""

print(notochord.status())
notochord.start(s)
print("Write q to stop...")

while notochord.input() != "q":
    inpt_str = input()


notochord.end()