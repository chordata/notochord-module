import notochord
from notochord.node import Mux, KCeptor, Mux_Channel
from notochord.config import Notochord_Config
from notochord.fusion import Kalman
from time import sleep


MUX_ADDR = 0x73
KC_ADDR = 0x1f
KC_WHO_REG = 0x0F

notochord.init()

conf = Notochord_Config()
# conf.comm.adapter = '/dev/null'
conf.comm.log_level = 1

notochord.configure(conf)
mux = Mux(None, MUX_ADDR, "test_mux")
branch_2 = mux.get_branch(Mux_Channel.CH_2)
mux.bang_branch(Mux_Channel.CH_2)

kc = KCeptor(branch_2, 0x02, "test_kc")

response = kc.setup()
print("KC setup", response)

print(kc.eeprom)
# print(kc.eeprom["timestamp"])
# print("{:x}".format(kc.eeprom["validation"]))

# print("KC OFFSET BEFORE EEPROM:",kc.g_offset, kc.a_offset, kc.m_offset)
# print("KC MATRIX BEFORE EEPROM:\n",kc.m_matrix)


kc.read_calib_from_eeprom()
# print("KC OFFSET AFTER EEPROM:",kc.g_offset, kc.a_offset, kc.m_offset)
# print("KC MATRIX AFTER EEPROM:\n",kc.m_matrix)



print(kc.eeprom)