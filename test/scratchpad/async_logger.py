import notochord, copp_server
from time import sleep
import uuid, string, random

from notochord import communicator as comm
from notochord.math import Quaternion
from notochord.timer import Node_Scheduler, Timer
from notochord.mock_node import Mock_Mux, Mock_KCeptor
from notochord.types import Mux_Channel

port = 8462
server = copp_server.COPP_Server( port = port )
server.receive_timeout = 0.1 # this speeds up the teardown wait

# add some COPP_Server types for convenience
server.COPP_Lazy_Packet = copp_server.COPP_Lazy_Packet
server.COPP_Common_packet = copp_server.COPP_Common_packet

server.start()

def_conf = notochord.config.Notochord_Config()
def_conf.comm.adapter = '/dev/null'
def_conf.comm.port = 2222
def_conf.comm.msg = comm.Output_Sink.OSC #| comm.Output_Sink.PYTHON
def_conf.comm.use_bundles = True

notochord.setup(def_conf)
to_osc = "OSC random string 8m0y4blvietzwwR31l2F"

notochord.communicator.info("#1 | " + to_osc + "----")
notochord.communicator.info("#2 | " + to_osc)
notochord.communicator.flush()

notochord.communicator.info("#3 | " + to_osc)
notochord.communicator.info("#4 | " + to_osc)
notochord.communicator.flush()

#print(comm.get_registry(True))

kc = Mock_KCeptor(None, 0x1b, "test_kc")

notochord.communicator.transmit_node(kc, Quaternion())
notochord.communicator.flush()

sleep(0.1)
number_msg_received = 0
for packet in server.get():
    for msg in packet.get():
        if msg.typetags == ',s':
            number_msg_received += 1
            print(msg.payload)
            if not msg.payload[0].endswith(to_osc):
                print(
                    "Expected OSC message to end with `{}`, received `{}` instead".format(to_osc, msg.payload[0]))

print(number_msg_received)


notochord.terminate()


server.close_and_wait()

print("\n\n\n-----\n\n\n")

sleep(2)

def_conf.comm.msg = comm.Output_Sink.STDOUT | comm.Output_Sink.PYTHON
def_conf.comm.use_bundles = False

notochord.setup(def_conf)


notochord.communicator.info("Starting test...")

for i in range(250):
    print("---------------------")
