import notochord

from notochord.config import Notochord_Config, Output_Sink, Gyro_Scale_Options_py
from notochord.node import KCeptor

n = Notochord_Config()
n.comm.adapter = '/dev/null'


n.comm.msg = Output_Sink.STDOUT
n.comm.log_level = 2
n.sensor.gyro_scale = Gyro_Scale_Options_py.gyro_245
print(n.sensor.gyro_scale)

notochord.configure(n)
notochord.communicator.info("Starting test...")

k = KCeptor(None, 0x1b, "test_kceptor")

print(k.gyro)



