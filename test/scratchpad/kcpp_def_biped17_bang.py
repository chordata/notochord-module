from time import sleep
from tracemalloc import stop
import uuid
import notochord
import random

from notochord.config import Notochord_Config, Output_Sink
from notochord.timer import Node_Scheduler, Timer
from notochord.node import Mux, KCeptorPP
from notochord.types import Mux_Channel

MUX_ADDR = 0x70

notochord.init()

n = Notochord_Config()
n.comm.msg = Output_Sink.STDOUT
n.comm.port = 6565
n.comm.ip = "192.168.1.40"

n.raw = True

n.calib = False

#n.comm.adapter = '/dev/null'
n.comm.log_level = 1

notochord.setup(n) 
notochord.communicator.info("Starting scheduler scratch...")
# notochord.terminate()

s = Node_Scheduler()
t = Timer(s)
sensors = []

mux = Mux(None, MUX_ADDR, "test_mux")

#Right leg
branch_1 = mux.get_branch(Mux_Channel.CH_1)
r_upperleg = KCeptorPP(branch_1,   0x40, "r-upperleg")
r_lowerleg = KCeptorPP(r_upperleg, 0x41, "r-lowerleg")
r_foot     = KCeptorPP(r_lowerleg, 0x42, "r-foot")

s.add_node(r_upperleg)
s.add_node(r_lowerleg)
s.add_node(r_foot)
s.add_node(branch_1)

branch_2 = mux.get_branch(Mux_Channel.CH_2)
s.add_node(KCeptorPP(branch_2, 0x40, "base"))
s.add_node(branch_2)

#Left leg
branch_3 = mux.get_branch(Mux_Channel.CH_3)
l_upperleg = KCeptorPP(branch_3,   0x40, "l-upperleg")
l_lowerleg = KCeptorPP(l_upperleg, 0x41, "l-lowerleg")
l_foot     = KCeptorPP(l_lowerleg, 0x42, "l-foot")

s.add_node(l_upperleg)
s.add_node(l_lowerleg)
s.add_node(l_foot)
s.add_node(branch_3)

# Left arm
branch_4 = mux.get_branch(Mux_Channel.CH_4)
l_clavicle = KCeptorPP(branch_4,   0x42, "l-clavicle")
l_upperarm = KCeptorPP(l_clavicle, 0x40, "l-upperarm")
l_lowerarm = KCeptorPP(l_upperarm, 0x41, "l-lowerarm")
l_hand     = KCeptorPP(l_lowerarm, 0x43, "l-hand")

s.add_node(l_clavicle)
s.add_node(l_upperarm)
s.add_node(l_lowerarm)
s.add_node(l_hand)
s.add_node(branch_4)

# trunk
branch_5 = mux.get_branch(Mux_Channel.CH_5)
dorsal = KCeptorPP(branch_5,   0x41, "dorsal")
neck   = KCeptorPP(dorsal,     0x42, "neck")

s.add_node(dorsal)
s.add_node(neck)
s.add_node(branch_5)

# Left arm
branch_6 = mux.get_branch(Mux_Channel.CH_6)
r_clavicle = KCeptorPP(branch_6,   0x42, "r-clavicle")
r_upperarm = KCeptorPP(r_clavicle, 0x40, "r-upperarm")
r_lowerarm = KCeptorPP(r_upperarm, 0x41, "r-lowerarm")
r_hand     = KCeptorPP(r_lowerarm, 0x43, "r-hand")

s.add_node(r_clavicle)
s.add_node(r_upperarm)
s.add_node(r_lowerarm)
s.add_node(r_hand)
s.add_node(branch_6)


inpt_str = ""

print(notochord.status())
notochord.start(s)
print("Write q to stop...")
while inpt_str != "q":
    inpt_str = input()

notochord.end()
