from time import sleep
from tracemalloc import stop
import uuid
import notochord
import random

from notochord.config import Notochord_Config, Output_Sink
from notochord.timer import Node_Scheduler, Timer
from notochord.node import KCeptor, Mux_Channel
from notochord.mock_node import Mock_KCeptor, Mock_Branch, Mock_Mux

n = Notochord_Config()
n.comm.adapter = '/dev/null'

n.comm.msg = Output_Sink.STDOUT
n.comm.log_level = 2

notochord.setup(n) 
notochord.communicator.info("Starting scheduler scratch...")
# notochord.terminate()
# notochord.setup(n) 

notochord.communicator.info("Re-starting")



s = Node_Scheduler()
mux = Mock_Mux(None, 0x73, "test_mux")
t = Timer(s)
sensors = []

for i in range(6):
    s.add_node(mux.get_branch(Mux_Channel(i+1)))

    for i in range(3):
        sensors.append(Mock_KCeptor(None, 0x1b, ("test_m_kc_" + str(uuid.uuid1()))))
        s.add_node(sensors[-1])


print(notochord.status())
notochord.start(s)
sleep(5)
notochord.end()


# wait = 1000000/(18*50)


# for i in range(10000):
#     wait = s.bang(wait)
#     sleep(wait/1_000_000)

# print([round(num, 1) for num in sensors[0].get_debug_buffer()[-20:]])

# print(t.start_timer(30000))

# t.start()

# sleep(5)

# t.terminate()

# print("--------------------")    

# s2 = Node_Scheduler()
# t2 = Timer(s2)
# print(t2.start())

# s.handlers()

# mux.get_branch(Mux_Channel(1)).bang()