# Chordata's pyNotochord dev scratchpad main file
# ===============================================
# each sumodule in the scrathpad executes a routine
# they are just quick test created during development
# in no particular order. 
# uncomment the one you want to run, or add new ones

import sys

if len(sys.argv) < 2:
	# from . import dump_osc_payload
	# from . import send_osc_msgs
	# from . import payload
	# from . import kalman
	# from . import read_i2c_KC
	# from . import memory_managment
	# from . import export_kalman_csv

	# from . import read_i2c_KCPP
	# from . import read_Mux_KCPP

	# from . import kalman_calibration
	# from . import eeprom

	# from . import math_types
	# from . import armature
	# from . import parse_xml_lucas

	# from . import node_scheduler
	# from . import kceptor_bang
	# from . import kcpp_test_matrix_rw
	# from . import kcpp_bang

	# from . import kcpp_def_biped_bang

	# from . import read_Mux_KC

	# from . import imu_settings
	# from . import async_logger

	# from . import kc_change_addr
	# from . import scheduler_scan
	# from . import kcpp_def_biped17_bang

	# from . import gltf
	# from . import gltf_bang

	#from . import joint
	from . import armature_parse

else:
	try:
		import importlib
		importlib.import_module("."+sys.argv[1], package="scratchpad")
	except ModuleNotFoundError as e:
		print("ERROR: There's no target in the scratchpad called:", sys.argv[1])
