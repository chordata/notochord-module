from os import get_blocking
import notochord
from notochord.config import Notochord_Config, Output_Sink
from notochord.communicator import transmit, flush
from pygltflib import *
from notochord.armature import *
from notochord.math import Vector, Quaternion, Matrix3, Matrix4
from os.path import join, realpath, dirname

# Load glTF file
path = join(dirname(realpath(__file__)),'../data/avatar.gltf')
gltf = GLTF2.load(path)

bones = []

# Utility functions
def get_node_by_name(name):
    for node in gltf.nodes:
        if node.name == name:
            return node
    return None

def node(id):
    if (isinstance(id, str)):
        return get_node_by_name(id)
    return gltf.nodes[id]

def name(id):
    return node(id).name

def children(id):
    children = []
    if node(id).children is None:
        return children

    for child in node(id).children:
        children.append(node(child))
    return children

def get_parent(node_id):
    for _node in gltf.nodes:
        if _node.children is not None:
            for child_id in _node.children:
                if child_id == node_id:
                    return _node.name
    return -1

def get_bone_by_name(name):
    for bone in bones:
        if bone.label == name:
            return bone
    return None

def vector(list):
    return Vector(list[0], list[1], list[2])

def quaternion(list):
    if list is None:
        return Quaternion(1, 0, 0, 0)
    return Quaternion(list[3], list[0], list[1], list[2])

# ----------------------------------------------------- #
# ----------------------- Setup ----------------------- #
# ----------------------------------------------------- #

translation = vector(node(18).translation)
translationMatrix = Matrix4(1, 0, 0, translation.x,
                            0, 1, 0, translation.y,
                            0, 0, 1, translation.z,
                            0, 0, 0, 1)

rotationMatrix3 = quaternion(node(18).rotation).toRotationMatrix()
rotationMatrix = Matrix4(rotationMatrix3[0,0], rotationMatrix3[0,1], rotationMatrix3[0,2], 0,
                         rotationMatrix3[1,0], rotationMatrix3[1,1], rotationMatrix3[1,2], 0,
                         rotationMatrix3[2,0], rotationMatrix3[2,1], rotationMatrix3[2,2], 0,
                         0, 0, 0, 1) 

localTransform = translationMatrix * rotationMatrix

bones.append(Bone(None, "base", rotationMatrix))
#bones[0].update(True)

for id in gltf.skins[0].joints[1:]:

    parent = get_bone_by_name(get_parent(id))
    if parent is None:        
        raise Exception("Parent not found")

    translation = Vector(vector(node(id).translation))
    translationMatrix = Matrix4(1, 0, 0, translation.x,
                                0, 1, 0, translation.y,
                                0, 0, 1, translation.z,
                                0, 0, 0, 1)
    rotationMatrix3 = quaternion(node(id).rotation).toRotationMatrix()
    rotationMatrix = Matrix4(rotationMatrix3[0,0], rotationMatrix3[0,1], rotationMatrix3[0,2], 0,
                             rotationMatrix3[1,0], rotationMatrix3[1,1], rotationMatrix3[1,2], 0,
                             rotationMatrix3[2,0], rotationMatrix3[2,1], rotationMatrix3[2,2], 0,
                             0, 0, 0, 1) 

    localTransform = translationMatrix * rotationMatrix
                    
    bone = Bone(parent, name(id), localTransform)
    #bone.update(True)
    bones.append(bone)
    
bones[0].update(True)
from math import pi
# print global position of each bone
# get_bone_by_name("l-lowerarm").rotateX(pi/2)#set_global_rotation(Quaternion(0.7071068, 0.7071068, 0, 0))
for bone in bones:
    print(bone.label, bone.global_position)


notochord.init()
n = Notochord_Config()
n.comm.msg = Output_Sink.STDOUT
n.comm.port = 6564
n.comm.ip = "192.168.1.112"
n.comm.use_bundles = True
n.comm.log_level = 1
n.comm.adapter = '/dev/null'
notochord.setup(n) 

def transmit_all(bones):
    for bone in bones:
        transmit("/%/{}".format(bone.label), bone.global_position)
    flush()

transmit_all(bones)

# ----------------------------------------------------- #
# ----------------------- Calib ----------------------- #
# ----------------------------------------------------- #
input("Press enter to start calibration")
import copp_server
from time import sleep
sleep(2)

buffers = {}
diffs  = {}
for bone in bones:
    buffers[bone.label] = []
    diffs[bone.label] = Quaternion()
    
# Get the rotation difference between the two quaternions
def get_difference(q1, q2):
    return q1.inverse() * q2

server = copp_server.COPP_Server( port = 6565 )
server.receive_timeout = 0.1 # this speeds up the teardown wait

# add some COPP_Server types for convenience
server.COPP_Lazy_Packet = copp_server.COPP_Lazy_Packet
server.COPP_Common_packet = copp_server.COPP_Common_packet
time = 0
server.start()
calib = False
while calib and time < 3:
    sleep(0.05)
    time += 0.05
    for packet in server.get():
		#Common_packet is created when the messages arrive outside bundles
        assert isinstance(packet, server.COPP_Lazy_Packet)
        num = 0
        for msg in packet.get():
            print("buffer for {:15s}: {:8d}".format(msg.subtarget, len(buffers[msg.subtarget])))
            num += 1
            if msg.typetags == ',ffff':
                buffers[msg.subtarget].append(msg.payload)
        
        print("Received {} messages".format(num))

def average(buffer):
    # print("buffer length", len(buffer))
    # w_avg = 0
    # x_avg = 0
    # y_avg = 0
    # z_avg = 0
    # for q in buffer:
    #     w_avg += q[0]
    #     x_avg += q[1]
    #     y_avg += q[2]
    #     z_avg += q[3]
    # w_avg /= len(buffer)
    # x_avg /= len(buffer)
    # y_avg /= len(buffer)
    # z_avg /= len(buffer)
    # return Quaternion(w_avg, x_avg, y_avg, z_avg)

    q = averageQuaternions(quat_queue_to_Nx4matrix(buffer))
    return Quaternion(q[0], q[1], q[2], q[3])
import numpy
def quat_queue_to_Nx4matrix(quats):
  pre_matrix = [tuple(q) for q in quats]
  return numpy.array(pre_matrix)

import numpy.matlib as npm

# Q is a Nx4 numpy matrix and contains the quaternions to average in the rows.
# The quaternions are arranged as (w,x,y,z), with w being the scalar
# The result will be the average quaternion of the input. Note that the signs
# of the output quaternion can be reversed, since q and -q describe the same orientation
def averageQuaternions(Q):
    # Number of quaternions to average
    M = Q.shape[0]
    A = npm.zeros(shape=(4,4))

    for i in range(0,M):
        q = Q[i,:]
        # multiply q with its transposed version q' and add A
        A = numpy.outer(q,q) + A

    # scale
    A = (1.0/M)*A
    # compute eigenvalues and -vectors
    eigenValues, eigenVectors = numpy.linalg.eig(A)
    # Sort by largest eigenvalue
    eigenVectors = eigenVectors[:,eigenValues.argsort()[::-1]]
    # return the real part of the largest eigenvector (has only real part)
    return numpy.real(eigenVectors[:,0].A1)



# for bone in bones:
#     # if bone.label in ("lumbar", "head", "l-clavicle", "r-clavicle"): continue
#     quat = get_bone_by_name(bone.label).global_rotation#.inverse()
#     print("Averaging {:15s}: {:8d}".format(bone.label, len(buffers[bone.label])))

#     if len(buffers[bone.label]) == 0:
#         diffs[bone.label] = Quaternion()
#     else:
#         diffs[bone.label] = quat.inverse() * average(buffers[bone.label])


# print("base global rotation",get_bone_by_name("base").global_rotation)
# print("base averaged sensor",average(buffers["base"]))
# print("base diff",diffs["base"])


#input("Press enter to continue")

# l_arm = []
# l_arm.append(get_bone_by_name("l-upperarm"))
# l_arm.append(get_bone_by_name("l-lowerarm"))
# l_arm.append(get_bone_by_name("l-hand"))

print("Calibration complete")

# ----------------------------------------------------- #
# ----------------------- Loop ----------------------- #
# ----------------------------------------------------- #
def set_rotation(bone, quaternion):
    bone.set_global_rotation(quaternion)
    # if bone.label == "base":
    #     set_rotation(get_bone_by_name("lumbar"), quaternion)

incr = 1
xrot = 0.6
time = 0
xdeg = 90
ydeg = 80
from math import pi, sin, cos
def q_from_axis_angle(degrees, axis):
    radians = degrees*pi/180
    w = cos(radians/2)
    x = axis[0]*sin(radians/2)
    y = axis[1]*sin(radians/2)
    z = axis[2]*sin(radians/2)
    return Quaternion(w, x, y, z)
while True:
    sleep(0.05)
    time += 0.05
    if True:#msg.subtarget == "erer":
        x = q_from_axis_angle(xdeg, (1,0,0))
        y = q_from_axis_angle(-ydeg/2, (0,1,0))
        set_rotation(get_bone_by_name("base"), x)
        #set_rotation(get_bone_by_name("dorsal"), y*x) 
        #set_rotation(get_bone_by_name("lumbar"), x)          
        xdeg += incr
        ydeg += incr
        if ydeg > 100:
            incr = -incr
        if ydeg < 75:
            incr = -incr  

    for packet in server.get():
		#Common_packet is created when the messages arrive outside bundles
        assert isinstance(packet, server.COPP_Lazy_Packet)
        for msg in packet.get():
            if msg.typetags == ',ffff':
                set_rotation(get_bone_by_name(msg.subtarget), Quaternion(msg.payload) * diffs[msg.subtarget])
                if True:#msg.subtarget == "erer":
                    x = q_from_axis_angle(xdeg, (1,0,0))
                    y = q_from_axis_angle(-ydeg/2, (0,1,0))
                    set_rotation(get_bone_by_name(msg.subtarget), x)
                    set_rotation(get_bone_by_name("dorsal"), y*x) 
                    #set_rotation(get_bone_by_name("lumbar"), x)          
                    #xdeg += incr
                    ydeg += incr
                    if ydeg > 100:
                        incr = -incr
                    if ydeg < 75:
                        incr = -incr     
                if msg.subtarget == "dorsal":  
                    pass  
                    x = q_from_axis_angle(xdeg, (1,0,0))
                    # y = q_from_axis_angle(ydeg, (0,1,0))
                    # q = y * x
                    #set_rotation(get_bone_by_name(msg.subtarget), x)  
                    ydeg += incr
                    #print("base diff {:8.4f} {:8.4f} {:8.4f} {:8.4f}".format(diffs["base"][0], diffs["base"][1], diffs["base"][2], diffs["base"][3]))
    
    bone.update()
        #print("transmitting")
    transmit_all(bones)

            #     num += 1
            #     if msg.typetags == ',ffff':
            #         buffers[msg.subtarget].append(msg.payload)
            



            #     if msg.subtarget == "kc_0x40branch4":
            #     if msg.subtarget == "kc_0x41branch4":
            #         set_rotation(l_arm, Quaternion(msg.payload) * b_diff, 1)
            #     if msg.subtarget == "kc_0x43branch4":
            #         set_rotation(l_arm, Quaternion(msg.payload) * c_diff, 2)

# notochord.end()
server.close_and_wait()