from os import path
import csv
from notochord.fusion import Kalman
from notochord.math import Quaternion
from math import isclose


csv_filename = "KC_DUMP_flat_on_table_mag_nord_calibrated_100Hz.csv"
csv_filepath = path.realpath(path.join(path.dirname(__file__),"../data/"+csv_filename))

K = Kalman(100, 1, sensor_axis = 'RAW')
SENSITIVITY_ACCELEROMETER_8 = 0.000244
SENSITIVITY_MAGNETOMETER_4 = 0.00014 
SENSITIVITY_GYROSCOPE_2000 = 0.07 
K.set_sensor_res(SENSITIVITY_GYROSCOPE_2000, SENSITIVITY_ACCELEROMETER_8, SENSITIVITY_MAGNETOMETER_4)

def quat_dot(left, right):            
    return left.x * right.x + left.y * right.y + left.z * right.z + left.w * right.w
  
K.m_offset = (0,0,0)
K.m_matrix = ((1,0,0),(0,1,0),(0,0,1))

with open(csv_filepath) as csvfile:
    reader = csv.DictReader(csvfile)
    with open('/tmp/notochord_kalman_test_out.csv','w') as dest_csv:
        writer = csv.writer(dest_csv)
        writer.writerow(['q_w','q_x','q_y','q_z','n_w','n_x','n_y','n_z','dot'])
        counter = 0
        for row in reader:
            if row['node_label'] == "kc-CH_2-0":
                counter +=1
                recorded_result = (row['q_w'], row['q_x'], row['q_y'], row['q_z'])
                recorded_result = tuple(float(val) for val in recorded_result)
                K.run(float(row['g_x']), float(row['g_y']), float(row['g_z']),
                        float(row['a_x']), float(row['a_y']), float(row['a_z']),
                        float(row['m_x']), float(row['m_y']), float(row['m_z']))

                K.mag_calib(float(row['m_x']), float(row['m_y']), float(row['m_z']))
                
                if counter < 500 or counter % 4 != 0: continue
                
                dot = quat_dot(K.q, Quaternion(*recorded_result))
                
                print("({:4d}) DOT:{} Kalman quat: {} | CSV quat: {}".format(counter, dot, tuple(K.q), recorded_result))
                writer.writerow(recorded_result+tuple(K.q)+(dot,) )
    
                # assert K.m_offset == (0,0,0)
                # assert K.m_matrix == [[1,0,0],[0,1,0],[0,0,1]]
                
print("\n=== Offset ===")                
print(K.m_offset)
print("\n=== Matrix ===")                
print(K.m_matrix[0])
print(K.m_matrix[1])
print(K.m_matrix[2])
