from sys import path
from os.path import join, realpath, dirname
path.append(join(dirname(realpath(__file__)), "../../dist"))
path.append(join(dirname(realpath(__file__)), "../modules"))
