import notochord
from notochord.fusion import Kalman
from notochord.node import Mux, KCeptor, Mux_Channel
from notochord.config import Notochord_Config

from os.path import join, realpath, dirname
import csv

notochord.init()
conf = Notochord_Config()
conf.comm.adapter = '/dev/null'
conf.comm.log_level = 1
notochord.configure(conf)

mux = Mux(None, 0x73, "test_mux")
branch_2 = mux.get_branch(Mux_Channel.CH_2)
mux.bang_branch(Mux_Channel.CH_2)
kc = KCeptor(branch_2, 0x02, "test_kc")

# KCeptor Kalman
k_kc = kc.kalman
# k_kc = Kalman(50, 1, sensor_axis = 'LSM9DS1',  enable_mag_correct = True)

# autonomous kalman
K = Kalman(50, 1, sensor_axis = 'RAW',  enable_mag_correct = True)

print("Initial matrix/ offset")
print(K.m_offset)
print(K.m_matrix)
print("Initial mag covariance:", K.mag_covariance )

csv_file = join(dirname(realpath(__file__)),'../data/2021_10_22_notochord_calib_module_dump.csv')

with open(csv_file, newline='') as csvfile:
	reader = csv.reader(csvfile)
	next(reader) #skip header
	count = 0
	for row in reader:
		vals = [int(num) for num in row[1:-1]]
		vals.append(float(row[-1]))
		K.set_gyro(vals[0],vals[1],vals[2])
		K.set_accel(vals[3],vals[4],vals[5])
		res = K.mag_calib(vals[6],vals[7],vals[8])
		cov = K.mag_covariance
		
		k_kc.set_gyro(vals[0],vals[1],vals[2])
		k_kc.set_accel(vals[3],vals[4],vals[5])
		kc_res = k_kc.mag_calib(vals[6],vals[7],vals[8])
		kc_cov = k_kc.mag_covariance
	
		diff_cov = abs(cov - kc_cov)

		if kc_res == res:
			ok_run = "OK"
		else:
			ok_run = "-- NO!!"
   
 
		print("CALC = buf:{:4d}, cov: {: 10.2f}, run {} | ORIGINAL buf:{:4d}, cov: {: 10.2f}, run {} | DIFF = cov: {: 6.2f} sync_run {} ".format(K.mag_calib_buf_count,
																	 cov, res,
																	 k_kc.mag_calib_buf_count, kc_cov, kc_res,
																	 diff_cov, ok_run))
		# assert diff_cov < 0.01
		count += 1
		if count % 50 == 0: 
			print(k_kc.m_offset)
			print(k_kc.m_matrix)
			input("press ENTER TO CONTINUE")
		

#         K.set_accel(a_raw_read[X_AXIS],a_raw_read[Y_AXIS],a_raw_read[Z_AXIS]);
#     bool calib_result = kalman->set_mag_run_calib(imu.mx, imu.my, imu.mz);

#     if (calib_result){
#         return kalman->get_m_covariance();

# 		# print(converted)
# 		K.mag_calib(*converted)
# 		print(K.mag_buf_count)


# print("After some calibration matrix/ offset")
# print(K.m_offset)
# print(K.m_matrix)

# print(len(tuple(K.mag_buffer)))

# print(list(K.mag_buffer))


# def some_funct():
# 	for i in range(10):
# 		yield i


# for u in some_funct():
# 	print(u)