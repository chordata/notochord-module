import copp_server
from copp_server import defaults

PORT = 6565


server = copp_server.COPP_Server( port = PORT )
server.receive_timeout = 0.1 # this speeds up the teardown wait

# add some COPP_Server types for convenience
server.COPP_Lazy_Packet = copp_server.COPP_Lazy_Packet
server.COPP_Common_packet = copp_server.COPP_Common_packet
server.start()
aux = 0
while True:
    for packet in server.get():        
        q_num = 0
        r_num = 0
        q_buffer = []
        r_buffer = []
        #Common_packet is created when the messages arrive outside bundles
        assert isinstance(packet, server.COPP_Lazy_Packet)
        
        if packet.target == defaults.DataTarget.Q:
            for msg in packet.get():
                q_num += 1
                
                if msg.subtarget not in q_buffer:
                    q_buffer.append(msg.subtarget)
                else:
                    print("Q message repeated")
                    print(msg.subtarget)
        if packet.target == defaults.DataTarget.RAW:
            for msg in packet.get():
                r_num += 1

                if msg.subtarget not in r_buffer:
                    r_buffer.append(msg.subtarget)
                else:
                    print("RAW message repeated")
                    print(msg.subtarget)

        if q_num > 15 or r_num > 15:
            print("WARNING: Too many messages in one packet!")
            print("q_num: ", q_num, "r_num: ", r_num)
            

        aux += 1
        if aux > 400:
            print("Server is still alive ", q_num, r_num)
            aux = 0
        


server.close_and_wait()