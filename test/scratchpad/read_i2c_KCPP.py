"""
Utility program, part of KCeptor++ firmware
perform a number of i2c read/writes to test KCpp's i2c interface.

"""

from smbus2 import SMBus, i2c_msg, I2cFunc
from time import sleep
from random import choice
import functools
from collections import OrderedDict
from sys import exit
import random
import struct
import math
import time
import json

# from osc4py3 import oscbuildparse as oscparse
import socket

import notochord
from notochord.fusion import Kalman

import argparse

# parser = argparse.ArgumentParser(description='Test KCeptor++ i2c.')
# parser.add_argument('--flash',  action='store_true',
#                     default=False,
#                     help='Test the Flash memory functionalities. Warning! this will delete any previous calibration on the KCeptor++!')
# parser.add_argument('--loop',  action='store_true',
#                     default=False,
#                     help='perform the whole procedure in a loop')

# args = parser.parse_args()

g_calibrator = notochord.utils.Offset_Calibrator()
a_calibrator = notochord.utils.Offset_Calibrator()

K = Kalman(119, 8, sensor_axis = 'LSM9DS1')

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
port = 6565
server_address = ('10.42.0.1', port)

i2cbus = SMBus(1)  # Create a new I2C bus
KC_i2caddress = 0x47
HUB_i2caddress = 0x70

SENSITIVITY_ACCELEROMETER_2 = 0.000061
SENSITIVITY_ACCELEROMETER_4 = 0.000122
SENSITIVITY_ACCELEROMETER_8 = 0.000244
SENSITIVITY_ACCELEROMETER_16 = 0.000732
SENSITIVITY_GYROSCOPE_245 = 0.00875
SENSITIVITY_GYROSCOPE_500 = 0.0175
SENSITIVITY_GYROSCOPE_2000 = 0.07
SENSITIVITY_MAGNETOMETER_4 = 0.00014
SENSITIVITY_MAGNETOMETER_8 = 0.00029
SENSITIVITY_MAGNETOMETER_12 = 0.00043
SENSITIVITY_MAGNETOMETER_16 = 0.00058


class KCpp_i2c_mode:
	# /*######## REGISTERS #######*/
	READ_AUTOINCREMENT 	= 0x00
	READ_WORD_GX		= 0x01
	READ_WORD_GY		= 0x02
	READ_WORD_GZ		= 0x03
	READ_WORD_AX		= 0x04
	READ_WORD_AY		= 0x05
	READ_WORD_AZ		= 0x06
	READ_WORD_MX		= 0x07
	READ_WORD_MY		= 0x08
	READ_WORD_MZ		= 0x09
	READ_PAYLOAD_DMA	= 0x0A
	
	# /*--------- IDLE ---------*/
	I2C_MODE_IDLE		= 0x0F

	# /*--------- CONF ---------*/
	CONF_READ_MODE		= 0x10 
	CONF_SET_MODE		= 0x11

	# /*########## VALUES #########*/
	# /*--------- LSM9DS1 ---------*/
	G_SCALE				= 0x12
	A_SCALE				= 0x13
	M_SCALE				= 0x14
	RATE				= 0x15

	FETCH_AG_REG		= 0x1A
	FETCH_M_REG			= 0x1B

	INIT_IMU			= 0x1F


	#/*--------- Calibration ---------*/

	SET_CALIB_GX_L		= 0x21
	SET_CALIB_GX_H		= 0x22
	SET_CALIB_GY_L		= 0x23
	SET_CALIB_GY_H		= 0x24
	SET_CALIB_GZ_L		= 0x25
	SET_CALIB_GZ_H		= 0x26
	SET_CALIB_AX_L		= 0x27
	SET_CALIB_AX_H		= 0x28
	SET_CALIB_AY_L		= 0x29
	SET_CALIB_AY_H		= 0x2A
	SET_CALIB_AZ_L		= 0x2B
	SET_CALIB_AZ_H		= 0x2C
	SET_CALIB_MX_L		= 0x2D
	SET_CALIB_MX_H		= 0x2E
	SET_CALIB_MY_L		= 0x2F
	SET_CALIB_MY_H		= 0x30
	SET_CALIB_MZ_L		= 0x31
	SET_CALIB_MZ_H		= 0x32

	CALIB_OFFSET		= 0x3A
	CALIB_MATRIX		= 0x3B
	CALIB_TIMESTAMP		= 0x3C

	STORE_CALIB			= 0x3F

	# /*--------- I2C ADDR! ---------*/
	SET_I2C_ADDR		= 0xA0

	# /*--------- ERRORS ---------*/

	GET_LAST_ERRNO		= 0xE0
	GET_NUM_ERRORS		= 0xE1


class KCpp_Error:
	E_NONE 					= 0x00
	E_LSM9DS1_NOT_FOUND		= 0x01
	E_WRONG_LSM9DS1_CONF	= 0x02
	E_SPI_ERROR				= 0x10
	E_SPI_TIMEOUT			= 0x11
	E_SPI_BUSY				= 0x10

	E_I2C_ERROR				= 0x20 #internal i2c Error
	E_I2C_BUSERROR			= 0x21 #i2c bus error
	E_I2C_LOGIC				= 0x23 #i2c logic error
	E_I2C_DMA				= 0x24 #i2c DMA error
	E_I2C_INCOMPLETE		= 0x25 #i2c Incomplete reading

	# /*----------  I2C master errors  ----------*/
	
	E_I2C_MASTER_NO_W		= 0x32 #No register write received,
	E_I2C_WRONG_DIR			= 0x33 #R/W direction not matching comand
	E_I2C_WRONG_REGISTER	= 0x34 #Not valid register
	E_I2C_WRONG_VALUE		= 0x35 #Not valid configuration value

	@classmethod
	def get_code_name(cls, code):
		for name in cls.__dict__:
			if code == cls.__dict__[name]:
				return "(0x{:0x}) {}".format(code, name)

		return "(0x{:0x}) {}".format(code, "UNKNOWN")


def write_conf(cmd, value):
	print("     write [{:2x}]({:2x}, {:2x}) ".format(KC_i2caddress, cmd, value))
	write = i2c_msg.write(KC_i2caddress, [ cmd, value ])
	i2cbus.i2c_rdwr(write)

def read_conf(cmd):
	print("     read [{:2x}]({:2x}) ".format(KC_i2caddress, cmd))
	write = i2c_msg.write(KC_i2caddress, [ cmd ])
	read = i2c_msg.read(KC_i2caddress, 1)
	i2cbus.i2c_rdwr(write, read)
	return list(read)

def read_word(cmd):
	print(" word read [{:2x}]({:2x}) ".format(KC_i2caddress, cmd))
	write = i2c_msg.write(KC_i2caddress, [ cmd ])
	read = i2c_msg.read(KC_i2caddress, 2)
	i2cbus.i2c_rdwr(write, read)
	return list(read)

def read_autoinc():
	reg = 0x00
	write = i2c_msg.write(KC_i2caddress, [reg,])
	read = i2c_msg.read(KC_i2caddress, 18)
	i2cbus.i2c_rdwr(write, read)
	data = ["{:2x}".format(val) for val in list(read)]
	print("INCREM  ", data)
	return list(read)

def read_autoinc_incomplete():
	reg = 0x00
	write = i2c_msg.write(KC_i2caddress, [reg,])
	read = i2c_msg.read(KC_i2caddress, 10)
	i2cbus.i2c_rdwr(write, read)
	data = ["{:2x}".format(val) for val in list(read)]
	print("INCRE ERR", data)
	return list(read)

def read_DMA():
	get_set_conf = 0x0A
	write = i2c_msg.write(KC_i2caddress, [get_set_conf,])
	read = i2c_msg.read(KC_i2caddress, 18)
	i2cbus.i2c_rdwr(write, read)
	return tuple(read)
	
def read_DMA_NOWRITE():
	read = i2c_msg.read(KC_i2caddress, 18)
	i2cbus.i2c_rdwr(read)
	return tuple(read)

def get_payload():
	res = read_DMA()
	# data = ["{:2x}".format(val) for val in res]
	# print("DMA     ", data)

	return struct.unpack("<hhhhhhhhh", bytearray(res))

	

if (i2cbus._get_funcs() & I2cFunc.I2C):
	is_initiated = False
	try:
		# with open("calib.json", "r") as f:
		# 	data = f.read()
		# 	data = json.loads(data)

		# gyro_offset = data["gyro_offset"]
		# acel_offset = data["acel_offset"]

		# K.mag_offset = tuple(data["mag_offset"])
		# K.mag_matrix = tuple(tuple(n for n in row) for row in data["mag_matrix"] ) 

		write = i2c_msg.write(HUB_i2caddress, [ 0xff ])
		i2cbus.i2c_rdwr(write)

		response = read_conf(KCpp_i2c_mode.INIT_IMU)
		print("IMU INITIATED: {} ".format(bool(response[0])))

		print("## ----- Write CONF ------")

		regs =  {	"G_SCALE": (0x12,2), 
					"A_SCALE": (0x13,3), 
					"M_SCALE": (0x14,0),
					"RATE": (0x15,1) 
				}

		for k in regs.keys():
			to_write = regs[k]
			write_conf(to_write[0], to_write[1])
			print("WRITE REG {} value: {}".format(k, to_write[1]))

		while not is_initiated:
			write_conf(KCpp_i2c_mode.INIT_IMU, 1)
			sleep(0.2)  

			response = read_conf(KCpp_i2c_mode.INIT_IMU)
			is_initiated = bool(response[0])
			print("IMU INITIATED: {} ".format(is_initiated))



		# write = i2c_msg.write(KC_i2caddress, [ KCpp_i2c_mode.CALIB_OFFSET  ])
		# read = i2c_msg.read(KC_i2caddress, 18)
		# i2cbus.i2c_rdwr(write, read)
		# read_vals = ["{:2x}".format(val) for val in list(read)]
		# print("Calib offset in flash memory :\n", read_vals)

		# write = i2c_msg.write(KC_i2caddress, [ KCpp_i2c_mode.CALIB_MATRIX  ])
		# read = i2c_msg.read(KC_i2caddress, 9*4)
		# i2cbus.i2c_rdwr(write, read)
		# packed_values = struct.unpack("<fffffffff", bytearray(list(read)))
		# print("Calib matrix in flash memory :\n", packed_values)
		


		input("Press a key to start reading")

		last_read = time.time()
		target_period = 1/119
		secs_sleep = target_period
		time_correction = target_period / 100
		counter = 0

		while(1):
			unpacked_payload = get_payload()
			formated_payload = ["{:6d}".format(val) for val in unpacked_payload] 
			print(formated_payload)

			sleep(0.02)
			notochord.io.test_i2c_rdwr()
			# gyro_read = (unpacked_payload[0] - gyro_offset[0], unpacked_payload[1] - gyro_offset[1], unpacked_payload[2] - gyro_offset[2])
			# acel_read = (unpacked_payload[3] - acel_offset[0], unpacked_payload[4] - acel_offset[1], unpacked_payload[5] - acel_offset[2])

			
			# K.run(gyro_read[0], gyro_read[1], gyro_read[2],
			# 	acel_read[0], acel_read[1], acel_read[2],
			# 	unpacked_payload[6], unpacked_payload[7], unpacked_payload[8])

			# print(K.q)

			# test_timetag = time.time()
			# test_timetag = oscparse.unixtime2timetag( 0.001 * test_timetag )
			# first_msg_addr = "/%%/Chordata/q"
			# msgs = [oscparse.OSCMessage(first_msg_addr, ",N", [None]), oscparse.OSCMessage("/%/kc++", ',ffff', tuple(K.q))]
			# bun = oscparse.OSCBundle(test_timetag, msgs)
			# raw_bun = oscparse.encode_packet(bun)
			
			# sock.sendto(raw_bun, server_address)


			sleep(secs_sleep)

			real_period = time.time() - last_read
			last_read = time.time()

			
			if real_period > target_period:
				secs_sleep -= time_correction
				# print("(-) decrement period, freq=", 1/real_period)
			elif real_period < target_period:
				secs_sleep += time_correction
				# print("(+) increment period, freq=", 1/real_period)




	finally:
		# print("calib Gyro:", g_calibrator.result)
		# print("calib Acel:", a_calibrator.result)
		# print("calib mag:", K.mag_offset)
		# print("calib mag Matrix:", K.mag_matrix)

		# import json

		# _json_str = json.dumps({"gyro_offset": g_calibrator.result,
		# 	"acel_offset" : a_calibrator.result,
		# 	"mag_offset" : K.mag_offset,
		# 	"mag_matrix" : K.mag_matrix})

		# with open("calib.json", "w") as f:
		# 	f.write(_json_str)
		# 	
		# sock.close()

		while is_initiated:
			write_conf(KCpp_i2c_mode.INIT_IMU, 0)

			sleep(0.2)  
			response = read_conf(KCpp_i2c_mode.INIT_IMU)

			is_initiated = bool(response[0])
			print("IMU INITIATED: {} ".format(is_initiated))


# if (i2cbus._get_funcs() & I2cFunc.I2C):
# 	while(1):
# 		try:
# 			print("## ----- Read ADDR ------")
			
# 			response = read_conf(KCpp_i2c_mode.SET_I2C_ADDR)
# 			print("Current i2c addr: {:0x} ".format(response[0]))   
# 			assert (response[0] == KC_i2caddress)
# 			# exit(0)

# 			print("## ----- Restart error queue ------")
# 			response = read_conf(KCpp_i2c_mode.GET_NUM_ERRORS)
# 			if response[0] > 0:
# 				write = i2c_msg.write(KC_i2caddress, [ KCpp_i2c_mode.GET_LAST_ERRNO ])
# 				read = i2c_msg.read(KC_i2caddress, response[0])
# 				i2cbus.i2c_rdwr(write, read)
# 				print("Error codes found: ({})".format(response[0]),[KCpp_Error.get_code_name(code) for code in list(read)] )

# 			print("## ----- Change ADDR ------")

# 			new_i2c_addr = 0x43
# 			write_conf(KCpp_i2c_mode.SET_I2C_ADDR, new_i2c_addr)
# 			sleep(0.1)

# 			KC_i2caddress = new_i2c_addr
# 			response = read_conf(KCpp_i2c_mode.SET_I2C_ADDR)
# 			print("New i2c addr: {:0x} ".format(response[0]))   
# 			assert response[0] == new_i2c_addr

# 			print("## ----- Write CONF ------")

# 			regs =  {	"G_SCALE": (0x12,1), 
# 						"A_SCALE": (0x13,1), 
# 						"M_SCALE": (0x14,2),
# 						"RATE": (0x15,0) 
# 					}

# 			for k in regs.keys():
# 				to_write = regs[k]
# 				write_conf(to_write[0], to_write[1])
# 				print("WRITE REG {} value: {}".format(k, to_write[1]))


# 			print("## ----- Read CONF ------")

# 			for k in regs.keys():
# 					response = read_conf(regs[k][0])
# 					print("REG {} value: {}".format(k, response[0]))
# 					assert( regs[k][1] == response[0])

# 			# exit(0)

# 			print("## ----- INIT IMU ------ (after setting IMU conf params)") 

# 			response = read_conf(KCpp_i2c_mode.INIT_IMU)
# 			print("IMU INITIATED: {} ".format(bool(response[0])))  

# 			is_initiated = False
# 			while not is_initiated:
# 				write_conf(KCpp_i2c_mode.INIT_IMU, 1)
# 				sleep(0.2)  

# 				response = read_conf(KCpp_i2c_mode.INIT_IMU)
# 				is_initiated = bool(response[0])
# 				print("IMU INITIATED: {} ".format(is_initiated))


# 			print("## ----- Verify CONF in IMU regs ------")

# 			fetchs = OrderedDict()

# 			fetchs["CTRL_REG_1_G"]  =  (KCpp_i2c_mode.FETCH_AG_REG, 0x10, 0x48)
# 			fetchs["CTRL_REG_6_XL"] = (KCpp_i2c_mode.FETCH_AG_REG, 0x20, 0x48)
# 			fetchs["CTRL_REG_2_M"]  = (KCpp_i2c_mode.FETCH_M_REG, 0x21, 0x40)

# 			for k, reg_data in fetchs.items():
# 				print("Fetching IMU REG", k)
# 				write_conf(reg_data[0], reg_data[1])

# 				# sleep(0.1) 

# 				response = read_conf(reg_data[0])
# 				print("FETCHED IMU REG {} value: 0x{:0x}".format(k, response[0]))
# 				assert response[0] == reg_data[2]
				
# 				# sleep(0.1) 


# 			print("## Read Autoincrement")
# 			result = read_autoinc() 
# 			while functools.reduce(lambda a,b : a+b, result) == 0:
# 				result = read_autoinc()
# 				sleep(0.1) 

# 			for i in range(10):
# 				read_autoinc() 
# 				sleep(1/50)

# 			print("## Read DMA")

# 			read_DMA()

# 			# print("## Read DMA no write frame")
# 			count_errors = 0
# 			for i in range(50):
# 				read_DMA_NOWRITE()
# 				sleep(1/50)

# 				if i == 25 or random.random()>0.8: 
# 					print("## make an error (DMA incoplete)")
# 					read_DMA_incomplete()
# 					read_DMA()
# 					count_errors +=1

# 				if i == 10 or random.random()>0.9:
# 					print("## make an error (Wrong reg)")
# 					write_conf(0xEF, 1) #Wrong reg
# 					count_errors +=1
# 					read_DMA()


# 			print("## Get Errors")
# 			response = read_conf(KCpp_i2c_mode.GET_NUM_ERRORS)
# 			print("Errors found: {} ".format(response[0]))
# 			count_errors = min(count_errors, 50)
# 			assert response[0] == count_errors

# 			print(" ---- Make some errors")
# 			error_codes = []
# 			write_conf(0xEF, 1) #Wrong reg
# 			error_codes.append(KCpp_Error.E_I2C_WRONG_REGISTER)
# 			write_conf(regs["G_SCALE"][0], 0xEF) #Wrong value
# 			error_codes.append(KCpp_Error.E_I2C_WRONG_VALUE)
# 			write_conf(regs["A_SCALE"][0], 0xEF) #Wrong value
# 			error_codes.append(KCpp_Error.E_I2C_WRONG_VALUE)
# 			read_with_err = i2c_msg.read(KC_i2caddress, 1)
# 			i2cbus.i2c_rdwr(read_with_err) #No master write
# 			error_codes.append(KCpp_Error.E_I2C_MASTER_NO_W)
# 			read_DMA_incomplete()
# 			error_codes.append(KCpp_Error.E_I2C_INCOMPLETE)
# 			read_autoinc_incomplete()
# 			error_codes.append(KCpp_Error.E_I2C_INCOMPLETE)

# 			error_codes.reverse() # we should get the last error first

# 			response = read_conf(KCpp_i2c_mode.GET_NUM_ERRORS)
# 			expected_error_num = min(len(error_codes) + count_errors, 50)
# 			print("Errors found: {} | expected {}".format(response[0], expected_error_num))

# 			write = i2c_msg.write(KC_i2caddress, [ KCpp_i2c_mode.GET_LAST_ERRNO ])
# 			read = i2c_msg.read(KC_i2caddress, response[0])
# 			i2cbus.i2c_rdwr(write, read)
# 			last_errors_fetched = list(read)[:len(error_codes)] # trim to only the recent errors after "Make some errors"

# 			print("Last error codes found: ",[KCpp_Error.get_code_name(code) for code in last_errors_fetched] )
# 			print("Last error codes expec: ",[KCpp_Error.get_code_name(code) for code in error_codes] )
			
# 			assert response[0] == expected_error_num 
# 			assert error_codes == last_errors_fetched

# 			print("## READ words")
# 			read_DMA()
# 			words = []
# 			for i in range(KCpp_i2c_mode.READ_WORD_GX, KCpp_i2c_mode.READ_WORD_MZ):
# 				words += read_word(i)

# 			data = ["{:2x}".format(val) for val in words]
# 			print("WORDS  ", data)


# 			if args.flash:
# 				print("## Get previous calibration")
# 				write = i2c_msg.write(KC_i2caddress, [ KCpp_i2c_mode.CALIB_TIMESTAMP  ])
# 				read = i2c_msg.read(KC_i2caddress, 4)
# 				i2cbus.i2c_rdwr(write, read)
# 				unpacked_read_timestamp = struct.unpack("<f", bytearray(list(read)))[0]
# 				print("Timestamp in flash memory: {:.6f}".format(unpacked_read_timestamp))

# 				write = i2c_msg.write(KC_i2caddress, [ KCpp_i2c_mode.CALIB_OFFSET  ])
# 				read = i2c_msg.read(KC_i2caddress, 18)
# 				i2cbus.i2c_rdwr(write, read)
# 				read_vals = ["{:2x}".format(val) for val in list(read)]
# 				print("Calib offset in flash memory :\n", read_vals)

# 				write = i2c_msg.write(KC_i2caddress, [ KCpp_i2c_mode.CALIB_MATRIX  ])
# 				read = i2c_msg.read(KC_i2caddress, 9*4)
# 				i2cbus.i2c_rdwr(write, read)
# 				packed_values = struct.unpack("<fffffffff", bytearray(list(read)))
# 				print("Calib matrix in flash memory :\n", packed_values)

# 			print("## Set Calibration random vals")

# 			read_DMA()

# 			now = time.time() + random.random()*9913245 #just to make it different from the prev one when running in loop
# 			now_packed = struct.pack("<f", now)

# 			write = i2c_msg.write(KC_i2caddress, [ KCpp_i2c_mode.CALIB_TIMESTAMP ] + list(now_packed) )
# 			i2cbus.i2c_rdwr(write)

# 			write = i2c_msg.write(KC_i2caddress, [ KCpp_i2c_mode.CALIB_TIMESTAMP  ])
# 			read = i2c_msg.read(KC_i2caddress, 4)
# 			i2cbus.i2c_rdwr(write, read)

# 			unpacked_read_timestamp = struct.unpack("<f", bytearray(list(read)))[0]

# 			print("Now is: {:.6f}, set/read timestamp: {:.6f}".format(now, unpacked_read_timestamp))
# 			assert math.isclose(now, unpacked_read_timestamp, rel_tol=1e-6, abs_tol=0.0)

# 			calib_vals = []
# 			for i in range(KCpp_i2c_mode.SET_CALIB_GX_L, KCpp_i2c_mode.SET_CALIB_MZ_H + 1):
# 				the_val = random.randint(0, 0xFF)
# 				calib_vals.append(the_val)

# 			write = i2c_msg.write(KC_i2caddress, [ KCpp_i2c_mode.CALIB_OFFSET ] + calib_vals )
# 			i2cbus.i2c_rdwr(write)

# 			write = i2c_msg.write(KC_i2caddress, [ KCpp_i2c_mode.CALIB_OFFSET  ])
# 			read = i2c_msg.read(KC_i2caddress, len(calib_vals))
# 			i2cbus.i2c_rdwr(write, read)

# 			read_vals = ["{:2x}".format(val) for val in list(read)]
# 			calib_vals = ["{:2x}".format(val) for val in calib_vals]
# 			print("-- written calib offset:\n", calib_vals)
# 			print("-- read calib offset:\n", read_vals)
# 			assert read_vals == calib_vals

# 			response = read_conf(KCpp_i2c_mode.INIT_IMU)
# 			assert list(response)[0] == 1 #assert the IMU reading was restored

# 			# get list of uint8_t from float matrix
# 			calib_matrix = [1,0,0, 0,1,0, 0,0,1]
# 			calib_matrix = [((random.random()-0.5)*0.1) + _ for _ in calib_matrix]
# 			calib_vals = [struct.pack("<f", val) for val in calib_matrix]
# 			calib_vals = [unpacked for packed in calib_vals for unpacked in packed]

# 			write = i2c_msg.write(KC_i2caddress, [ KCpp_i2c_mode.CALIB_MATRIX ] + calib_vals )
# 			i2cbus.i2c_rdwr(write)

# 			write = i2c_msg.write(KC_i2caddress, [ KCpp_i2c_mode.CALIB_MATRIX  ])
# 			read = i2c_msg.read(KC_i2caddress, len(calib_vals))
# 			i2cbus.i2c_rdwr(write, read)

# 			packed_values = struct.unpack("<fffffffff", bytearray(list(read)))

# 			print("-- written calib matrix:\n", calib_matrix)
# 			print("-- read calib matrix:\n", packed_values)

# 			read_DMA()
# 			sleep(0.5)

# 			response = read_conf(KCpp_i2c_mode.INIT_IMU)
# 			assert list(response)[0] == 1 #assert the IMU reading was restored
 
# 			for i, num in enumerate(calib_matrix):
# 				assert math.isclose(num, packed_values[i], rel_tol=1e-6, abs_tol=0.0)

# 			if args.flash:
# 				# WRITE STORE_CALIB 1 = store
# 				# WRITE STORE_CALIB 0 = reset
# 				# READ STORE_CALIB = was last storing sucessfull? side effect reset this indicator
# 				write = i2c_msg.write(KC_i2caddress, [ KCpp_i2c_mode.STORE_CALIB, 1 ])
# 				i2cbus.i2c_rdwr(write)

# 				calibration_stored = 1
# 				while calibration_stored < 10:
# 					write = i2c_msg.write(KC_i2caddress, [ KCpp_i2c_mode.STORE_CALIB ])
# 					read = i2c_msg.read(KC_i2caddress, 1)
# 					i2cbus.i2c_rdwr(write, read)
# 					if list(read)[0] == 1:
# 						print("Check calibratrion stored in flash, attempt #", calibration_stored, "> SUCCESFUL!! <")
# 						calibration_stored = 0
# 						break
# 					print("Check calibratrion stored in flash, attempt #", calibration_stored, "UNSUCCESFUL")
# 					calibration_stored +=1	

# 				assert calibration_stored == 0

# 			print("## STOP IMU")
# 			while is_initiated:
# 				write_conf(KCpp_i2c_mode.INIT_IMU, 0)

# 				sleep(0.2)  
# 				response = read_conf(KCpp_i2c_mode.INIT_IMU)

# 				is_initiated = bool(response[0])
# 				print("IMU INITIATED: {} ".format(is_initiated))


		
# 		finally:
# 			print("## RESTORE I2C ADDR")
# 			new_i2c_addr = 0x40
# 			write_conf(KCpp_i2c_mode.SET_I2C_ADDR, new_i2c_addr)
# 			KC_i2caddress = new_i2c_addr


# 		if args.loop:
# 			sleep(0.2)
# 		else:
# 			break

else:
	#from https://www.kernel.org/doc/html/latest/i2c/dev-interface.html#full-interface-description
	print("The i2c adapter doesn't support RDWR")

