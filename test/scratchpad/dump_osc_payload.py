import copp_server

from copp_server.osc4py3_buildparse import oscbuildparse as oscparse

#based on fixtures from the COPP_server
# @pytest.fixture
# def OSC_payload():
# 	test_subtargets = ["comm", "comm"]
# 	payload = (',s', ["ABCDEF"])
# 	subtargets = ['/%/{}'.format(sub) for sub in test_subtargets]
# 	msgs = [oscparse.OSCMessage(sub, *payload) for sub in subtargets]
# 	return msgs, subtargets, payload

# @pytest.fixture
# def OSC_raw_bundle(OSC_payload):
# 	"""This fisture will output a raw byte sequence like this:
# 	000:2362756e 646c6500 83c288bb 67d59800     #bun dle. .... g...
# 	016:00000014 2f25252f 43686f72 64617461     .... /%%/ Chor data
# 	032:2f710000 2c4e0000 00000020 2f252f62     /q.. ,N.. ...  /%/b
# 	048:61736500 2c666666 66000000 3dcccccd     ase. ,fff f... =...
# 	064:3e4ccccd 3e99999a 3ecccccd 00000024     >L.. >... >... ...$
# 	080:2f252f64 6f727361 6c000000 2c666666     /%/d orsa l... ,fff
# 	096:66000000 3dcccccd 3e4ccccd 3e99999a     f... =... >L.. >...
# 	112:3ecccccd 00000020 2f252f68 65616400     >... ...  /%/h ead.
# 	128:2c666666 66000000 3dcccccd 3e4ccccd     ,fff f... =... >L..
# 	144:3e99999a 3ecccccd                       >... >...
# 	"""
# 	first_msg_addr = "/%%/Chordata/extra"
# 	first_msg = [oscparse.OSCMessage(first_msg_addr, ",N", [None])]
# 	import time

# 	bun = oscparse.OSCBundle(oscparse.OSC_IMMEDIATELY , first_msg + OSC_payload[0])
# 	raw = oscparse.encode_packet(bun)
# 	return raw


# m = oscparse.OSCMessage("/Chordata/raw/a-really-long-subtarget", ',iiiiiiiii', [0X11aAbBcC]*9)

m = oscparse.OSCMessage("/%/head", ',ffffii', [1,2,3,4,1,-1])


p = oscparse.encode_packet(m)

oscparse.dumphex_buffer(p)