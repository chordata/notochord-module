from os import get_blocking
import notochord
from notochord.config import Notochord_Config, Output_Sink
from notochord.communicator import transmit, flush
from pygltflib import *
from notochord.armature import *
from notochord.math import Vector, Quaternion, Matrix3, Matrix4
from os.path import join, realpath, dirname

# Load glTF file
path = join(dirname(realpath(__file__)),'../data/arm.gltf')
gltf = GLTF2.load(path)

bones = []

# Utility functions
def get_node_by_name(name):
    for node in gltf.nodes:
        if node.name == name:
            return node
    return None

def node(id):
    if (isinstance(id, str)):
        return get_node_by_name(id)
    return gltf.nodes[id]

def name(id):
    return node(id).name

def children(id):
    children = []
    if node(id).children is None:
        return children

    for child in node(id).children:
        children.append(node(child))
    return children

def get_parent(node_id):
    for _node in gltf.nodes:
        if _node.children is not None:
            for child_id in _node.children:
                if child_id == node_id:
                    return _node.name
    return -1

def get_bone_by_name(name):
    for bone in bones:
        if bone.label == name:
            return bone
    return None

def vector(list):
    return Vector(list[0], list[1], list[2])

def quaternion(list):
    if list is None:
        return Quaternion(1, 0, 0, 0)
    return Quaternion(list[3], list[0], list[1], list[2])

# ----------------------------------------------------- #
# ----------------------- Setup ----------------------- #
# ----------------------------------------------------- #

#translation = vector(node(2).translation)
rotationMatrix3 = quaternion(node(4).rotation).toRotationMatrix()
rotationMatrix = Matrix4(rotationMatrix3[0,0], rotationMatrix3[0,1], rotationMatrix3[0,2], 0,
                         rotationMatrix3[1,0], rotationMatrix3[1,1], rotationMatrix3[1,2], 0,
                         rotationMatrix3[2,0], rotationMatrix3[2,1], rotationMatrix3[2,2], 0,
                         0, 0, 0, 1) 

bones.append(Bone(None, "kc_0x40branch2", rotationMatrix))


for id in gltf.skins[0].joints[1:]:

    parent = get_bone_by_name(get_parent(id))
    if parent is None:        
        raise Exception("Parent not found")

    translation = Vector(vector(node(id).translation))
    translationMatrix = Matrix4(1, 0, 0, translation.x,
                                0, 1, 0, translation.y,
                                0, 0, 1, translation.z,
                                0, 0, 0, 1)
    rotationMatrix3 = quaternion(node(id).rotation).toRotationMatrix()
    rotationMatrix = Matrix4(rotationMatrix3[0,0], rotationMatrix3[0,1], rotationMatrix3[0,2], 0,
                             rotationMatrix3[1,0], rotationMatrix3[1,1], rotationMatrix3[1,2], 0,
                             rotationMatrix3[2,0], rotationMatrix3[2,1], rotationMatrix3[2,2], 0,
                             0, 0, 0, 1) 

    localTransform = translationMatrix * rotationMatrix
                    
    bone = Bone(parent, name(id), localTransform)
    #bone.update(True)
    bones.append(bone)

bones[0].update(True)
from math import pi
# print global position of each bone
# get_bone_by_name("l-lowerarm").rotateX(pi/2)#set_global_rotation(Quaternion(0.7071068, 0.7071068, 0, 0))
# bones[0].update()
for bone in bones:
    print(bone.label, bone.global_position)


notochord.init()
n = Notochord_Config()
n.comm.msg = Output_Sink.STDOUT
n.comm.port = 6564
n.comm.ip = "192.168.1.112"
n.comm.use_bundles = True
n.comm.log_level = 1
n.comm.adapter = '/dev/null'
notochord.setup(n) 

def transmit_all(bones):
    for bone in bones:
        transmit("/%/{}".format(bone.label), bone.global_position)
    flush()

transmit_all(bones)

# ----------------------------------------------------- #
# ----------------------- Calib ----------------------- #
# ----------------------------------------------------- #
input("Press enter to start calibration")
import copp_server
from time import sleep
sleep(2)

buffers = {}
diffs  = {}
for bone in bones:
    buffers[bone.label] = []
    diffs[bone.label] = Quaternion()
    
# Get the rotation difference between the two quaternions
def get_difference(q1, q2):
    return q1.inverse() * q2

server = copp_server.COPP_Server( port = 6565 )
server.receive_timeout = 0.1 # this speeds up the teardown wait

# add some COPP_Server types for convenience
server.COPP_Lazy_Packet = copp_server.COPP_Lazy_Packet
server.COPP_Common_packet = copp_server.COPP_Common_packet
time = 0
server.start()
while time < 3:
    sleep(0.05)
    time += 0.05
    for packet in server.get():
		#Common_packet is created when the messages arrive outside bundles
        assert isinstance(packet, server.COPP_Lazy_Packet)
        num = 0
        for msg in packet.get():
            #print("buffer for {:15s}: {:8d}".format(msg.subtarget, len(buffers[msg.subtarget])))
            num += 1
            if msg.typetags == ',ffff':
                buffers[msg.subtarget].append(msg.payload)
        
        #print("Received {} messages".format(num))

def average(buffer):
    print("buffer length", len(buffer))
    q = averageQuaternions(quat_queue_to_Nx4matrix(buffer))
    return Quaternion(q[0], q[1], q[2], q[3])
import numpy
def quat_queue_to_Nx4matrix(quats):
  pre_matrix = [tuple(q) for q in quats]
  return numpy.array(pre_matrix)

import numpy.matlib as npm

# Q is a Nx4 numpy matrix and contains the quaternions to average in the rows.
# The quaternions are arranged as (w,x,y,z), with w being the scalar
# The result will be the average quaternion of the input. Note that the signs
# of the output quaternion can be reversed, since q and -q describe the same orientation
def averageQuaternions(Q):
    # Number of quaternions to average
    M = Q.shape[0]
    A = npm.zeros(shape=(4,4))

    for i in range(0,M):
        q = Q[i,:]
        # multiply q with its transposed version q' and add A
        A = numpy.outer(q,q) + A

    # scale
    A = (1.0/M)*A
    # compute eigenvalues and -vectors
    eigenValues, eigenVectors = numpy.linalg.eig(A)
    # Sort by largest eigenvalue
    eigenVectors = eigenVectors[:,eigenValues.argsort()[::-1]]
    # return the real part of the largest eigenvector (has only real part)
    return numpy.real(eigenVectors[:,0].A1)

# a_diff = get_bone_by_name("l-upperarm").global_rotation.inverse() *  average(A_buffer)
# b_diff = get_bone_by_name("l-lowerarm").global_rotation.inverse() *  average(B_buffer)
# c_diff = get_bone_by_name("l-hand").global_rotation.inverse() *  average(C_buffer)


for bone in bones:
    # if bone.label in ("lumbar", "head", "l-clavicle", "r-clavicle"): continue
    quat = get_bone_by_name(bone.label).global_rotation.inverse()
    print("Averaging {:15s}: {:8d}".format(bone.label, len(buffers[bone.label])))

    if len(buffers[bone.label]) == 0:
        diffs[bone.label] = Quaternion()
    else:
        diffs[bone.label] = quat * average(buffers[bone.label])

print("Calibration complete")

# ----------------------------------------------------- #
# ----------------------- Loop ----------------------- #
# ----------------------------------------------------- #
def set_rotation(bone, quaternion):
    bone.set_global_rotation(quaternion)
    
    # if bone.label == "kc_0x40branch2":
    #     set_rotation(get_bone_by_name("kc_0x43branch2"), quaternion)

print("transmitting")

time = 0
while time < 1000:
    sleep(0.05)
    time += 0.05

    for packet in server.get():
		#Common_packet is created when the messages arrive outside bundles
        assert isinstance(packet, server.COPP_Lazy_Packet)
        for msg in packet.get():
            if msg.typetags == ',ffff':
                set_rotation(get_bone_by_name(msg.subtarget), Quaternion(msg.payload) * diffs[msg.subtarget])
    bone.update()
    
    transmit_all(bones)
        

# notochord.end()
server.close_and_wait()