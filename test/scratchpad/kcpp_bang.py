from time import sleep
from tracemalloc import stop
import uuid
import notochord
import random

from notochord.config import Notochord_Config, Output_Sink
from notochord.timer import Node_Scheduler

notochord.init()

n = Notochord_Config()
n.comm.msg = Output_Sink.STDOUT
n.comm.port = 6565
n.comm.ip = "192.168.1.112"
n.comm.use_bundles = True
#n.comm.log_level = 1
#n.comm.adapter = "/dev/null"

n.raw = True
n.calib = False
n.use_armature = False

inp = input("Run calibration? (y/n)")

if inp == "y" or inp == "Y":
    n.calib = True



notochord.setup(n) 
notochord.communicator.info("Starting Notochord Scratch. IP {}".format(n.comm.ip))

s = Node_Scheduler()
s.scan()

inpt_str = ""

print(notochord.status())
notochord.start(s)
print("Write q to stop...")

while inpt_str != "q":
    inpt_str = input()


notochord.end()
