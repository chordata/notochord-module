from notochord.config import Notochord_Config, Kc_Values
from os import path
from lxml import etree
from notochord.error import Parse_Error

from notochord import parse
conf = Notochord_Config()
xml_path = path.realpath(path.join(path.dirname(__file__), "../data/test_Chordata.xml"))
with open(xml_path) as f:
	xml_string = f.read()
schema_path = path.realpath(path.join(path.dirname(__file__), "../data/test_Chordata.xsd"))
with open(schema_path) as f:
	schema_string = f.read()

xml = etree.fromstring(xml_string)
schema_xml = etree.fromstring(schema_string)
schema = etree.XMLSchema(schema_xml)

try:
	schema.assertValid(xml)
except etree.DocumentInvalid as e:
	raise Parse_Error(e) 
	
version = tuple(map(int, xml.attrib.get("version").split(".")))
if version < (1,0,0):
	raise Parse_Error("Version must be at least 1.0.0")	

config = xml.find("Configuration")
kc_revision = config.find(".//KC_revision") or config.find(".//kc_revision")
if kc_revision is not None:
	kc = kc_revision.text.strip()
	if kc == '2':
		conf.kc_revision == Kc_Values.Two
	elif kc == '++':
		conf.kc_revision == Kc_Values.PlusPlus
print(kc_revision.text)