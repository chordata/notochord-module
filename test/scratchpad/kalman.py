from notochord.fusion import Kalman

from os.path import join, realpath, dirname
import csv

K = Kalman()

print("Initial matrix/ offset")
print(K.m_offset)
print(K.m_matrix)


csv_file = join(dirname(realpath(__file__)),'../data/fake_spheroid_data.csv')

with open(csv_file, newline='') as csvfile:
	reader = csv.reader(csvfile)
	for row in reader:
		converted = tuple(int(num) for num in row)
		K.mag_calib(*converted)


print("After some calibration matrix/ offset")
print(K.m_offset)
print(K.m_matrix)

print(len(tuple(K.mag_buffer)))

print(list(K.mag_buffer))

