import notochord
from notochord.node import Mux, KCeptor, Mux_Channel
from notochord.config import Notochord_Config
from notochord.fusion import Kalman
from time import sleep

MUX_ADDR = 0x73
KC_ADDR = 0x1f
KC_WHO_REG = 0x0F
# KC_WHO_ANS = 0b01101000
# i2c = I2C()
# i2c.write_simple_byte(MUX_ADDR, 0x0f)
# assert i2c.read_simple_byte(MUX_ADDR) == 0x0f

# kc_who_resp = i2c.read_byte(KC_ADDR, KC_WHO_REG)
# assert kc_who_resp == KC_WHO_ANS
# print("KC WHOAMI RESPONSE: 0x{:0x}".format(kc_who_resp))

# #INIT ACEL:
# CTRL_REG5_XL = 0x1F
# CTRL_REG6_XL = 0x20
# OUT_X_L_XL = 0x28
# tempRegValue = 0;
# tempRegValue |= (1<<5);
# tempRegValue |= (1<<4);
# tempRegValue |= (1<<3);
# i2c.write_byte(KC_ADDR, CTRL_REG5_XL, tempRegValue);

# tempRegValue = 0;
# tempRegValue |= (2 & 0x07) << 5;
# tempRegValue |= (0x2 << 3);


# i2c.write_byte(KC_ADDR, CTRL_REG6_XL, tempRegValue);

# while 1:
#     res = i2c.read_bytes(KC_ADDR, OUT_X_L_XL, 6)
#     print(res)
#     sleep(0.2)
notochord.init()

conf = Notochord_Config()
# conf.comm.adapter = '/dev/null'
conf.comm.log_level = 1

notochord.setup(conf)
mux = Mux(None, MUX_ADDR, "test_mux")
branch_2 = mux.get_branch(Mux_Channel.CH_2)
mux.bang_branch(Mux_Channel.CH_2)

kc = KCeptor(branch_2, 0x0, "test_kc")

print(kc.m_matrix)


response = kc.setup()
print("KC setup", response)

slope = 1 

for i in range(10):
    sleep(0.05) 
    reading = kc.read_sensors(False)
    f = ["{:8d}".format(val) for val in reading]
    print(f)
    
while slope != 0:
    slope = kc.calibrate_ag()
    print("Calibrating, slope=", slope)
    sleep(0.05)

print("CALIB DONE")
print("g_calib =", kc.g_offset)
print("a_calib =", kc.a_offset)
print("==========")
input("press ENTER key to do mag calibration")


# while 1:
#     try:
#         reading = kc.read_sensors(False)
#         print(",".join((str(val) for val in reading)))
#         sleep(0.05)
#     except:
#         break


# exit()


# kraw = Kalman(50,1)
# input("KALMAN created")
# print("type-calib,gx,gy,gz,ax,ay,az,mx,my,mz,mag_calib_run,mag_buf_count,m_cov")

k = kc.kalman
mean_cov = 3000
try:
    while 1:
        # vals = kc.read_sensors(False)
        # kraw.set_gyro(vals[1],vals[0],vals[2])
        # kraw.set_accel(-vals[4],-vals[3],-vals[5])
        # res = kraw.mag_calib(-vals[7],vals[6],vals[8])

        # # # k.set_gyro(vals[0],vals[1],vals[2])
        # # # k.set_accel(vals[3],vals[4],vals[5])
        # # # res = k.mag_calib(vals[6],vals[7],vals[8])

        # cov = kraw.mag_covariance
        # if res:
        #     print("===== RAW kalman cov: {:.4f}".format(cov), kraw.mag_calib_buf_count)
        
        # if res:
        #     filt_run = ">>> RUN <<<<"
        #     print(filt_run)
        # else:
        #     filt_run = "NO"
        # # print(filt_run, "M covariance: {:.4f}".format(cov), k.mag_calib_buf_count)
        # vals += (int(res), int(k.mag_calib_buf_count), cov)
        # vals = [str(v) for v in vals]
        
        # print("mag-calib," + "\t".join(vals))
        cov = kc.calibrate_m()
        if cov >0:
            mean_cov = (cov + mean_cov) /2
            print("------ KC kalman cov: {:.4f}, mean: {:.4f}, buf count:".format(cov, mean_cov), k.mag_calib_buf_count)

        if mean_cov < 50:
            break

        sleep(1/conf.odr)
except Exception as e:
    print(e)
    
finally:
    # print("MAG OFFSET", k.m_offset)
    print("KC MAG OFFSET", kc.m_offset)
    
    
    # print("MAG MATRIX", k.m_matrix)
    print("KC MAG MATRIX", kc.m_matrix)

count = 0    
with open("/home/pi/scratch_dump.log", "w") as f:
    f.write("g_x,g_y,g_z,a_x,a_y,a_z,m_x,m_y,m_z\n")
    while 1:
        reading = kc.read_sensors(False)
        o = ",".join((str(val) for val in reading))
        print(count, o)
        f.write(o+"\n")
        sleep(0.05)
        count +=1
        if count > 500: break
    



