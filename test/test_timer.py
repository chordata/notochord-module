# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import pytest, uuid
from time import sleep
import random

@pytest.fixture
def Timekeeper():
    from notochord.timer import Timekeeper
    return Timekeeper

@pytest.mark.slow
def test_timekeeper(notochord_init, Timekeeper):
    tk = Timekeeper("test timekeeper")
    sleep(0.12)
    start = tk.millis
    #print(start)
    sleep(0.8)
    duration = tk.micros - start*1000
    assert duration == pytest.approx(800000, rel=0.1)
    duration = tk.millis - start
    assert duration == pytest.approx(800, rel=0.1)
    duration = tk.seconds - start/1000
    assert duration == pytest.approx(0.8, rel=0.1)

    tk.reset()
    assert tk.millis == pytest.approx(0) 

def test_global_timekeeper_no_noto_init(notochord_noninit, Timekeeper, error):   
    timer_mod = notochord_noninit.timer 
    assert timer_mod.global_timekeeper == None
    notochord_noninit.init()

    assert isinstance(timer_mod.global_timekeeper, timer_mod.Timekeeper)
    assert timer_mod.global_timekeeper.label == 'global'

    notochord_noninit.terminate()
    assert timer_mod.global_timekeeper == None

@pytest.mark.slow
def test_node_scheduler(Timer, Node_Scheduler, Mock_KCeptor, Mock_Mux, Mux_Channel, def_conf, notochord_init, comm):
    def_conf.comm.msg = comm.Output_Sink.STDOUT
    def_conf.comm.log_level = 2
    notochord_init.setup(def_conf)

    s = Node_Scheduler()
    mux = Mock_Mux(None, 0x73, "test_mux")
    t = Timer(s)
    sensors = []

    for i in range(6):
        s.add_node(mux.get_branch(Mux_Channel(i+1)))
        for i in range(3):
            sensors.append(Mock_KCeptor(None, 0x1b, ("test_m_kc_" + str(uuid.uuid1()))))
            s.add_node(sensors[-1])
            
    assert s.length() == 24
    assert len(sensors) == 18
    
    t.start()
    sleep(5)
    t.terminate()
    
    # We discard first buffer elements because the timer slowly corrects the frecuency
    for sensor in sensors:        
        for element in sensor.get_debug_buffer(True)[-16:]:
            assert element == pytest.approx(50, abs=4)
    
    # Ensure that buffer is clear for the next test
    assert len(sensors[0].get_debug_buffer()) == 0

    t.start()
    sleep(5)
    t.terminate()
    
    # We discard first buffer elements because the timer slowly corrects the frecuency
    for sensor in sensors:        
        for element in sensor.get_debug_buffer(True)[-16:]:
            assert element == pytest.approx(50, abs=4)

    # Start timer with an empty scheduler, it should return false
    s2 = Node_Scheduler()
    t2 = Timer(s2)

    assert t2.start() == False

@pytest.mark.slow
def test_core_start(Node_Scheduler, Mock_KCeptor, Mock_Mux, Mux_Channel, def_conf, notochord_init, comm):
    def_conf.comm.msg = comm.Output_Sink.STDOUT
    def_conf.comm.log_level = 2
    notochord_init.setup(def_conf)

    s = Node_Scheduler()
    mux = Mock_Mux(None, 0x73, "test_mux")
    sensors = []

    for i in range(6):
        s.add_node(mux.get_branch(Mux_Channel(i+1)))
        for i in range(3):
            sensors.append(Mock_KCeptor(None, 0x1b, ("test_m_kc_" + str(uuid.uuid1()))))
            s.add_node(sensors[-1])
            
    assert s.length() == 24
    assert len(sensors) == 18
    
    assert len(sensors[0].get_debug_buffer()) == 0
    
    notochord_init.start(s)
    sleep(5)
    notochord_init.end()

    assert len(sensors[0].get_debug_buffer()) > 0
    
    # We discard first buffer elements because the timer slowly corrects the frecuency
    for sensor in sensors:        
        for element in sensor.get_debug_buffer(True)[-16:]:
            assert element == pytest.approx(50, abs=4)



@pytest.mark.i2c
def test_node_scheduler_kc(Timer, Node_Scheduler, KCeptor, Mux, Mux_Channel, def_conf, notochord_init, comm):
    def_conf.comm.msg = comm.Output_Sink.STDOUT
    def_conf.comm.log_level = 2
    notochord_init.setup(def_conf)

    s = Node_Scheduler()
    mux = Mux(None, 0x73, "test_mux")
    t = Timer(s)
    sensors = []

    for i in range(6):
        s.add_node(mux.get_branch(Mux_Channel(i+1)))
        for i in range(3):
            sensors.append(KCeptor(None, 0x1b, ("test_m_kc_" + str(uuid.uuid1()))))
            s.add_node(sensors[-1])
            
    assert s.length() == 24
    assert len(sensors) == 18
    
    t.start()
    sleep(5)
    t.terminate()
    
    # We discard first buffer elements because the timer slowly corrects the frecuency
    # for sensor in sensors:        
    #     for element in sensor.get_debug_buffer(True)[-10:]:
    #         assert element == pytest.approx(50, abs=4)
    
    # Ensure that buffer is clear for the next test
    # assert len(sensors[0].get_debug_buffer()) == 0

    # t.start()
    # sleep(5)
    # t.terminate()
    
    # We discard first buffer elements because the timer slowly corrects the frecuency
    # for sensor in sensors:        
    #     for element in sensor.get_debug_buffer(True)[-10:]:
    #         assert element == pytest.approx(50, abs=4)

    # Start timer with an empty scheduler, it should return false
    s2 = Node_Scheduler()
    t2 = Timer(s2)

    assert t2.start() == False


