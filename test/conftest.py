# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import pytest
import random
import string
from lxml import etree
from sys import path
from os.path import join, realpath, dirname

path.append(join(dirname(realpath(__file__)), "../dist"))
path.append(join(dirname(realpath(__file__)), "modules"))

def pytest_addoption(parser):
	parser.addoption('--i2c', action='store_true', 
				 default=False, help="Enable i2c tests. It requires a Hub + KCeptor connected")

	parser.addoption('--slow', action='store_true', 
				 default=False, help="Enable slow tests")
		

def pytest_collection_modifyitems(config, items):
	if config.getoption("--i2c"):
		skip_non_i2c = pytest.mark.skip(reason="doesn't run with --i2c option ")
		for item in items:
			if "i2c" not in item.keywords:
				item.add_marker(skip_non_i2c)
		return

	skip_i2c = pytest.mark.skip(reason="need --i2c option to run")
	for item in items:
		if "i2c" in item.keywords:
			item.add_marker(skip_i2c)

	if config.getoption("--slow"):
		skip_non_slow = pytest.mark.skip(reason="doesn't run with --slow option ")
		for item in items:
			if "slow" not in item.keywords:
				item.add_marker(skip_non_slow)
		return

	skip_slow = pytest.mark.skip(reason="need --slow option to run")
	for item in items:
		if "slow" in item.keywords:
			item.add_marker(skip_slow)

@pytest.fixture
def notochord_noninit():
	import notochord
	# notochord_init.terminate() #clean eventually active notochord_init runtime
	return notochord

@pytest.fixture
def notochord_init():
	import notochord
	# notochord_init.terminate() #clean eventually active notochord_init runtime
	notochord.init()
	yield notochord
	notochord.terminate()

def _random_str(length):
	posible_chars = string.ascii_letters + string.digits
	return ''.join(random.SystemRandom().choice(posible_chars) for _ in range(length))

@pytest.fixture
def randstr():
	return _random_str(20)

@pytest.fixture
def comm(notochord_init):
	return notochord_init.communicator

@pytest.fixture
def config(notochord_noninit):
	return notochord_noninit.config

@pytest.fixture
def core(notochord_noninit):
	return notochord_noninit.core

@pytest.fixture
def utils(notochord_noninit):
	return notochord_noninit.utils

@pytest.fixture
def error(notochord_noninit):
	from notochord import error
	return error

@pytest.fixture
def Quaternion():
	from notochord.math import Quaternion
	return Quaternion

@pytest.fixture
def Matrix3():
	from notochord.math import Matrix3
	return Matrix3

@pytest.fixture
def Matrix4():
	from notochord.math import Matrix4
	return Matrix4

@pytest.fixture
def Vector():
	from notochord.math import Vector
	return Vector

@pytest.fixture
def def_conf(config):
	def_conf = config.Notochord_Config()
	def_conf.comm.adapter = '/dev/null'
	return def_conf 

@pytest.fixture
def transmit_payload(Quaternion):
	quats = {Quaternion(1,2,3,4), Quaternion(2,3,4,5)}
	addrs = {"/%/some_addr", "/%/other_addr"}
	return quats, addrs, [tuple(q) for q in quats]

@pytest.fixture
def transmit_payload_vector(Vector):
	vecs = {Vector(1,2,3), Vector(2,3,4)}
	addrs = {"/%/some_addr", "/%/other_addr"}
	return vecs, addrs, [tuple(v) for v in vecs]

@pytest.fixture
def copp_server():
	import copp_server
	port = 7683
	server = copp_server.COPP_Server( port = port )
	server.receive_timeout = 0.1 # this speeds up the teardown wait

	# add some COPP_Server types for convenience
	server.COPP_Lazy_Packet = copp_server.COPP_Lazy_Packet
	server.COPP_Common_packet = copp_server.COPP_Common_packet

	server.start()
	yield server
	server.close_and_wait()

@pytest.fixture
def basic_xml_file():
	return realpath(join(dirname(__file__), 'data/test_Chordata.xml'))

@pytest.fixture
def modified_xml_file():
	return realpath(join(dirname(__file__), 'data/test_Chordata_500.xml'))

@pytest.fixture
def basic_xsd_file():
	return realpath(join(dirname(__file__), 'data/test_Chordata.xsd'))

@pytest.fixture
def invalid_xml_file():
	return realpath(join(dirname(__file__), 'data/test_Chordata_not_valid.xml'))

@pytest.fixture
def default_biped_xml_file():
	return realpath(join(dirname(__file__), 'data/default_biped.xml'))

@pytest.fixture
def basic_xml(basic_xml_string):
	return etree.fromstring(basic_xml_string)

@pytest.fixture
def modified_xml(modified_xml_string):
	return etree.fromstring(modified_xml_string)

@pytest.fixture
def default_biped_xml(default_biped_xml_string):
	return etree.fromstring(default_biped_xml_string)

@pytest.fixture
def invalid_xml(invalid_xml_string):
	return etree.fromstring(invalid_xml_string)

@pytest.fixture
def basic_schema(basic_xsd_string):
	return etree.XMLSchema(etree.fromstring(basic_xsd_string))

@pytest.fixture
def basic_xml_string(basic_xml_file):
	with open(basic_xml_file, 'r') as xml:
		return xml.read()

@pytest.fixture
def modified_xml_string(modified_xml_file):
	with open(modified_xml_file, 'r') as xml:
		return xml.read()

@pytest.fixture
def invalid_xml_string(invalid_xml_file):
	the_file = invalid_xml_file
	with open(the_file, 'r') as xml:
		return xml.read()

@pytest.fixture
def default_biped_xml_string(default_biped_xml_file):
	the_file = default_biped_xml_file
	with open(the_file, 'r') as xml:
		return xml.read()

@pytest.fixture
def basic_xsd_string(basic_xsd_file):
	with open(basic_xsd_file, 'r') as xml:
		return xml.read()

@pytest.fixture
def I2C(notochord_init):
	from notochord.io import I2C
	return I2C

@pytest.fixture
def i2c_stub(notochord_init, def_conf):
	from notochord.io import I2C
	assert notochord_init.io.i2c is None
	notochord_init.setup(def_conf)
	assert isinstance(notochord_init.io.i2c, I2C)
	return notochord_init.io.i2c

@pytest.fixture
def i2c(notochord_init, config):
	from notochord.io import I2C
	assert notochord_init.io.i2c is None
	notochord_init.setup(config.Notochord_Config())
	assert isinstance(notochord_init.io.i2c, I2C)
	return notochord_init.io.i2c

@pytest.fixture
def Mux(notochord_init):
    from notochord.node import Mux
    return Mux

@pytest.fixture
def KCeptor(notochord_init):
    from notochord.node import KCeptor
    return KCeptor

@pytest.fixture
def Hierarchy(notochord_init):
    from notochord.node import Hierarchy
    return Hierarchy

@pytest.fixture 
def Node_Scheduler():
	from notochord.timer import Node_Scheduler
	return Node_Scheduler

@pytest.fixture
def Mock_Mux():
    from notochord.mock_node import Mock_Mux
    return Mock_Mux

@pytest.fixture
def Mock_KCeptor():
    from notochord.mock_node import Mock_KCeptor
    return Mock_KCeptor

@pytest.fixture
def Mux_Channel():
    from notochord.node import Mux_Channel
    return Mux_Channel

@pytest.fixture
def Timer():
    from notochord.timer import Timer
    return Timer

@pytest.fixture
def parse():
	import notochord.parse
	return notochord.parse

@pytest.fixture
def types():
	import notochord.types
	return notochord.types

@pytest.fixture
def Accel_Scale_Options():
    from notochord.types import Accel_Scale_Options
    return Accel_Scale_Options

@pytest.fixture
def Gyro_Scale_Options():
    from notochord.types import Gyro_Scale_Options
    return Gyro_Scale_Options

@pytest.fixture
def Mag_Scale_Options():
    from notochord.types import Mag_Scale_Options
    return Mag_Scale_Options

@pytest.fixture
def Output_Data_Rate():
    from notochord.types import Output_Data_Rate
    return Output_Data_Rate

@pytest.fixture
def Bone():
	from notochord.armature import Bone
	return Bone

@pytest.fixture
def Calibrator():
	import notochord.calibration as Calibrator
	return Calibrator

