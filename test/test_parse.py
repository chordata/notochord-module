# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import pytest
from lxml import etree
from os import path
import sys

def test_validate_xml(basic_xml, basic_schema):
	try:
		basic_schema.assertValid(basic_xml)
	except Exception as ex:
		assert False, f"schema.assertValid raised exception {ex}"

def test_invalid_xml_validation(invalid_xml, basic_schema):
	with(pytest.raises(etree.DocumentInvalid)):
		basic_schema.assertValid(invalid_xml)

def test_xml_not_valid_parsing(invalid_xml_file, basic_xsd_file, error, parse):
	xml_path = invalid_xml_file
	schema_path = basic_xsd_file
	args = ["--ip", "1.2.3.4", "--port", "1234","-c",xml_path,"--schema",schema_path, "--send_rate", "45.5"]
	with pytest.raises(error.Parse_Error):
		parse.parse_config(args)

def test_read_xml(basic_xml_string, parse):
	xml_string = parse.read_xml(path.join(path.dirname(path.realpath(__file__)), 'data/test_Chordata.xml'))
	assert len(xml_string) == len(basic_xml_string)
	assert xml_string == basic_xml_string

def test_xml_parsing(def_conf, modified_xml, comm, config, parse):
	c = def_conf
	parse.parse(modified_xml, c)
	assert c.comm.ip == "127.0.0.1"
	assert c.comm.port == 6565
	assert c.comm.send_rate == 50
	assert c.comm.adapter == "/dev/i2c-1"
	assert c.comm.msg == comm.Output_Sink.FILE | comm.Output_Sink.STDOUT | comm.Output_Sink.PYTHON
	assert c.comm.transmit == comm.Output_Sink.OSC | comm.Output_Sink.PYTHON
	assert c.comm.send_rate == 50
	assert c.comm.log_level == 0
	assert c.osc.base_address == "/Chordata"
	assert c.sensor.gyro_scale == config.Gyro_Scale_Options.gyro_500
	assert c.sensor.accel_scale == config.Accel_Scale_Options.accel_8
	assert c.sensor.mag_scale == config.Mag_Scale_Options.mag_12
	assert c.sensor.madgwick == True
	assert c.sensor.gyro_scale == True
	assert c.kc_revision == config.Kc_Values.Two

def test_cmd_parsing(comm, config, parse, modified_xml_file, basic_xsd_file):
	xml_path = modified_xml_file
	schema_path = basic_xsd_file

	args = ["-c",xml_path,"--schema",schema_path, "--send_rate", "45.5", "1.2.3.4", "1234"]
	c = parse.parse_config(args)
	assert c.comm.ip == "1.2.3.4"
	assert c.comm.port == 1234
	assert c.comm.send_rate == 45.5
	assert c.comm.adapter == "/dev/i2c-1"
	assert c.comm.msg == comm.Output_Sink.FILE | comm.Output_Sink.STDOUT | comm.Output_Sink.PYTHON
	assert c.comm.transmit == comm.Output_Sink.OSC | comm.Output_Sink.PYTHON
	assert c.comm.log_level == 0
	assert c.comm.websocket_port == 7681
	assert c.osc.base_address == "/Chordata"
	assert c.sensor.gyro_scale == config.Gyro_Scale_Options.gyro_500
	assert c.sensor.accel_scale == config.Accel_Scale_Options.accel_8
	assert c.sensor.mag_scale == config.Mag_Scale_Options.mag_12
	assert c.sensor.madgwick == True
	assert c.sensor.gyro_scale == True
	assert c.kc_revision == config.Kc_Values.Two


def test_cmd_parsing_empty_args(comm, config, parse, modified_xml_file, basic_xsd_file):
	xml_path = modified_xml_file
	schema_path = basic_xsd_file
	old_argv = sys.argv
	sys.argv = ['test_parse.py', "-c",xml_path,"--schema",schema_path]
	try:
		c = parse.parse_config()
	finally:
		sys.argv = old_argv
	assert c.comm.ip == "127.0.0.1"
	assert c.comm.port == 6565
	assert c.comm.send_rate == 50
	assert c.comm.adapter == "/dev/i2c-1"
	assert c.comm.msg == comm.Output_Sink.FILE | comm.Output_Sink.STDOUT | comm.Output_Sink.PYTHON
	assert c.comm.transmit == comm.Output_Sink.OSC | comm.Output_Sink.PYTHON
	assert c.comm.send_rate == 50
	assert c.comm.log_level == 0
	assert c.comm.websocket_port == 7681
	assert c.osc.base_address == "/Chordata"
	assert c.sensor.gyro_scale == config.Gyro_Scale_Options.gyro_500
	assert c.sensor.accel_scale == config.Accel_Scale_Options.accel_8
	assert c.sensor.mag_scale == config.Mag_Scale_Options.mag_12
	assert c.sensor.madgwick == True
	assert c.sensor.gyro_scale == True
	assert c.kc_revision == config.Kc_Values.Two


def test_parse_hierarchy_xml_default_biped(notochord_init, default_biped_xml, Node_Scheduler, parse):
	s = Node_Scheduler()
	h = parse.parse_hierarchy_xml(default_biped_xml, s)
	kc_info = h.get_node_info("r-arm")

	assert kc_info["parent_label"] == 'r-forarm'
	assert 'r-hand' in kc_info["children_label"]

def test_parse_hierarchy_xml_test_chordata(notochord_init, basic_xml, Node_Scheduler, parse):
	s = Node_Scheduler()
	h = parse.parse_hierarchy_xml(basic_xml, s)
	kc_info = h.get_node_info("Unico")

	assert kc_info["parent_label"] == ''
	assert len(kc_info["children_label"]) == 0
