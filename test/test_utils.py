# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import pytest
from math import pi
from os import path

def test_conversions(notochord_init):
	original = (90, 180, 0, 1, 2, 3, 4, 5, 6)
	expected_degs = (180, 90, 0, 2, 1, 3, 5, -4, 6)
	expected_rads = (pi, pi/2, 0, 2, 1, 3, 5, -4, 6)

	converted = notochord_init.utils.convert_LSM9DS1_ENU_rads(*original)
	assert converted == pytest.approx(expected_rads)

	converted = notochord_init.utils.convert_LSM9DS1_ENU_degs(*original)
	assert converted == pytest.approx(expected_degs)


def test_offset_calibrator_constant(notochord_init):
	from random import randint
	C = notochord_init.utils.Offset_Calibrator()
	assert C.result == (0,0,0)

	expected_offset = 345

	for i in range(100):
		r = tuple(randint(-6, 6) + expected_offset for _ in range(3))
		C.add_reading(*r)

	assert C.result == pytest.approx((expected_offset,)*3, abs=2)


def test_offset_calibrator_slope(notochord_init):
	from random import randint
	C = notochord_init.utils.Offset_Calibrator()

	sum_slope = 0
	for i in range(100):
		r = tuple(randint(-3, 3) - i + 100 for _ in range(3))	
		slope = C.add_reading(*r)
		if i > 50: # the first readings are discarded
			#print(slope)
			sum_slope += slope
			assert slope == pytest.approx(-1, abs=0.3)

	assert sum_slope/50 == pytest.approx(-1, abs=0.05)

	sum_slope = 0
	for i in range(100):
		r = tuple(randint(-3, 3) + i/2 + 100 for _ in range(3))	
		slope = C.add_reading(*r)
		if i > 50: # the first readings are discarded
			sum_slope += slope
			assert slope == pytest.approx(0.5, abs=0.25)

	assert sum_slope/50 == pytest.approx(0.5, abs=0.05)
