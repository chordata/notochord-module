// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

// #include "hierarchy.h"
#include "phy_nodes.h"
#include <iostream>

int main(int argc, char const *argv[])
{
    Chordata::Node n(21, "lala");

    //Commented out these lines because the constructors became private,
    //need to refactor test if used
    // Chordata::Armature a(nullptr); 
    // Chordata::_Mux m(nullptr, 0x70, "pippo", nullptr);

    // Chordata::Link *lm = static_cast<Chordata::Link*>(&m);
    // Chordata::Multiplexer *mm = dynamic_cast<Chordata::_Mux*>(lm);
    
    // Chordata::Node *mn= dynamic_cast<Chordata::Node*>(lm);



    // std::cout << lm <<std::endl;
    // std::cout << mm <<std::endl;
    // std::cout << mm->get_label() <<std::endl;
    // std::cout << mn <<std::endl;



    return 0;
}
