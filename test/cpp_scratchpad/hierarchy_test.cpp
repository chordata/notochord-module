// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#include "hierarchy.h"
#include "phy_nodes.h"
#include <memory>
#include <vector>
#include <iostream>
#include <functional>
#include <map>

using namespace Chordata;

using node_ptr = std::shared_ptr<_Node>;
using link_wptr = std::weak_ptr<Link>;


// struct Link_Info{
//     std::string label;
//     std::string parent_label;
//     link_wptr parent; 
//     std::vector<link_wptr>children; 
//     std::vector<std::string>children_label;
//     long shared_ptr_count;
// };

link_index info_list;

int counter = 0;

void print_label(link_wptr this_wlink, int*& _counter){
    // node_ptr this_node = std::dynamic_pointer_cast<_Node>(this_link);

    *(_counter) = (*(_counter)) + 1;
    long shared_count = this_wlink.use_count();
    
    if (link_ptr this_link = this_wlink.lock()){
        std::cout << "+++ WALK NODE: " << this_link->get_label() <<std::endl;

        std::vector<std::string> children_labels;
        std::vector<link_wptr> children;
        
        for (size_t i = 0; i < this_link->get_children_n(); i++)
            {
                children_labels.push_back(this_link->get_child(i)->get_label());
                children.push_back(std::move(this_link->get_weak_child(i)));
            }
        
        std::string parent_label("");
        link_wptr wparent = this_link->getParent();
        if (link_ptr parent = wparent.lock()){
            if (parent){
                parent_label = parent->get_label();
            } 
        } else {
            std::cout << "PARENT SHARED PTR HAS EXPIRED" << std::endl;
        }
    

        info_list[this_link->get_label()] = {
            this_link->get_label(),
            parent_label,
            std::move(this_link->getParent()),
            std::move(children), 
            std::move(children_labels),
            shared_count
        };

    } else {
        std::cout << "SHARED PTR HAS EXPIRED" << std::endl;
    }


}

node_ptr root;

void create_hierarchy(node_ptr root){
    node_ptr child_1 = std::make_shared<_Node>(root, 0x02, "child_1");
    std::cout << "## inmediate CH_1 shrd_ptr_count (build):" << child_1.use_count() <<std::endl;
    root->add_child(child_1);
    std::cout << "## inmediate CH_1 shrd_ptr_count (after add_child1):" << root->get_child(0).use_count() <<std::endl;

    node_ptr child_2 = std::make_shared<_Node>(child_1, 0x03, "child_2");
    root->get_child(0)->add_child(child_2);

    std::cout << "## inmediate CH_1 shrd_ptr_count (after add_child2):" << root->get_child(0).use_count() <<std::endl;

}


int main(int argc, char const *argv[])
{
    std::cout << "========= Start test =========\n" <<std::endl;
    
    root = std::make_shared<_Node>(nullptr, 0x01, "root");

    
    create_hierarchy(root);

    link_wptr child_1 = root->get_child(0);

    std::cout << "## inmediate CH_1 shrd_ptr_count (before walk):" << child_1.use_count() <<std::endl;

    Hierarchy h(std::move(root));
    h.walk(print_label, &counter);

    std::cout << "## inmediate CH_1 shrd_ptr_count (after walk):" <<  child_1.use_count() <<std::endl;

    std::cout << "\n========= READ info struct ========= \n" <<std::endl;


    std::cout << "ROOT shrd_ptr_count:" << info_list["root"].shared_ptr_count <<std::endl;
    std::cout << "CH_1 shrd_ptr_count:" << info_list["child_1"].shared_ptr_count <<std::endl;
    std::cout << "CH_2 shrd_ptr_count:" << info_list["child_2"].shared_ptr_count <<std::endl;
    std::cout << "Counter:" << counter << std::endl;

    std::cout << "\n========= END test ========= \n" <<std::endl;

    Link_Info& root_inf = info_list.at("root");
    Link_Info& chi1_inf = info_list.at("child_1");
    Link_Info& chi2_inf = info_list.at("child_2");

    h.build_index();

    const Link_Info& root_H_inf = h.get_index().at("root");
    const Link_Info& chi1_H_inf = h.get_index().at("child_1");
    const Link_Info& chi2_H_inf = h.get_index().at("child_2");



    //Commented out these lines because the constructors became private,
    //need to refactor test if used
    // Chordata::Armature a(nullptr); 
    // Chordata::_Mux m(nullptr, 0x70, "pippo", nullptr);

    // Chordata::Link *lm = static_cast<Chordata::Link*>(&m);
    // Chordata::Multiplexer *mm = dynamic_cast<Chordata::_Mux*>(lm);
    
    // Chordata::Node *mn= dynamic_cast<Chordata::Node*>(lm);



    // std::cout << lm <<std::endl;
    // std::cout << mm <<std::endl;
    // std::cout << mm->get_label() <<std::endl;
    // std::cout << mn <<std::endl;



    return 0;
}
