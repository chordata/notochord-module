// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#include <Eigen/Dense>
#include <Eigen/Geometry> 
#include <iostream>

// from Quaternion.h:372
// Coefficients is a 4x1 Matrix in the order X Y Z W

// namespace internal {
//   template<typename _Scalar, int _Options>
//   struct traits<Map<Quaternion<_Scalar>, _Options> > : traits<Quaternion<_Scalar, (int(_Options)&Aligned)==Aligned ? AutoAlign : DontAlign> >
//   {
//     typedef Map<Matrix<_Scalar,4,1>, _Options> Coefficients;
//   };
// }

int main(int argc, char const *argv[]){
    auto q = Eigen::Quaternionf(1,0,0,0);

    Eigen::Quaternionf::Coefficients& vec_repr = q.coeffs();

    vec_repr(3,0) = 14.2;
    vec_repr(0,0) = 24.2;
    vec_repr(1,0) = 34.2;
    vec_repr(2,0) = 44.2;

    std::cout   << q.w() << " "
                << q.x() << " "
                << q.y() << " " 
                << q.z() << std::endl;

    //prints 14.2 24.2 34.2 44.2

    auto v = Eigen::Vector3f(1,2,3);
    v *= 2;

    std::cout   << v.x() << " "
                << v.y() << " " 
                << v.z() << std::endl;

    //prints 2 4 6

    auto m = Eigen::Matrix3f{{1, 1, 1},
                             {2, 2, 2},
                             {3, 3, 3}};

    m *= 2;

    std::cout << m(0,0) << " "
              << m(1,0) << " "
              << m(2,0) << std::endl;

    //prints 2 4 6

    m *= m;

    std::cout << m(0,0) << " "
              << m(1,0) << " "
              << m(2,0) << std::endl;

    //prints 24 48 72

    auto vec = Eigen::Vector3f(0,0,0);
    float x = 5;
    int16_t y = x;


	enum sensor_axis {
		X_AXIS = 0,
		Y_AXIS = 1,
		Z_AXIS = 2
	};

    vec[Y_AXIS] = y;
   
    std::cout << vec[Y_AXIS] << std::endl;

}