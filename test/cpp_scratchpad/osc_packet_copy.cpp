#include "SwapOutboundPacketStream.h"
#include <iostream>


template< int capacity, int padding = 0 >
struct OSC_Packet {
    OSC_Packet():
    buffer(),
    p(buffer+padding, capacity)
    {}

    char buffer[capacity+padding];
    osc::SwapOutboundPacketStream p;

    const char* data() const {
        return p.Data()+padding;
    }

    size_t size() const {
        return p.Size()-padding;
    }

};
int main (int argc, char const *argv[])
{
    // Create a packet
    OSC_Packet<100> packet, packet2;

    // Write some data to it
    std::cout << "Write some data to the packet 1" << std::endl;
    packet.p << osc::BeginBundleImmediate
             << osc::BeginMessage("/test")
             << "first write" << osc::EndMessage
             << osc::EndBundle;
    
    // Print the data
    for (int i = 0; i < packet.size(); i++) {
        std::cout << packet.data()[i] << " ";
    }
    std::cout << std::endl;

    // Copy the packet
    std::cout << "Copying the packet..." << std::endl;
    // OSC_Packet<100> packet2;


    std::cout << "Packet Data before copy" << std::endl;
    std::cout << "Packet 1: " << std::hex << (void*)packet.p.Data() << std::endl;
    std::cout << "Packet 2: " << std::hex << (void*)packet2.p.Data() << std::endl;
    packet2.p.Swap(packet.p);
    std::cout << "Packet Data after copy" << std::hex << std::endl;
    std::cout << "Packet 1: " << std::hex << (void*)packet.p.Data() << std::endl;
    std::cout << "Packet 2: " << std::hex << (void*)packet2.p.Data() << std::endl;


    // Print the data
    std::cout << "Printing the data of both packets..." << std::endl;
    std::cout << "Packet 1" << std::endl;
    for (int i = 0; i < packet.size(); i++) {
        std::cout << packet.data()[i] << " ";
    }
    std::cout << std::endl;

    std::cout << "Packet 2" << std::endl;
    for (int i = 0; i < packet2.size(); i++) {
        std::cout << packet2.data()[i] << " ";
    }
    std::cout << std::endl;
    packet.p.Clear();
    // Write some more data to the packet 1
    std::cout << "Write some data to the packet 1" << std::endl;
    packet.p << osc::BeginBundleImmediate
             << osc::BeginMessage("/test")
             << "second write" << osc::EndMessage
             << osc::EndBundle;

    //packet2.p = packet.p;
    
    // Print the data
    std::cout << "Printing the data of both packets..." << std::endl;
    std::cout << "Packet 1 size: " << packet.size() << std::endl;
    for (int i = 0; i < packet.size(); i++) {
        std::cout << packet.data()[i] << " ";
    }
    std::cout << std::endl;

    std::cout << "Packet 2 size: " << packet2.size() << std::endl;
    for (int i = 0; i < packet2.size(); i++) {
        std::cout << packet2.data()[i] << " ";
    }
    std::cout << std::endl;
    

    return 0;
}