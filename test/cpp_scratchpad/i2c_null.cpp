// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#include "phy_nodes.h"
#include "core.h"
#include "io.h"
#include <memory>
#include <vector>
#include <iostream>
#include <functional>
#include <map>

using namespace Chordata;



int main(int argc, char const *argv[])
{
    std::cout << "========= Start test =========\n" <<std::endl;
  
    Notochord_Runtime *n = get_runtime();
    Configuration_Data conf;
    conf.comm.adapter = "/dev/null";
    n->configure_i2c(&conf);
    io_ptr i2c = n->get_i2c();

    i2c->read_byte(0x6b);

    terminate_runtime();
    return 0;
}
