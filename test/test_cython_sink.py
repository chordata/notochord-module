
import pytest
import re, uuid
from time import sleep

ASYNC_SLEEP = 0.05
from notochord.types import Log_Levels

def test_comm_cython_sink(notochord_init, comm, config, capfd, def_conf, randstr):
	def_conf.comm.msg = comm.Output_Sink.PYTHON
	to_cython = "cython random string " + randstr
	notochord_init.setup(def_conf)
	comm.flush()
	captured = comm.get_registries(True)
	assert comm.Output_Sink.PYTHON in def_conf.comm.msg
	assert len(captured) == 6

def test_comm_cython_sink_trace(notochord_init, comm, config, capfd, def_conf, randstr):
	def_conf.comm.msg = comm.Output_Sink.PYTHON
	def_conf.comm.log_level = 2
	to_cython = "cython random string " + randstr
	notochord_init.setup(def_conf)
	comm.flush()
	captured = comm.get_registries(True)
	assert comm.Output_Sink.PYTHON in def_conf.comm.msg

	comm.trace(to_cython)
	comm.flush()
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = comm.get_registry(True, Log_Levels.Trace)
	assert len(captured) == 1
	assert to_cython in captured[0]

	assert len(comm.get_registry(False, Log_Levels.Trace)) == 0

def test_comm_cython_sink_debug(notochord_init, comm, config, capfd, def_conf, randstr):
	def_conf.comm.msg = comm.Output_Sink.PYTHON
	def_conf.comm.log_level = 1
	to_cython = "cython random string " + randstr
	notochord_init.setup(def_conf)
	comm.flush()
	captured = comm.get_registries(True)
	assert comm.Output_Sink.PYTHON in def_conf.comm.msg

	comm.debug(to_cython)
	comm.flush()
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = comm.get_registry(True, Log_Levels.Debug)
	assert len(captured) == 1
	assert to_cython in captured[0]

	assert len(comm.get_registry(False, Log_Levels.Debug)) == 0

def test_comm_cython_sink_info(notochord_init, comm, config, capfd, def_conf, randstr):
	def_conf.comm.msg = comm.Output_Sink.PYTHON
	to_cython = "cython random string " + randstr
	notochord_init.setup(def_conf)
	comm.flush()
	captured = comm.get_registries(True)
	assert comm.Output_Sink.PYTHON in def_conf.comm.msg

	comm.info(to_cython)
	comm.flush()
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = comm.get_registry(True)
	assert len(captured) == 1
	assert to_cython in captured[0]

	assert len(comm.get_registry(False, Log_Levels.Info)) == 0

def test_comm_cython_sink_warn(notochord_init, comm, config, capfd, def_conf, randstr):
	def_conf.comm.msg = comm.Output_Sink.PYTHON
	to_cython = "cython random string " + randstr
	notochord_init.setup(def_conf)
	comm.flush()
	captured = comm.get_registries(True)
	assert comm.Output_Sink.PYTHON in def_conf.comm.msg

	comm.warn(to_cython)
	comm.flush()
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = comm.get_registry(True, Log_Levels.Warn)
	assert len(captured) == 1
	assert to_cython in captured[0]

	assert len(comm.get_registry(False, Log_Levels.Warn)) == 0

def test_comm_cython_sink_error(notochord_init, comm, config, capfd, def_conf, randstr):
	def_conf.comm.msg = comm.Output_Sink.PYTHON
	def_conf.comm.error = comm.Output_Sink.PYTHON
	to_cython = "cython random string " + randstr
	notochord_init.setup(def_conf)
	comm.flush()
	captured = comm.get_registries(True)
	assert comm.Output_Sink.PYTHON in def_conf.comm.error

	comm.error(to_cython)
	comm.flush()
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = comm.get_registry(True, Log_Levels.Error)
	assert len(captured) == 1
	assert to_cython in captured[0]

	assert len(comm.get_registry(False, Log_Levels.Error)) == 0

def test_comm_cython_sink_critical(notochord_init, comm, config, capfd, def_conf, randstr):
	def_conf.comm.msg = comm.Output_Sink.PYTHON
	to_cython = "cython random string " + randstr
	notochord_init.setup(def_conf)
	comm.flush()
	captured = comm.get_registries(True)
	assert comm.Output_Sink.PYTHON in def_conf.comm.msg

	comm.critical(to_cython)
	comm.flush()
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = comm.get_registry(True, Log_Levels.Critical)
	assert len(captured) == 1
	assert to_cython in captured[0]

	assert len(comm.get_registry(False, Log_Levels.Critical)) == 0


def test_comm_cython_sink_trace_no_clear(notochord_init, comm, config, capfd, def_conf, randstr):
	def_conf.comm.msg = comm.Output_Sink.PYTHON
	def_conf.comm.log_level = 2
	to_cython = "cython random string " + randstr
	notochord_init.setup(def_conf)
	comm.flush()
	captured = comm.get_registries(True)
	assert comm.Output_Sink.PYTHON in def_conf.comm.msg

	comm.trace(to_cython)
	comm.flush()
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = comm.get_registry(False, Log_Levels.Trace)
	assert len(captured) == 1
	assert to_cython in captured[0]

	assert len(comm.get_registry(False, Log_Levels.Trace)) == 1

def test_comm_cython_sink_debug_no_clear(notochord_init, comm, config, capfd, def_conf, randstr):
	def_conf.comm.msg = comm.Output_Sink.PYTHON
	def_conf.comm.log_level = 1
	to_cython = "cython random string " + randstr
	notochord_init.setup(def_conf)
	comm.flush()
	captured = comm.get_registries(True)
	assert comm.Output_Sink.PYTHON in def_conf.comm.msg

	comm.debug(to_cython)
	comm.flush()
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = comm.get_registry(False, Log_Levels.Debug)
	assert len(captured) == 1
	assert to_cython in captured[0]

	assert len(comm.get_registry(False, Log_Levels.Debug)) == 1

def test_comm_cython_sink_info_no_clear(notochord_init, comm, config, capfd, def_conf, randstr):
	def_conf.comm.msg = comm.Output_Sink.PYTHON
	to_cython = "cython random string " + randstr
	notochord_init.setup(def_conf)
	comm.flush()
	captured = comm.get_registries(True)
	assert comm.Output_Sink.PYTHON in def_conf.comm.msg

	comm.info(to_cython)
	comm.flush()
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = comm.get_registry(False)
	assert len(captured) == 1
	assert to_cython in captured[0]

	assert len(comm.get_registry(False, Log_Levels.Info)) == 1

def test_comm_cython_sink_warn_no_clear(notochord_init, comm, config, capfd, def_conf, randstr):
	def_conf.comm.msg = comm.Output_Sink.PYTHON
	to_cython = "cython random string " + randstr
	notochord_init.setup(def_conf)
	comm.flush()
	captured = comm.get_registries(True)
	assert comm.Output_Sink.PYTHON in def_conf.comm.msg

	comm.warn(to_cython)
	comm.flush()
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = comm.get_registry(False, Log_Levels.Warn)
	assert len(captured) == 1
	assert to_cython in captured[0]

	assert len(comm.get_registry(False, Log_Levels.Warn)) == 1

def test_comm_cython_sink_error_no_clear(notochord_init, comm, config, capfd, def_conf, randstr):
	def_conf.comm.msg = comm.Output_Sink.PYTHON
	def_conf.comm.error = comm.Output_Sink.PYTHON
	to_cython = "cython random string " + randstr
	notochord_init.setup(def_conf)
	comm.flush()
	captured = comm.get_registries(True)
	assert comm.Output_Sink.PYTHON in def_conf.comm.error

	comm.error(to_cython)
	comm.flush()
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = comm.get_registry(False, Log_Levels.Error)
	assert len(captured) == 1
	assert to_cython in captured[0]

	assert len(comm.get_registry(False, Log_Levels.Error)) == 1

def test_comm_cython_sink_critical_no_clear(notochord_init, comm, config, capfd, def_conf, randstr):
	def_conf.comm.msg = comm.Output_Sink.PYTHON
	to_cython = "cython random string " + randstr
	notochord_init.setup(def_conf)
	comm.flush()
	captured = comm.get_registries(True)
	assert comm.Output_Sink.PYTHON in def_conf.comm.msg

	comm.critical(to_cython)
	comm.flush()
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = comm.get_registry(False, Log_Levels.Critical)
	assert len(captured) == 1
	assert to_cython in captured[0]

	assert len(comm.get_registry(False, Log_Levels.Critical)) == 1