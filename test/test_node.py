# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import pytest
from time import sleep
from random import random, randint

def test_mux_props(Mux):
    mux = Mux(None, 0x73, "test_mux")
    assert mux.label == "test_mux"
    assert mux.address == 0x73
    assert mux.parent == None

def test_mux_bang(notochord_init, Mux):
    mux = Mux(None, 0x73, "test_mux")
    assert mux.state == notochord_init.node.Mux_Channel.OFF
    mux.bang_branch(notochord_init.node.Mux_Channel.CH_1)
    assert mux.state == notochord_init.node.Mux_Channel.CH_1

@pytest.mark.i2c
def test_phy_branch_bang(notochord_init, i2c, Mux):
    mux = Mux(None, 0x73, "test_mux", i2c)
    mux.bang_branch(notochord_init.node.Mux_Channel.CH_1)
    mux_i2c_read = i2c.read_simple_byte(0x73)
    assert mux_i2c_read == notochord_init.node.Mux_Channel.CH_1.value

    mux.bang_branch(notochord_init.node.Mux_Channel.CH_2)
    mux_i2c_read = i2c.read_simple_byte(0x73)
    assert mux_i2c_read == notochord_init.node.Mux_Channel.CH_2.value

    mux.bang_branch(notochord_init.node.Mux_Channel.OFF)
    mux_i2c_read = i2c.read_simple_byte(0x73)
    assert mux_i2c_read == notochord_init.node.Mux_Channel.OFF.value


def test_KC_props(notochord_init, KCeptor, def_conf, Matrix3, Output_Data_Rate):
    kc_noconf = KCeptor(None, 0x1b, "test_kc_noconf")
    assert kc_noconf.i2c != notochord_init.io.i2c
    
    def_conf.odr = Output_Data_Rate.OUTPUT_RATE_119
    notochord_init.setup(def_conf)
    kc = KCeptor(None, 0x1b, "test_kc")
    assert kc.i2c == notochord_init.io.i2c
    #kc.odr returns the actual int value, not the enum option
    assert kc.odr == 119 
    
    assert kc.g_offset == (0,0,0)
    assert kc.m_offset == (0,0,0)
    assert kc.a_offset == (0,0,0)

    kc.g_offset = (2,2,2)

    assert kc.g_offset[0] == 2
    
    vals = tuple(randint(-1000, 1000) for _ in range(3))
    kc.g_offset = (vals)
    assert kc.g_offset == vals
    
    vals = tuple(randint(-1000, 1000) for _ in range(3))
    kc.a_offset = vals
    assert kc.a_offset == vals

    vals = tuple(randint(-1000, 1000) for _ in range(3))
    kc.m_offset = vals
    assert kc.m_offset == vals

    vals = tuple(random() for _ in range(9))
    m = Matrix3(*vals)
    kc.m_matrix = m
    assert kc.m_matrix == m
    
        
def test_KC_mock_io(notochord_init, KCeptor, def_conf):
    kc = KCeptor(None, 0x1b, "test_kc")
    assert len(kc.i2c._mock_reads) == 0
    kc.setup()
    assert len(kc.i2c._mock_reads) == 2
    

def test_KC_imu_settings(KCeptor, notochord_init, comm, def_conf, config):
    assert comm.Output_Sink.STDOUT in def_conf.comm.msg
    def_conf.comm.log_level = 2

    def_conf.sensor.accel_scale = config.Accel_Scale_Options.accel_16
    def_conf.sensor.gyro_scale = config.Gyro_Scale_Options.gyro_245
    def_conf.sensor.mag_scale = config.Mag_Scale_Options.mag_16

    notochord_init.setup(def_conf)
    
    kc = KCeptor(None, 0x1b, "test_kc")
    
    assert kc.accel == config.Accel_Scale_Options.accel_16
    assert kc.gyro == config.Gyro_Scale_Options.gyro_245
    assert kc.mag == config.Mag_Scale_Options.mag_16

    # Rewrite this test, since we cannot longer set random numbers

    # def_conf.sensor.accel_scale = 0
    # def_conf.sensor.gyro_scale = 0
    # def_conf.sensor.mag_scale = 0
    # notochord_init.setup(def_conf)

    # kc = KCeptor(None, 0x1b, "test_kc2")
    
    # # Should be default values, since we introduced wrong inputs
    # assert kc.accel ==  config.Accel_Scale_Options.accel_8
    # assert kc.gyro ==  config.Gyro_Scale_Options.gyro_2000
    # assert kc.mag == config.Mag_Scale_Options.mag_4

def test_KC_kalman_settings(KCeptor, notochord_init, comm, def_conf, types):
    assert comm.Output_Sink.STDOUT in def_conf.comm.msg
    def_conf.comm.log_level = 2

    def_conf.sensor.accel_scale = types.Accel_Scale_Options.accel_16
    def_conf.sensor.gyro_scale = types.Gyro_Scale_Options.gyro_245
    def_conf.sensor.mag_scale = types.Mag_Scale_Options.mag_16

    notochord_init.setup(def_conf)
    
    kc = KCeptor(None, 0x1b, "test_kc")
    
    assert kc.kalman.accel_res == pytest.approx(0.000732)
    assert kc.kalman.gyro_res == pytest.approx(0.00875)
    assert kc.kalman.mag_res == pytest.approx(0.00058)


def test_EEPROM_version_timestamp(Mock_KCeptor, comm, def_conf, notochord_init, capfd):
    import time
    def_conf.comm.msg = comm.Output_Sink.PYTHON | comm.Output_Sink.STDOUT
    def_conf.comm.log_level = 2
    notochord_init.setup(def_conf)
    kc = Mock_KCeptor(None, 0x1b, "test_kc")
    kc.set_EEPROM_attributes(10101, time.time())
    kc.setup()



    

