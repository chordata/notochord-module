import sysconfig
import tarfile
import os
from tempfile import gettempdir
import json
from io import BytesIO
from sys import version_info as py_version

if (py_version.major, py_version.minor) < (3, 7):
    print("Python 3.7 or greater required, using {}.{} exiting".format(py_version.major, py_version.minor))
    from sys import exit 

    exit(1)

flags = {
    'CFLAGS' : sysconfig.get_config_var('CFLAGS'),
    'LDFLAGS' : sysconfig.get_config_var('LDFLAGS'),
    'EXT_SUFFIX' : sysconfig.get_config_var('EXT_SUFFIX'),
    'MULTIARCH' : sysconfig.get_config_var('MULTIARCH'),
    'LDVERSION' : sysconfig.get_config_var('LDVERSION')
}

incl_dir = sysconfig.get_config_var('INCLUDEDIR')
incl_path = sysconfig.get_path('include')
arch_incl_path = os.path.join(incl_dir, flags['MULTIARCH'])

tar_name = 'multiarch_files_{}.tar'.format(flags['MULTIARCH'])

path = os.path.join(gettempdir(), tar_name)

old_dir = os.getcwd()

include_py_name = os.path.basename(os.path.normpath(incl_path))


try:
    # ================= Adding basic include files ================= 
    print("== Found python include dir: {} ==".format(incl_path))
    os.chdir(incl_path)   # change directory

    files = sorted(os.listdir())

    tar = tarfile.open(path, "w")
    print("== Creating temp Tarball at: {} ==".format(path))

    for filename in files:
        arcname = os.path.join(include_py_name, filename)
        print("+adding:", arcname)
        if filename != tar_name:
            tar.add(filename, arcname=arcname)

    # ================= Adding multiarch include files ================= 
    print("== Found python multiarch include dir: {} ==".format(arch_incl_path))
    os.chdir(arch_incl_path)   # change directory

    files = sorted(os.listdir())

    for filename in files:
        arcname = os.path.join(flags['MULTIARCH'], filename)
        print("+adding:", arcname)
        if filename != tar_name:
            tar.add(filename, arcname=arcname)

    # ================= Adding conf vars JSON ================= 
    arcname = os.path.join(flags['MULTIARCH'], 'multiarch_conf_vars.json')
    tarinfo = tarfile.TarInfo(arcname)
    data = json.dumps(flags)
    tarinfo.size = len(data)

    print("+adding:", arcname)
    tar.addfile(tarinfo, BytesIO(bytes(data, encoding='utf-8')))
    
finally:
    tar.close()
    # --- go back to previuos directory
    os.chdir(old_dir)
    
final_location = os.path.join(old_dir, tar_name)
os.rename(path, final_location)

print("Created tarball:", final_location)
print("Place it in the 'multiarch' folder of the notochord-module project on compiling platform")