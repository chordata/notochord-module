/**
 * @file MadgwickAHRS.h
 * Implementation of Madgwick's IMU and AHRS algorithms.
 *
 * @author SOH Madgwick
 * @version 2.0 
 * @date 14/03/2017
 *  
 *=====================================================================================================
 * MadgwickAHRS.h
 *=====================================================================================================
 *
 * Implementation of Madgwick's IMU and AHRS algorithms.
 * From: http: http://www.x-io.co.uk/node/8#open_source_ahrs_and_imu_algorithms
 *
 * Date			Author          	Notes
 * 29/09/2011	SOH Madgwick    	Initial release
 * 02/10/2011	SOH Madgwick		Optimised for reduced CPU load
 * 
 * Modification:
 * 14/03/2017	Bruno Laurencich	Just some formal arrangement in order to adapt it to C++ OOP
 *
 * At the moment of downloading this file no licence was attached to the source,
 * but there was a notice on the website:
 *
 * "Open-source resources available on this website are provided under the 
 * [GNU General Public Licence](https://www.gnu.org/licenses/gpl.html) 
 * unless an alternative licence is provided in source."
 *
 * Following the link, the text of the GNU-GPLv3 license was displayed, so according to 
 * the directives from the Free software foudation, I'm including a license disclamer 
 * on this file:
 *
 * This file is part of the Implementation of Madgwick's IMU and AHRS algorithms.
 *	
 *    The Implementation of Madgwick's IMU and AHRS algorithms is free software: 
 *    you can redistribute it and/or modify it under the terms of the 
 *    GNU General Public License as published by the Free Software Foundation, 
 *    either version 3 of the License, or (at your option) any later version.
 *
 *    The Implementation of Madgwick's IMU and AHRS algorithms is distributed 
 *    in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 *    without even the implied warranty of MERCHANTABILITY or FITNESS 
 *    FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with The Implementation of Madgwick's IMU and AHRS algorithms.
 *    If not, see <https://www.gnu.org/licenses/>. * 
 */

//=====================================================================================================
#ifndef MadgwickAHRS_h
#define MadgwickAHRS_h

//----------------------------------------------------------------------------------------------------

namespace FSsf{
    extern "C" {
        #include "FS_sensor_fusion/build.h"
        #include "FS_sensor_fusion/types.h"
		#include "FS_sensor_fusion/orientation.h"
    }
}

class Fusion_Madgwick {
	float q0, q1, q2, q3;
public:
	Fusion_Madgwick(float hz, float _beta):
	q0(1.0f), q1(0.0f), q2(0.0f), q3(0.0f),
	sampleFreq(hz),
	beta(_beta)
	{};

	void MadgwickAHRSupdate(float gx, float gy, float gz, 
							float ax, float ay, float az, 
							float mx, float my, float mz) volatile;

	void MadgwickAHRSupdateIMU(float gx, float gy, float gz, 
								float ax, float ay, float az) volatile;

	const FSsf::fquaternion *get_quat(){
		static 	FSsf::fquaternion result;
		static const FSsf::fquaternion rotate_match_coord_sys = 
				{0.707106828689575f, 0.0f, 0.0f, 0.707106828689575f}; 
		const FSsf::fquaternion raw_quat{q0, q1, q2, q3};
		
		FSsf::qAeqBxC(&result, &rotate_match_coord_sys, &raw_quat );
		
		return &result;
	} 

	float sampleFreq;
	float beta;
};

#endif
//=====================================================================================================
// End of file
//=====================================================================================================
