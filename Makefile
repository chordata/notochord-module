NOTOCHORD_SETUP_PY?=python3.7
PYTHON=$(NOTOCHORD_SETUP_PY)
PY_LIB_LOC=$(shell $(PYTHON) -c "import sysconfig; print(sysconfig.get_config_var('srcdir'))")
SHELL:=/bin/bash
APT:=$(shell command -v apt-get)

NOTOCHORD_SETUP_VENV?=./venv
VENV_PY=$(NOTOCHORD_SETUP_VENV)/bin/python
VENV_PIP=$(NOTOCHORD_SETUP_VENV)/bin/pip
VENV_PYTEST=$(NOTOCHORD_SETUP_VENV)/bin/pytest
VENV_ACTIVATE=$(NOTOCHORD_SETUP_VENV)/bin/activate
SCONS=$(NOTOCHORD_SETUP_VENV)/bin/scons
CYTHON=$(NOTOCHORD_SETUP_VENV)/bin/cython

FMT:=$(shell ls lib/fmt)
SPDLOG:=$(shell ls lib/spdlog)
SCONSFLAGS?=-j2
ccflags?=-DUSE_SYNC_LOGGER


# ========== COMPILE ==========

all: check-deps
	$(SCONS) $(SCONSFLAGS) --cython=$(CYTHON) \
		--debug-build --ccflags="$(ccflags)"

explain:
	$(SCONS) $(SCONSFLAGS) --cython=$(CYTHON) \
		--debug-build --debug=explain --verbose |& tee .notochord_build.log

pedantic:
	$(SCONS) $(SCONSFLAGS) --cython=$(CYTHON) \
		--debug-build --debug=explain --pedantic |& tee .notochord_build.log

no-debug: check-deps
	$(SCONS) $(SCONSFLAGS) --cython=$(CYTHON) 

single-job:
	$(SCONS) -j1 --debug-build --cython=$(CYTHON) 

rebuild: clean all

cy-rebuild: touch-cy all

install: all
	$(VENV_PIP) install -e ./dist
	$(VENV_PIP) install 'notochord[dev]'

# ========== UTILS ==========

see-deps-tree: check-deps
	(SCONS) $(SCONSFLAGS) -Q --tree=all

touch-cy:
	rm -rf build/cy/*

clean:
	$(SCONS) --clean
	rm -rf build/*
	rm -rf dist/notochord

check-build: all
	$(VENV_PY) -c "import dist"

# ========== DEPENDENCIES & SETUP ==========

check-deps:
ifndef FMT
	$(error Missing libfmt, run `make setup` first.)
endif
ifndef SPDLOG
	$(error Missing libspdlog, run `make setup` first.)
endif

install-deps:
	./install_deps.sh $(PYTHON)

setup-submodules:
	git submodule init
	git submodule update --recursive

setup-venv:
	$(PYTHON) -m venv $(NOTOCHORD_SETUP_VENV)
	$(VENV_PIP) install 'scons==4.2.0' 'scons-compiledb==0.4.7' cython

	


setup: install-deps setup-submodules setup-venv  

# ========== MULTIARCH ==========

armhf: check-deps setup-armhf
	multiarch/extract_multiarch_tar.sh armhf
	$(SCONS) $(SCONSFLAGS) --armhf --cython=$(CYTHON) |& tee .notochord_armhf_build.log

arm64: check-deps setup-arm64
	multiarch/extract_multiarch_tar.sh arm64
	$(SCONS) $(SCONSFLAGS) --arm64 --verbose --cython=$(CYTHON) |& tee .notochord_arm64_build.log

GPP_64 := $(shell command -v aarch64-linux-gnu-g++ 2> /dev/null)
GPP_HF := $(shell command -v arm-linux-gnueabihf-g++ 2> /dev/null)

setup-armhf:
ifdef GPP_HF
	$(info arm-linux-gnueabihf-g++ found..)
else
ifndef APT
	$(warning "Only apt-get supported installation of dependencies, you should manually install: \n\
	 	- crossbuild-essential-armhf\n\
	 	")
else
	$(info Installing dependencies, you will be asked to enter your password)
	sudo apt install -y crossbuild-essential-armhf
endif
endif

setup-arm64:
ifdef GPP_64
	$(info aarch64-linux-gnu-g++ found..)
else
ifndef APT
	$(warning "Only apt-get supported installation of dependencies, you should manually install: \n\
	 	- crossbuild-essential-arm64\n\
	 	")
else
	$(info Installing dependencies, you will be asked to enter your password)
	sudo apt install -y crossbuild-essential-arm64
endif
endif

# ========== DEBUG ==========

debug: all
	gdb --args $(VENV_PY) -m pytest -vs

debug-test:
	gdb --args $(VENV_PY) -m pytest -vs test/test_$(TEST).py

debug-test-slow:
	gdb --args $(VENV_PY) -m pytest -vs test/test_$(TEST).py --slow

debug-scratch:
	export PYTHONPATH='test' && $(VENV_PY) -m scratchpad $(SKETCH) $(SKETCH)

debug-repl:
	gdb --args $(VENV_PY) -i -c "import notochord"

debug-cpp-scratch: _compile-cpp-scratch
	gdb ./test/cpp_scratchpad/notochord_scratch.out

# ========== TESTS ==========

test: all
	$(VENV_PYTEST)

just-test: 
	$(VENV_PYTEST) -x

i2c-test: 
	$(VENV_PYTEST) -vs --i2c

test-verbose: all
	$(VENV_PYTEST) -vs

slow-test: 
	$(VENV_PYTEST) --slow

slow-test-verbose: 
	$(VENV_PYTEST) --slow -vs

test-imports: cy-rebuild
	@-ln -s dist/notochord .
	-$(VENV_PY) -c "from notochord import config"
	-$(VENV_PY) -c "from notochord import communicator"
	-$(VENV_PY) -c "from notochord import core"
	@rm ./notochord

# ========== DEV UTILS ==========

# explore:
# 	-cd dist && $(RUN_PIPENV) run ipython
# 	cd -

scratch:
	@echo "Warning! this target is no compiling, just running the scratchpad"
	cd test && ../$(VENV_PY) -m scratchpad $(SKETCH)

repl:
	@echo "Warning! this target is no compiling, just running the repl"
	$(VENV_PY) -i -c "import notochord"

_compile-cpp-scratch:
	@echo This target is linking agaist the shared library which is 
	@echo compiled by other \'make\' targets such as \'all\'.
	@echo if you are testing changes in the notochord code 
	@echo you should run that target first.
	g++ -std=c++14 -g -O0 -Isrc -Ilib -Ilib/spdlog/include -DLWS_BUILDING_SHARED \
	-lcap \
	-I/usr/include/$(PYTHON) \
	-Ilib -Ilib/eigen-3.4.0 \
	-Ilib/fmt/include \
	-Ilib/LSM9DS1 \
	-Ilib/oscpack_1_1_0 \
	-Ilib/libwebsockets/lib \
	-Ilib/libwebsockets/include \
	-Ilib/build_lws \
	-Ldist/notochord  \
	-L$(PY_LIB_LOC) \
	-Wl,-rpath=dist/notochord \
	-Wall -Wextra -pedantic -pthread test/cpp_scratchpad/main.cpp \
	-Wno-unused-parameter \
	-lchordata -l$(PYTHON) \
	-o ./test/cpp_scratchpad/notochord_scratch.out

cpp-scratch: _compile-cpp-scratch
	./test/cpp_scratchpad/notochord_scratch.out

# ========== DOCS ==========

docs:
	@doxygen doc/Doxyfile
