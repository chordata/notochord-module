// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#ifndef __NOTOCHORD_LOGGER
#define __NOTOCHORD_LOGGER

//Uncoment this line to use a sync logger
// #define USE_SYNC_LOGGER

#include <memory>
#include <type_traits>
#include "types.h"
#include "math_types.h"
#include "osc.h"
#include "spdlog/spdlog.h"
#include "spdlog/async.h"
#include "spdlog/common.h"
#include "spdlog/sinks/null_sink.h"
#include "phy_nodes.h"

namespace Chordata{
	namespace Communicator{
		
		class Logger{
		private:
			std::shared_ptr<spdlog::logger> logger_ref;
		public:
			Logger(std::string name, spdlog::sink_ptr single_sink, bool _transmit_logger = false):
			osc_sink(std::shared_ptr<Chordata::Communicator::OscOut_Null>()),
			ws_sink(std::shared_ptr<Chordata::Communicator::OscOut_Null>()),
			is_transmit_logger(_transmit_logger),
			has_log_sinks(true)
			{
				if(spdlog::get(name) != nullptr) {
					logger_ref = spdlog::get(name);
				} else {
#ifdef USE_SYNC_LOGGER
					logger_ref = std::make_shared<spdlog::logger>(name, single_sink);
#else
					logger_ref = spdlog::create_async<spdlog::sinks::null_sink_st>(name);
#endif
				}
			}

			~Logger() {
			}

			template <typename Payload>
			void transmit(const std::string& subtarget, const Payload& payload) {
				osc_sink->transmit(subtarget, payload);
				ws_sink->transmit(subtarget, payload);

				if (has_log_sinks){
					spdlog::memory_buf_t buf;
					payload.format_to(subtarget, buf);

					spdlog::details::log_msg msg(spdlog::source_loc{}, 
												logger_ref->name(), spdlog::level::trace, 
												spdlog::string_view_t(buf.data(), buf.size()));
					
					for (auto &sink : logger_ref->sinks()){
						sink->log(msg);
					}
				}
			}

			template <typename Payload>
			void transmit(const _Node& n, const Payload& payload) {
				osc_sink->transmit(n, payload);
				ws_sink->transmit(n, payload);

				if (has_log_sinks){
					spdlog::memory_buf_t buf;
					payload.format_to(n.get_label(), buf); // TODO: FIXME

					spdlog::details::log_msg msg(spdlog::source_loc{}, 
												logger_ref->name(), spdlog::level::trace, 
												spdlog::string_view_t(buf.data(), buf.size()));
					
					for (auto &sink : logger_ref->sinks()){
						sink->log(msg);
					}
				}
			}

			void transmit(const _KCeptor_Base& kc) {
				osc_sink->transmit(kc);
				ws_sink->transmit(kc);				
			}

			void transmit(const _Armature& arm) {
				osc_sink->transmit(arm);
				ws_sink->transmit(arm);				
			}

			void flush_osc(){
				osc_sink->flush_();
			}

			void flush_websocket(){
				ws_sink->flush();
			}

			void clear_sinks();

			void add_sink(spdlog::sink_ptr new_sink, const Output_Redirect redir);

			Output_Redirect get_sinks() const;

			std::string get_trasmit_packet_hex() const;

			void set_level(spdlog::level::level_enum log_level){
				logger_ref->set_level(log_level);
			};

			void set_formatter(std::unique_ptr<spdlog::formatter> f){
				logger_ref->set_formatter(std::move(f->clone()));
			};

			void flush(){
				logger_ref->flush();
			};

			template <typename... Args>
			inline void info(const char* fmt, const Args&... args){
				logger_ref->info(fmt, args...);
			}

			template <typename... Args>
			inline void error(const char* fmt, const Args&... args){
				logger_ref->error(fmt, args...);
			} 

			template <typename... Args>
			inline void debug(const char* fmt, const Args&... args){
				logger_ref->debug(fmt, args...);
			} 

			template <typename... Args>
			inline void trace(const char* fmt, const Args&... args){
				logger_ref->trace(fmt, args...);
			} 

			template <typename... Args>
			inline void warn(const char* fmt, const Args&... args){
				logger_ref->warn(fmt, args...);
			} 

			template <typename... Args>
			inline void critical(const char* fmt, const Args&... args){
				logger_ref->critical(fmt, args...);
			} 

		private:
			osc_sink_ptr osc_sink;
			osc_sink_ptr ws_sink;
			bool is_transmit_logger, has_log_sinks, has_osc_sink, has_ws_sink;
		};
	}
}

#endif
