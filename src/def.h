// -- START OF COPYRIGHT & LICENSE NOTICES --
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can
// redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES --

#ifndef __NOTOCHORD_DEF__
#define __NOTOCHORD_DEF__

/////////////////////////////////
///The version of the program
/////////////////////////////////
#define _NOTOCHORD_VERSION_MAJOR 1
#define _NOTOCHORD_VERSION_MINOR 0
#define _NOTOCHORD_VERSION_PATCH 2
#define _NOTOCHORD_VERSION_POST ""

#define _GET_VERSION_INT(MAJ, MIN, PATCH) (uint32_t)(( MAJ * 10000 ) +\
													 ( MIN * 100 ) +\
													 ( PATCH ))
#define _GET_MAJOR_FROM_INT(VERSION) (VERSION/10000)%100
#define _GET_MINOR_FROM_INT(VERSION) (VERSION/100)%100
#define _GET_PATCH_FROM_INT(VERSION) VERSION%100


#define _NOTOCHORD_VERSION _GET_VERSION_INT( _NOTOCHORD_VERSION_MAJOR,\
									 		_NOTOCHORD_VERSION_MINOR,\
									  		_NOTOCHORD_VERSION_PATCH )

//this is the string "chordata", used for checking presence of data in EEPROM
#define _NOTOCHORD_CRC32 0xCBF43926

//////////////////////////////////
/// XML defaults
//////////////////////////////////
#define _CHORDATA_CONF_SCHEMA "./Chordata.xsd"
#define _CHORDATA_CONF "./Chordata.xml"

//////////////////////////////////
/// Communication defaults
//////////////////////////////////
#define _CHORDATA_VERBOSITY 0
#define _CHORDATA_SEND_RATE 25
#define _CHORDATA_FILEOUT_NAME "chord_log"
#define _CHORDATA_FILEOUT_DIR ".notochord_log"
#define _CHORDATA_FILEOUT_PATH fmt::format("{}/{}",_CHORDATA_FILEOUT_DIR, _CHORDATA_FILEOUT_NAME)
#define _CHORDATA_TRANSMIT_ADDR "127.0.0.1"
#define _CHORDATA_TRANSMIT_PORT (uint16_t) 6565
#define _CHORDATA_RUNNER_PORT (uint16_t) 50419
#define _CHORDATA_COMM_BUFFER_SIZE 512
#define _CHORDATA_PAYLOAD_BUFFER_SIZE 128
#define _CHORDATA_PAYLOAD_RESERVE_SLOTS 32
#define _CHORDATA_LOG_FLUSH_INTERVAL 5
#define _CHORDATA_I2C_DEFAULT_ADAPTER_ "/dev/i2c-1"
#define _CHORDATA_CYTHON_SINK_BUFFER_SIZE 100

#define ___CAST_REDIR_ static_cast<Chordata::Output_Redirect>
#define _CHORDATA_DEF_LOG_REDIR ___CAST_REDIR_ (Chordata::STDOUT|Chordata::FILE)
#define _CHORDATA_DEF_ERR_REDIR ___CAST_REDIR_ (Chordata::STDERR|Chordata::FILE)
#define _CHORDATA_DEF_TRA_REDIR ___CAST_REDIR_ (Chordata::OSC)

#define _CHORDATA_DEF_PROMPT "~>"
#define _CHORDATA_DEF_EOM "~#"

#define _CHORDATA_DEF_OSC_ADDR "/Chordata"
#define _CHORDATA_DEF_ERROR_SADDR "/error"
#define _CHORDATA_DEF_MSG_SADDR "/comm"
#define _CHORDATA_BUNDLE_REPL_MARK "/%%"
#define _CHORDATA_BUNDLE_PAYLOAD_MARK "/%"

#define _CHORDATA_PATTERN_FORMAT "[{{:>{}}}] {{}}"
#define _CHORDATA_LOGGING_PATTERN "%^%l~%$[%*] %v"

#define _CHORDATA_DEF_WEBSOCKET_PROTOCOL "Chordata"
#define _CHORDATA_DEF_WEBSOCKET_PORT 7681

#define _CHORDATA_THREAD_DELAY_MS 50

//////////////////////////////////
/// Nodes defaults
//////////////////////////////////
#define _CHORDATA_MUX_R1_ADDR 0x73
#define _CHORDATA_MUX_PP_ADDR 0x70

#define _CHORDATA_MUX_MAX_GATES 6

#define _MAX_AG_CALIB_SLOPE 0.12f

// The starting addrs of the chunks of calib data stored on the EEPROM
#define _CHORDATA_EEPROM_VAL_CHKSUM		0
#define _CHORDATA_EEPROM_VERSION		8
#define _CHORDATA_EEPROM_TIMESTAMP		12
#define _CHORDATA_EEPROM_GYRO_SPACE 	16
#define _CHORDATA_EEPROM_ACCEL_SPACE 	24
#define _CHORDATA_EEPROM_MAG_SPACE 		32
#define _CHORDATA_EEPROM_MMAG_SPACE		40

#define KCR1_EEPROM_ADDR 0x50
#define KCR2_EEPROM_ADDR 0x51

#define _CHORDATA_EEPROM_VERSION_0_1_2 _GET_VERSION_INT(0,1,2)
#define _CHORDATA_EEPROM_CURRENT_VERSION _CHORDATA_EEPROM_VERSION_0_1_2

// //////////////////////////////////
// /// Sensor fusion defaults
// //////////////////////////////////
#define _MADGWICK_BETA 0.2f
#define _KALMAN_OVERSAMPLE_RATIO 1

#define _CHORDATA_DEF_GYRO (Chordata::Gyro_Scale_Options::gyro_2000)
#define _CHORDATA_DEF_ACCEL (Chordata::Accel_Scale_Options::accel_8)
#define _CHORDATA_DEF_MAG (Chordata::Mag_Scale_Options::mag_4)
#define _CHORDATA_DEF_RATE (Chordata::Output_Data_Rate::OUTPUT_RATE_50)
#define _CHORDATA_DEF_ODR 50

#define KCPP_DEF_I2C_ADDR 0x40
#define KCPP_MIN_I2C_ADDR KCPP_DEF_I2C_ADDR
#define KCPP_MAX_I2C_ADDR 0x5F

#endif //EOF
