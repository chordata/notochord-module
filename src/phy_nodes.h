// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#ifndef __NOTOCHORD_NODE__
#define __NOTOCHORD_NODE__

#include <string>
#include <cstdint>

#include "hierarchy.h"
#include "math_types.h"
#include "types.h"
#include "io.h"
#include "utils.h"
#include "kalman.h"


#include "LSM9DS1.h"

namespace Chordata {
	/**
	 * _Node holds the basic information for creating representation of physical nodes.
	 */
	class _Node: public Link {
	protected:
		uint8_t address;
		io_ptr io;
				

	public:
		bool first_sensor;
		_Node(link_ptr _parent,
					uint8_t _address, std::string _label,
					io_ptr _io, 
					std::size_t max = _CHORDATA_MUX_MAX_GATES):
		Link(_parent, _label, max),
		address(_address),
		io(_io),
		first_sensor(false)
		{
		};

		virtual ~_Node(){};

		uint8_t get_address() const{ return address;}

		virtual bool is_sensor(){ return false; } //Shall this node be used to calculate the global ODR?
		
		void bang() override {}

		virtual void stop(){};

	};
	

// ====================================================== //
// ======================= Branch ======================= //
// ====================================================== //
	class _Mux; 
	using Mux = _Mux; //avoid name clash in cython
	using mux_ptr = std::shared_ptr<_Mux>;

	class _Branch: public _Node{
	protected:
		friend Mux;
		_Mux *thisMux;
		const Mux_Channel channel;
		const uint8_t channel_value;

		virtual void switch_branch();

		_Branch(mux_ptr _mux,
				uint8_t _address, std::string _label, 
				io_ptr _io, Mux_Channel _channel
				);

	public:
		~_Branch(){}

		void bang() override;

		Mux_Channel get_channel() const {return channel;}
		
	}; 	using Branch = _Branch; //avoid name clash in cython
	
	using branch_ptr = std::shared_ptr<_Branch>;

// ====================================================== //
// ========================= Mux ======================== //
// ====================================================== //

	class _Mux: public _Node {
	protected:
		Mux_Channel state;
		friend Branch;
		Branch off_branch;

		_Mux(	link_ptr _parent,
				uint8_t _address, std::string _label,
				io_ptr _io
			);
	public:

		~_Mux(){}

		static link_ptr Create(link_ptr _parent,
									uint8_t _address, std::string _label,
									io_ptr _i2c );

		Mux_Channel get_active_channel(){
			return state;
		}

		void bang_branch(Mux_Channel ch);

		Mux_Channel get_state() const { return state; }

	};

// ====================================================== //
// ==================== KCeptor_Base ==================== //
// ====================================================== //

	extern uint64_t global_round_micros;

	enum sensor_axis {
		X_AXIS = 0,
		Y_AXIS = 1,
		Z_AXIS = 2
	};

	enum KC_STATUS {
		SETUP    = 0,
		GX_CALIB = 1,
		WAITING  = 2,
		M_CALIB  = 3,
		SAVING   = 4,
		ACTIVE   = 5
	};

	class _KCeptor_Base: public _Node {
	protected:
		static int PENDING_KC;
		KC_STATUS status;
		bool setup_done;
		int odr; // Int value of KC_Rate
		// This value is calculated as (sensor scale) / (2^15)
		float gRes, aRes, mRes;
		int16_t gx, gy, gz; // x, y, and z axis readings of the gyroscope
		int16_t ax, ay, az; // x, y, and z axis readings of the accelerometer
		int16_t mx, my, mz; // x, y, and z axis readings of the magnetometer
		int16_t g_raw_read[3], a_raw_read[3], m_raw_read[3]; // Calibrated readings
		vector3_ptr g_offset, a_offset, m_offset;
		matrix3_ptr m_matrix;

		Chordata::Offset_Calibrator *g_calibrator, *a_calibrator;
		std::shared_ptr<Fusion_Kalman_Base> kalman;

		_KCeptor_Base(link_ptr _parent,
					uint8_t _address, std::string _label,
					io_ptr _io);

	public:
		~_KCeptor_Base();

		template<typename T>
		static link_ptr Create(link_ptr _parent,
                        uint8_t _address, std::string _label,
                        io_ptr _io ) {
			T* this_link_raw = new T(_parent, _address, _label, _io);	
			auto this_link = std::shared_ptr<T>(this_link_raw);

			if (_parent){
				_parent->add_child(this_link);
			}
			
			return this_link;			
		};

		bool check_status(); 

		void bang() override;

		///Only the K_Ceptor influence the global ODR
		bool is_sensor() override { return true; }

		// Maybe we could do the verification inside config and store the data there?
		bool verify_imu_settings();

		/*-------------- Getters --------------*/
		const Chordata::Offset_Calibrator *get_g_calibrator() const { return g_calibrator ;};
		const Chordata::Offset_Calibrator *get_a_calibrator() const { return a_calibrator ;};

		inline int16_t get_g_raw_read(sensor_axis axis) const { return g_raw_read[axis]; };
		inline int16_t get_a_raw_read(sensor_axis axis) const { return a_raw_read[axis]; };
		inline int16_t get_m_raw_read(sensor_axis axis) const { return m_raw_read[axis]; };

		inline const vector3_ptr get_g_offset() const { return g_offset; };
		inline const vector3_ptr get_a_offset() const { return a_offset; }; 
		inline const vector3_ptr get_m_offset() const { return m_offset; }; 
		inline const matrix3_ptr get_m_matrix() const { return m_matrix; }; 

		int get_odr() const { return odr; }; 
		std::shared_ptr<Fusion_Kalman_Base> get_kalman() { return kalman; };

		inline float get_g_read(sensor_axis axis){ return gRes * g_raw_read[axis]; };
		inline float get_m_read(sensor_axis axis){ return mRes * m_raw_read[axis]; };
		inline float get_a_read(sensor_axis axis){ return aRes * a_raw_read[axis]; };

		Accel_Scale_Options _get_accel_scale() { return Chordata::get_config()->sensor.accel_scale; };
		Gyro_Scale_Options _get_gyro_scale() { return Chordata::get_config()->sensor.gyro_scale; };
		Mag_Scale_Options _get_mag_scale()   { return Chordata::get_config()->sensor.mag_scale; };

		Output_Data_Rate _get_kcpp_rate()   { return Chordata::get_config()->sensor.output_rate; };

		/*-------------- Setters --------------*/
		void set_g_offset(int16_t,int16_t,int16_t);
		void set_a_offset(int16_t,int16_t,int16_t);
		void set_m_offset(int16_t,int16_t,int16_t);
		void set_m_matrix(matrix3_ptr);
		
		/*-------------- IMU Control --------------*/
		virtual bool setup() = 0;
		virtual void read_gyro() = 0;
		virtual void read_accel() = 0;
		virtual void read_mag(bool calc = true) = 0;
		virtual void read_sensors(bool calc = true) = 0;
		virtual void save_data() = 0;

		/*-------------- Calibration --------------*/
		float calibrate_m(int16_t,int16_t,int16_t,int16_t,int16_t,int16_t,int16_t,int16_t,int16_t);
		float calibrate_m();
		float calibrate_xg();
		void apply_mag_calib(int16_t*);
		void reset_xg_calib();
		bool set_state(int _status);

	}; 	

// ====================================================== //
// ======================= KCeptor ====================== //
// ====================================================== //
	
	class _KCeptor: public _KCeptor_Base {
	protected:
		friend _KCeptor_Base;
		LSM9DS1<IO_Base> imu;

		_KCeptor(link_ptr _parent,
			uint8_t _address, std::string _label,
			io_ptr _io);

		/*-------------- Private EEPROM manage --------------*/
		void write_g_offset_to_EEPROM() const;
		void write_a_offset_to_EEPROM() const;
		void write_m_offset_to_EEPROM() const;
		void write_m_matrix_to_EEPROM() const;

		void read_g_offset_from_EEPROM();
		void read_a_offset_from_EEPROM();
		void read_m_offset_from_EEPROM();
		void read_m_matrix_from_EEPROM();

	public:	
		static link_ptr Create(link_ptr _parent,
							uint8_t _address, std::string _label,
							io_ptr _io );

		/*-------------- Getters --------------*/
		const LSM9DS1<IO_Base> *get_imu() const { return &imu; };		

		/*-------------- IMU Control --------------*/
		bool setup() override;
		void read_gyro() override;
		void read_accel() override;
		void read_mag(bool calc = true) override;
		void read_sensors(bool calc = true) override;
		void stop() override {};	

		/*-------------- EEPROM Control --------------*/
		EEPROM_info get_info_from_EEPROM();
		void read_calib_from_EEPROM();
		void write_calib_to_EEPROM(const EEPROM_info *) const;
		void save_data() override;

		/*-------------- Utils --------------*/
		static bool is_kc_connected(io_ptr _io, uint8_t _address);
		

	}; using KCeptor = _KCeptor; //avoid name clash in cython

	using kceptor_ptr = std::shared_ptr<Chordata::_KCeptor>;
	
	enum class KCpp_i2c_mode
	{
		/*######## REGISTERS #######*/
		READ_AUTOINCREMENT 	= 0x00U,
		READ_WORD_GX		= 0x01U,
		READ_WORD_GY		= 0x02U,
		READ_WORD_GZ		= 0x03U,
		READ_WORD_AX		= 0x04U,
		READ_WORD_AY		= 0x05U,
		READ_WORD_AZ		= 0x06U,
		READ_WORD_MX		= 0x07U,
		READ_WORD_MY		= 0x08U,
		READ_WORD_MZ		= 0x09U,
		READ_PAYLOAD_DMA	= 0x0AU,
		
		/*--------- IDLE ---------*/
		I2C_MODE_IDLE		= 0x0FU,

		/*--------- CONF ---------*/
		CONF_READ_MODE		= 0x10U, //UNUSED as i2c interface, only as internal comparsion
		CONF_SET_MODE		= 0x11U, //UNUSED as i2c interface, only as internal comparsion

		/*########## VALUES #########*/
		/*--------- LSM9DS1 ---------*/
		G_SCALE				= 0x12U,
		A_SCALE				= 0x13U,
		M_SCALE				= 0x14U,
		RATE				= 0x15U,

		FETCH_AG_REG		= 0x1AU,
		FETCH_M_REG			= 0x1BU,

		INIT_IMU			= 0x1FU,

		/*--------- Calibration ---------*/

		SET_CALIB_GX_L		= 0x21U,
		SET_CALIB_GX_H		= 0x22U,
		SET_CALIB_GY_L		= 0x23U,
		SET_CALIB_GY_H		= 0x24U,
		SET_CALIB_GZ_L		= 0x25U,
		SET_CALIB_GZ_H		= 0x26U,
		SET_CALIB_AX_L		= 0x27U,
		SET_CALIB_AX_H		= 0x28U,
		SET_CALIB_AY_L		= 0x29U,
		SET_CALIB_AY_H		= 0x2AU,
		SET_CALIB_AZ_L		= 0x2BU,
		SET_CALIB_AZ_H		= 0x2CU,
		SET_CALIB_MX_L		= 0x2DU,
		SET_CALIB_MX_H		= 0x2EU,
		SET_CALIB_MY_L		= 0x2FU,
		SET_CALIB_MY_H		= 0x30U,
		SET_CALIB_MZ_L		= 0x31U,
		SET_CALIB_MZ_H		= 0x32U,
			
		CALIB_MATRIX_ROW1	= 0x33U,
		CALIB_MATRIX_ROW2	= 0x34U,
		CALIB_MATRIX_ROW3	= 0x35U,

		CALIB_OFFSET		= 0x3AU,
		CALIB_MATRIX		= 0x3BU, // This command works but most i2c implementations trim the payload at 32bits and we need 36 to r/w the whole matrix
		CALIB_TIMESTAMP		= 0x3CU,

		STORE_CALIB			= 0x3FU,

		/*--------- I2C ADDR! ---------*/
		SET_I2C_ADDR		= 0xA0U,

		/*--------- ERRORS ---------*/

		GET_LAST_ERRNO		= 0xE0U,
		GET_NUM_ERRORS		= 0xE1U

	};

	class _KCeptorPP: public _KCeptor_Base {
	protected:
		friend _KCeptor_Base;
		_KCeptorPP(link_ptr _parent,
				uint8_t _address, std::string _label,
				io_ptr _io);
	public:				
		static link_ptr Create(link_ptr _parent,
							uint8_t _address, std::string _label,
							io_ptr _io );

		bool setup() override;

		void read_gyro() override ;
		void read_accel() override;
		void read_mag(bool calc = true) override;
		void read_sensors_DMA(bool calc);
		void read_sensors(bool calc = true) override;

		/*-------------- Calibration --------------*/

		bool save_offsets(int16_t *data, int count);
		bool save_matrix(int32_t *data, int count, KCpp_i2c_mode cmd);
		void save_data() override;

		void read_data();

		void change_i2c_addr(uint8_t addr);
		
		void stop() override;

		/*-------------- Utils --------------*/
		static bool is_kc_connected(io_ptr _io, uint8_t _address);

	}; using KCeptorPP = _KCeptorPP; //avoid name clash in cython

	using kceptorpp_ptr = std::shared_ptr<Chordata::_KCeptorPP>;

}



#endif