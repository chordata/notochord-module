// -- START OF COPYRIGHT & LICENSE NOTICES --
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can
// redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES --

#ifndef __NOTOCHORD_TYPES__
#define __NOTOCHORD_TYPES__

#include <memory>
#include <unordered_map>
#include "spdlog/spdlog.h"
#include "math_types.h"

namespace Chordata{

	/*----------  RUNTIME  ----------*/
	class Notochord_Runtime;
	class Communicator_Runtime;
	using comm_runtime_ptr = std::shared_ptr<Communicator_Runtime>;

	enum _Notochord_Status {
		IDLE,
		RUNNING,
		STOPPED
	}; using Notochord_Status = _Notochord_Status;

	/*----------  i2c  ----------*/
	class IO_Base;
	using io_ptr = std::shared_ptr<IO_Base>;

	/*----------  timer  ----------*/
	class _Timekeeper;
	using timek_ptr = std::shared_ptr<_Timekeeper>;
	using timek_index = std::unordered_map<std::string, timek_ptr>;
	class _Node_Scheduler;
	using scheduler_ptr = std::shared_ptr<_Node_Scheduler>;
	class _Timer;
	using timer_ptr = std::unique_ptr<_Timer>;

	/*----------  EEPROM  ----------*/
	struct EEPROM_info{
		uint32_t validation, version, timestamp;
	};

	/*---------- ARMATURE  ----------*/
	class _Armature;
	using armature_ptr = std::shared_ptr<_Armature>;
	
	/*----------  COMMUNICATOR  ----------*/
	namespace Communicator{
		class OscOutBase;
		class Logger;
	}

	using logger_ptr = std::shared_ptr<Communicator::Logger>;
	using osc_sink_ptr = std::shared_ptr<Chordata::Communicator::OscOutBase>;

	enum Logger_type{
		MSG_LOG,
		ERR_LOG,
		TRA_LOG
	};

	enum Output_Redirect{
		NONE = 0,
		STDOUT = 1,
		STDERR = 1<<1,
		FILE = 1<<2,
		OSC= 1<<3,
		PYTHON = 1<<4,
		WEBSOCKET = 1<<5
	};

	const uint16_t _int_gyro_scale [4] = {245, 500, 2000, 2000};
	enum _Gyro_Scale_Options{
		gyro_245 = 0,
		gyro_500 = 1,
		gyro_2000 = 2
	}; using Gyro_Scale_Options = _Gyro_Scale_Options;

	const uint8_t _int_accel_scale [4] = {2, 16, 4, 8};
	enum _Accel_Scale_Options{
		accel_2 = 0,
		accel_16 = 1,
		accel_4 = 2,
		accel_8 = 3
	}; using Accel_Scale_Options = _Accel_Scale_Options;

	const uint8_t _int_mag_scale [4] = {4, 8, 12, 16};
	enum _Mag_Scale_Options{
		mag_4 = 0,
		mag_8 = 1,
		mag_12 = 2,
		mag_16 = 3
	}; using Mag_Scale_Options = _Mag_Scale_Options;

	const uint8_t _int_ODR [3] = {50, 119, 238};
	enum _Output_Data_Rate {
		OUTPUT_RATE_50 = 0,
		OUTPUT_RATE_119 = 1,
		OUTPUT_RATE_238 = 2
	}; using Output_Data_Rate = _Output_Data_Rate;


	enum _Kc_Values {
		PlusPlus = 1,
		Two = 2
	}; using Kc_Values = _Kc_Values;

	#define OUTPUT_REDIRECT_N 6

	inline Output_Redirect& operator |=(Output_Redirect& a, Output_Redirect b)
	{
	    return a= static_cast<Output_Redirect>(static_cast<char>(a) | static_cast<char>(b));
	}

	inline Output_Redirect& operator |(Output_Redirect& a, Output_Redirect &b)
	{
	    return a= static_cast<Output_Redirect>(static_cast<char>(a) | static_cast<char>(b));
	}

	inline Output_Redirect& operator &=(Output_Redirect& a, Output_Redirect b)
	{
	    return a= static_cast<Output_Redirect>(static_cast<char>(a) & static_cast<char>(b));
	}

	inline Output_Redirect& operator &(Output_Redirect& a, Output_Redirect &b)
	{
	    return a= static_cast<Output_Redirect>(static_cast<char>(a) & static_cast<char>(b));
	}

	inline Output_Redirect operator ~(Output_Redirect& a)
	{
	    return static_cast<Output_Redirect>(~ static_cast<char>(a));
	}


	/**
	 * The register values for opening each of the channels on the Mux
	 */
	enum _Mux_Channel{
		OFF = 0, ///< 0
		CH_1 = 1, ///< 0x1
		CH_2 = 2, ///< 0x2
		CH_3 = 3, ///< 0x4
		CH_4 = 4, ///< 0x8
		CH_5 = 5, ///< 0x10
		CH_6 = 6, ///< 0x20
	}; 	using Mux_Channel = _Mux_Channel;

} // end namespace Chordata


#endif