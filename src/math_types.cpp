// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#include "math_types.h"


using Chordata::matrix3_ptr;
using Chordata::_Matrix3;

_Matrix3 Chordata::create_Matrix3(float _00, float _01, float _02,
                                  float _10, float _11, float _12,
                                  float _20, float _21, float _22) {
    _Matrix3 m =  _Matrix3{{_00,  _01, _02},
                           {_10, _11, _12},
                           {_20, _21, _22}};

    return m;
}

matrix3_ptr Chordata::create_Matrix3_ptr(float _00, float _01, float _02,
                float _10, float _11, float _12,
                float _20, float _21, float _22){
    
    _Matrix3 *m = new _Matrix3{{_00,  _01,  _02 },
                                {_10,  _11,  _12},
                                {_20,  _21,  _22}};

    return std::shared_ptr<_Matrix3>(m);
};


matrix3_ptr Chordata::create_Matrix3_ptr(){
    
    _Matrix3 *m = new _Matrix3{{1, 0, 0 },
                                {0, 1, 0},
                                {0, 0, 1}};

    return std::shared_ptr<_Matrix3>(m);
};

using Chordata::matrix4_ptr;
using Chordata::_Matrix4;

_Matrix4 Chordata::create_Matrix4(float _00, float _01, float _02, float _03,
                float _10, float _11, float _12, float _13,
                float _20, float _21, float _22, float _23,
                float _30, float _31, float _32, float _33){
    
    _Matrix4 m =  _Matrix4{{_00,  _01, _02, _03},
                                {_10, _11, _12, _13},
                                {_20, _21, _22, _23},
                                {_30, _31, _32, _33}};

    return m;
};

matrix4_ptr Chordata::create_Matrix4_ptr(float _00, float _01, float _02, float _03,
                float _10, float _11, float _12, float _13,
                float _20, float _21, float _22, float _23,
                float _30, float _31, float _32, float _33){
    
    _Matrix4 *m = new _Matrix4{{_00,  _01, _02, _03},
                                {_10, _11, _12, _13},
                                {_20, _21, _22, _23},
                                {_30, _31, _32, _33}};

    return std::shared_ptr<_Matrix4>(m);
};


matrix4_ptr Chordata::create_Matrix4_ptr(){
    
    _Matrix4 *m = new _Matrix4{ {1, 0, 0, 0},
                                {0, 1, 0, 0},
                                {0, 0, 1, 0},
                                {0, 0, 0, 1}};

    return std::shared_ptr<_Matrix4>(m);
};