// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

//#include "fmt/chrono.h"
#include "mock_phy_nodes.h"
#include "timer.h"
#include "communicator.h"

// #include <iostream>
using Chordata::link_ptr;
using Chordata::mock_mux_ptr;
using Chordata::mock_branch_ptr;
using Chordata::mock_kceptor_ptr;
namespace comm = Chordata::Communicator;

// ====================================================== //
// ========================= _Mux ======================= //
// ====================================================== //

Chordata::_Mock_Mux::_Mock_Mux( link_ptr _parent,
            uint8_t _address, std::string _label,
            io_ptr _io
        ):
    _Mux(_parent, _address, _label, _io),
    off_branch(nullptr, _address, "off_branch", _io, OFF)
    {
        //comm::_trace("(+Node) Created Mux:{}, addr: {:#04x} | parent: {}", _label, 
        //                                        // _address, (_parent)? _parent->get_label() : "None");

    };

// -------- Factory ------- //

link_ptr Chordata::_Mock_Mux::Create(link_ptr _parent,
                                    uint8_t _address, std::string _label,
                                    io_ptr _io )
{

    _Mock_Mux* this_link_raw = new _Mock_Mux(_parent, _address, _label, _io);
    mock_mux_ptr this_link = std::shared_ptr<_Mock_Mux>(this_link_raw);
    
    this_link->off_branch.parent = this_link;;
    this_link->off_branch.thisMux = this_link.get();
    this_link->off_branch.bang();

    for (std::size_t i = 0; i < this_link->max_children; i++){
        Mock_Branch *new_branch_raw = new Mock_Branch(this_link, _address, 
                                             fmt::format("{}.CH_{}", _label, i+1), 
                                            _io, static_cast<Mux_Channel>( i+1 ) );

        link_ptr new_branch = std::shared_ptr<Mock_Branch>(new_branch_raw);
        this_link->add_child(new_branch);

    }  
        
    if (_parent){
        _parent->add_child(this_link);
    }
    return this_link;
} 

// ====================================================== //
// ======================= Branch ======================= //
// ====================================================== //

Chordata::_Mock_Branch::_Mock_Branch(	mock_mux_ptr _mux,
                            uint8_t _address, std::string _label, 
                            io_ptr _io, Mux_Channel _channel):
    _Branch(_mux, _address, _label, _io, _channel),
    thisMux(_mux.get())
    {
    };


void Chordata::_Mock_Branch::bang(){

    for (int i = 0; i < 5; ++i){
        try{
            switch_branch();
            thisMux->state = channel;
            return;
        } catch (const Chordata::IO_Error& e){
            comm::_error("error switching branch {}. Retrying.. ({})", get_label(), i);
            // thread_sleep(Chordata::millis(20));
        }
    }

    comm::_error("Too many errors when switching branch {}.", get_label());
    throw IO_Error("Can't write to _Mux");

}

void Chordata::_Mock_Branch::switch_branch(){
    // Estimated duration for this method's call
    //throw IO_Error("Testing error communications");
    usleep((float)(rand() % 10 + 80)/1000);
}

// ====================================================== //
// ======================= KCeptor ====================== //
// ====================================================== //

Chordata::_Mock_KCeptor::_Mock_KCeptor(link_ptr _parent,
            uint8_t _address, std::string _label,
            io_ptr _io):
    _KCeptor(_parent, _address, _label, _io)

    {        
        get_runtime()->add_index_timekeeper(std::make_shared<_Timekeeper>(_label));  
        g_raw_read[0] = 5;
        g_raw_read[1] = 125;
        g_raw_read[2] = 590;

        a_raw_read[0] = 1;
        a_raw_read[1] = 54;
        a_raw_read[2] = 34;

        m_raw_read[0] = 654;
        m_raw_read[1] = 453;
        m_raw_read[2] = 231;
        
    }

Chordata::_Mock_KCeptor::~_Mock_KCeptor(){
}

// -------- Factory ------- //
link_ptr Chordata::_Mock_KCeptor::Create(link_ptr _parent,
                                    uint8_t _address, std::string _label,
                                    io_ptr _io ){

    _Mock_KCeptor* this_link_raw = new _Mock_KCeptor(_parent, _address, _label, _io);
    mock_kceptor_ptr this_link = std::shared_ptr<_Mock_KCeptor>(this_link_raw);
            
    if (_parent){
        _parent->add_child(this_link);
    }
    
    return this_link;
} 

// -------- Methods ------- //

void Chordata::_Mock_KCeptor::bang() {
    // Store KCeptor lap time for each bang
        srand(std::hash<std::string>{}(get_label()));
        Chordata::Communicator::_transmit(this, new Quaternion(rand()%100, rand()%100, rand()%100,rand()%100));
        Chordata::Communicator::_transmit(this);
        //Chordata::Communicator::transmit("/%/some_addr", Quaternion(1,2,3,4));
    //}
    //if (debug_buffer.size() == (rand() % 2)) {
    Quaternion q = Quaternion(rand()%100, rand()%100, rand()%100,rand()%100);

    Chordata::get_runtime()->get_calibration_manager().add_payload(global_round_micros, this, std::move(q));

    if (this->first_sensor)
        Chordata::Communicator::flush_loggers();
    debug_buffer.push_back(_get_hz());
    // From empiric measurements a KCeptor payload read takes between 400 and 700 us
    usleep((float)(rand() % 300 + 400)/1000);
    
    if (debug_buffer.size() == (rand() % 400)) {        
        throw Chordata::Notochord_Error("Mock_KCeptor test error. Timer should keep running.");
    }
}

void Chordata::_Mock_KCeptor::apply_mag_calib(int16_t *a, int16_t x, int16_t y, int16_t z) {
    a[X_AXIS] = x - (*m_offset)[X_AXIS];
    a[Y_AXIS] = y - (*m_offset)[Y_AXIS];
    a[Z_AXIS] = z - (*m_offset)[Z_AXIS];

    float b[3] = {(float)a[0], (float)a[1], (float)a[2]};
    float c[3] = {0,0,0};
    for (int i=0;i<3;i++){
        for (int j=0;j<3;j++){
            c[i]+=( (*m_matrix)(i,j)*b[j]);
        }
    }
    a[X_AXIS] = (int16_t)c[X_AXIS];
    a[Y_AXIS] = (int16_t)c[Y_AXIS];
    a[Z_AXIS] = (int16_t)c[Z_AXIS];
}

float Chordata::_Mock_KCeptor::_get_hz() {
    uint64_t aux = get_runtime()->get_timekeeper(get_label())->micros();
    get_runtime()->get_timekeeper(get_label())->reset();
    return ((float)1000000.0/aux);
}

void Chordata::_Mock_KCeptor::set_EEPROM_attributes(uint32_t version, uint32_t timestamp){
    EEPROM_version = version;
    EEPROM_timestamp = std::time_t(timestamp);
}

bool Chordata::_Mock_KCeptor::setup() {
    uint32_t version = _GET_VERSION_INT(1,2,3);
    set_EEPROM_attributes(version, std::time(0));
    trace_EEPROM_info(version, EEPROM_timestamp);
    return true;
}