// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#ifndef __FUSION_WORKER__
#define __FUSION_WORKER__

#include "math_types.h"
#include <queue>
#include <mutex>
#include <memory>

namespace Chordata{	
	class _KCeptor_Base;

	struct Worker_Task{
		protected:
		_KCeptor_Base* node;
		uint64_t time;

		public:
		// Constructor
		Worker_Task(_KCeptor_Base* node, uint64_t time) :
			node(node), time(time) {}

		virtual void run() = 0;
	};

	struct Fusion_Task : public Worker_Task {
		private:
		int16_t gx, gy, gz, ax, ay, az, mx, my, mz;

		public:
		// Constructor
		Fusion_Task(_KCeptor_Base* node, uint64_t time, int16_t gx, int16_t gy, int16_t gz, int16_t ax, int16_t ay, int16_t az, int16_t mx, int16_t my, int16_t mz) :
			Worker_Task(node, time), gx(gx), gy(gy), gz(gz), ax(ax), ay(ay), az(az), mx(mx), my(my), mz(mz) {}

		void run() override;
	};

	struct Armature_Task : public Worker_Task {
		private:
		Quaternion q;

		public:
		// Constructor
		Armature_Task(_KCeptor_Base* node, uint64_t time, Quaternion q) :
			Worker_Task(node, time), q(q) {}

		void run() override;
	};

    class Notochord_Worker{
	public:
		using Task = std::unique_ptr<Worker_Task>;

		void enqueue(Task&& t);

		void consume();

		void handler();

		void stop();

		Notochord_Worker(): is_running(true){};
		~Notochord_Worker(){ stop(); }

	private:
		std::queue<std::unique_ptr<Worker_Task>> q;
		std::mutex m;
		std::condition_variable c;
		volatile bool keep_running, is_running;
		// std::thread th;
	};

}

#endif