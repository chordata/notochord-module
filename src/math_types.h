// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#ifndef __NOTOCHORD_PAYLOAD_
#define __NOTOCHORD_PAYLOAD_

#include <memory>
#include "spdlog/spdlog.h"
#include <iostream>
// Add plugins and include Eigen Matrix, Vector and Quaternion types
// Don't change the order of these statements!
// = These plugins add a member method: 
// = void format_to(const std::string&, spdlog::memory_buf_t& buf) const
// = to convert them to strings
#define MATH_FMT_NUM_PRECISION "4"
#define EIGEN_MATRIXBASE_PLUGIN "eigen_plugins/matrix_plugin.h"
#define EIGEN_QUATERNION_PLUGIN "eigen_plugins/quaternion_plugin.h"
#include <Eigen/Dense>
#include <Eigen/Geometry> 
// end include Eigen types

namespace Chordata{

	using Quaternion = Eigen::Quaternionf;
	using _Quaternion = Eigen::Quaternionf; 
	using quat_ptr = std::shared_ptr<_Quaternion>;

	using Vector3 = Eigen::Vector3f;
	using _Vector3 = Eigen::Vector3f;
	using vector3_ptr = std::shared_ptr<_Vector3>;

	using Vector3i = Eigen::Matrix<int16_t, 3, 1>;
	using _Vector3i = Eigen::Matrix<int16_t, 3, 1>;

	using Matrix3 = Eigen::Matrix3f;
	using _Matrix3 = Eigen::Matrix3f;
	using matrix3_ptr = std::shared_ptr<_Matrix3>;

	_Matrix3 create_Matrix3(float m00, float m01, float m02,
							float m10, float m11, float m12,
							float m20, float m21, float m22);

	matrix3_ptr create_Matrix3_ptr(float _00, float _01, float _02,
					float _10, float _11, float _12,
					float _20, float _21, float _22);

	matrix3_ptr create_Matrix3_ptr();

	using Matrix4 = Eigen::Matrix4f;
	using _Matrix4 = Eigen::Matrix4f;
	using matrix4_ptr = std::shared_ptr<_Matrix4>;

	_Matrix4 create_Matrix4(float _00, float _01, float _02, float _03,
							 float _10, float _11, float _12, float _13,
							 float _20, float _21, float _22, float _23,
							 float _30, float _31, float _32, float _33);

	matrix4_ptr create_Matrix4_ptr(float _00, float _01, float _02, float _03,
								   float _10, float _11, float _12, float _13,
								   float _20, float _21, float _22, float _23,
								   float _30, float _31, float _32, float _33);

	matrix4_ptr create_Matrix4_ptr();

}
#endif