// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#ifndef __CHORDATA_KALMAN__
#define __CHORDATA_KALMAN__ 

#include "kalman_base.h"

namespace Chordata{

	class Fusion_Kalman_Raw: public Fusion_Kalman_Base{
	public:
		Fusion_Kalman_Raw(FSsf::uint16 sensorFs, FSsf::uint8 oversampleRatio, bool enable_mag_correct = false):
		Fusion_Kalman_Base(sensorFs, oversampleRatio, enable_mag_correct)
		{}

		~Fusion_Kalman_Raw() override {};
		
		void set_gyro(FSsf::int16, FSsf::int16, FSsf::int16) override;
		void set_accel(FSsf::int16, FSsf::int16, FSsf::int16) override;
		void set_mag(FSsf::int16, FSsf::int16, FSsf::int16) override;

		//void get_mag_offset_for_EEPROM(FSsf::int16 *magOffset) const override;
		void get_mag_offset_for_EEPROM(vector3_ptr magOffset) const override;
		void get_mag_matrix_for_EEPROM(float (&magMatrix)[3][3]) const override;

		void set_mag_offset_from_EEPROM(const FSsf::int16 *magOffset) override;
		void set_mag_matrix_from_EEPROM(const float (&magMatrix)[3][3]) override;
		
	};

	class Fusion_Kalman_LSM9DS1: public Fusion_Kalman_Base{
	public:
		Fusion_Kalman_LSM9DS1(FSsf::uint16 sensorFs, FSsf::uint8 oversampleRatio, bool enable_mag_correct = false);

		~Fusion_Kalman_LSM9DS1() override {};
		
		void set_gyro(FSsf::int16, FSsf::int16, FSsf::int16) override;
		void set_accel(FSsf::int16, FSsf::int16, FSsf::int16) override;
		void set_mag(FSsf::int16, FSsf::int16, FSsf::int16) override;

		void get_mag_offset_for_EEPROM(vector3_ptr magOffset) const override;
		void get_mag_matrix_for_EEPROM(float (&magMatrix)[3][3]) const override;

		void set_mag_offset_from_EEPROM(const FSsf::int16 *magOffset) override;
		void set_mag_matrix_from_EEPROM(const float (&magMatrix)[3][3]) override;
		
	};
}







#endif