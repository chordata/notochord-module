// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#ifndef __NOTOCHORD_ARMATURE__
#define __NOTOCHORD_ARMATURE__ 

#include <vector>
#include <exception>
#include <memory>
#include <map>
#include <iostream>

#include "def.h"
#include "chord_error.h"

namespace Chordata {

	class Link;
	class _Hierarchy;
	using link_ptr = std::shared_ptr<Link>;
	using link_wptr = std::weak_ptr<Link>;

	struct Link_Info{
		std::string label;
		std::string parent_label;
		link_wptr parent; 
		std::vector<link_wptr>children; 
		std::vector<std::string>children_label;
		long shared_ptr_count;
	};

	using link_index = std::map<std::string, Link_Info>;

	/*=============================
	=            LINKS            =
	=============================*/
	
	class Link {
		friend _Hierarchy;
		std::vector<link_ptr> children;
	
	protected:
		link_wptr parent;
		const std::string label;
		const std::size_t max_children;
		const std::string osc_q_header, osc_r_header, osc_bundle_header;

		Link(link_ptr _parent, std::string _label, std::size_t max):
		parent(_parent),
		label(_label),
		max_children(max),
		osc_q_header(get_config()->osc.base_address+"/q/"+_label),
		osc_r_header(get_config()->osc.base_address+"/r/"+_label),
		osc_bundle_header("/%/"+_label)
		{};

	public:
		virtual ~Link(){}

		link_wptr get_parent() const { return parent; }

		std::string get_label() const{
			return label;
		}
		
		link_ptr get_child( std::size_t nChild = 0 ) const{
			if (nChild >= children.size())
				throw Hierarchy_Error("This Link has only " + std::to_string(children.size()) + ", asked for the #" +std::to_string(nChild));

			return children[nChild];
		}

		link_wptr get_weak_child( std::size_t nChild = 0 ) const{
			if (nChild >= children.size())
				throw Hierarchy_Error("This Link has only " + std::to_string(children.size()) + ", asked for the #" +std::to_string(nChild));

			return children[nChild];
		}

		std::size_t get_children_n() const { return children.size(); }

		Link &add_child(link_ptr _child)  {
			if ( get_children_n() >= max_children )
				throw Chordata::Hierarchy_Error("Maximum allowed number of children for this Link");	
			
			children.push_back(_child);
			return *this;
		}

		std::size_t get_max_children() const  {return max_children;}

		const std::string get_osc_q_header() const { return osc_q_header; };
		const std::string get_osc_r_header() const { return osc_r_header; };
		const std::string get_bundle_header() const { return osc_bundle_header; };

		virtual void bang() = 0;
	};

	/*================================
	=            HIERARCHY           =
	================================*/
	
	class _Hierarchy{
	public:
		_Hierarchy( link_ptr _root_node ):
		root_node(_root_node), empty_node(nullptr)
		{};

		link_ptr get_root_node(){ return root_node; }

		template <typename FN, typename... Targs>
		void walk(FN handler, Targs... Fargs){
			walk_link(root_node, handler, std::forward<Targs>(Fargs)... );
		}

		static Link_Info get_link_info(link_wptr the_wlink);

		void build_index(){
			walk(build_link_index, this);
		}

		const link_index get_index() const {
			//TODO: BUILD if not already done!
			 return index;
		};

		const Link_Info& get_link_info(const std::string& label){
			if(index.find(label) != index.end()){
				return index.at(label);
			}
			if(empty_node == nullptr){
				empty_node = std::make_shared<Link_Info>();
				empty_node->shared_ptr_count = 0;
			}

			return *(empty_node.get());
		}

		bool node_exists(const std::string& label) const{
			return index.find(label) != index.end();
		}

	private:
		
		static void build_link_index(link_wptr the_wlink, _Hierarchy *h);

		template <typename FN, typename... Targs>
		void walk_link(link_wptr the_wlink, FN handler, Targs... Fargs){
			handler(the_wlink, Fargs...);

			if ( link_ptr the_link = the_wlink.lock()){

				for (size_t i = 0; i < the_link->get_children_n(); i++)
				{
					walk_link(std::move(the_link->get_weak_child(i)), handler, std::forward<Targs>(Fargs)...);
				}
			} else {
				throw Chordata::Hierarchy_Error("Hierarchy::walk trying to referece an expired pointer");
			}
		}

		link_ptr root_node;
		link_index index;
		std::shared_ptr<Link_Info> empty_node;
	}; using Hierarchy = _Hierarchy;

}
#endif