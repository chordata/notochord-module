#ifndef INCLUDED_SWAP_OSCOUTBOUNDPACKETSTREAM_H
#define INCLUDED_SWAP_OSCOUTBOUNDPACKETSTREAM_H

#include "osc/OscOutboundPacketStream.h"

namespace osc{
    class SwapOutboundPacketStream: public OutboundPacketStream{
    public:
        SwapOutboundPacketStream( char *buffer, std::size_t capacity );
        ~SwapOutboundPacketStream();
        SwapOutboundPacketStream& Swap( SwapOutboundPacketStream& rhs );

        // Delete the copy constructor and assignment operator
        SwapOutboundPacketStream( const SwapOutboundPacketStream& ) = delete;
        SwapOutboundPacketStream& operator=( const SwapOutboundPacketStream& ) = delete;
    };
}

#endif