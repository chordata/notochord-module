// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#include "timer.h"
#include "communicator.h"

#define LAP_MODIF_FACTOR 400
#define LAP_TOLERANCE 100
#define LAP_CORRECT_TIME_MARGIN 1*1000
#define BRANCH_WRITE_uS 45 // This is an empirical value

Chordata::_Timekeeper::_Timekeeper(const std::string &_label):
    start(clock::now()),
    lap(clock::now()),
    label(_label)
    {
        if (!Notochord_Runtime::is_active())
        {
            throw Chordata::Notochord_Error("The Notochord_Runtime should be initiated before creating a TimeKeeper instance");
        }
    };

// ====================================================== //
// =================== Node Scheduler =================== //
// ====================================================== //

Chordata::Node_Scheduler& Chordata::Node_Scheduler::step() {
	// The scheduler gets loaded in a manner that the step has to be made backwards
	if (current-- == nodelist.begin())
	{
		current = nodelist.end()-1;
	}
	return *this;
};

void Chordata::Node_Scheduler::do_search(){
	try{
		consume_buffer.push((*current).get()); 
		
		step();

		while (!(*current)->is_sensor()) {
			consume_buffer.push((*current).get());
			step();
		}

		while (!consume_buffer.empty()){
			auto el = consume_buffer.front();
#ifdef PEDANTIC_TRACE
			Chordata::Communicator::_trace("Bang node: {}, addr:{:#04x}", el->get_label(), el->get_address());
#endif
			el->bang();
			consume_buffer.pop();
		}
	} catch (const Chordata::IO_Error& e){
		auto el = consume_buffer.front();
		Chordata::Communicator::_error("Error in Node r/w: {}, addr:{:#04x}", el->get_label(), el->get_address() );
		consume_buffer.pop();
	} catch (const std::exception& e){
		Chordata::Communicator::_error("Error in main Timer: {}", e.what() );
		consume_buffer.pop();
	}
}

const Chordata::Node_Scheduler::scheduler_citer seek_first_sensor(const std::vector<std::shared_ptr<Chordata::_Node>>& list){
	for (Chordata::Node_Scheduler::scheduler_citer i = list.begin(); i != list.end(); ++i){
		if ((*i)->is_sensor()) {
			(*i)->first_sensor = true;
			Chordata::Communicator::_info("First sensor in Scheduler: {}", (*i)->get_label());
			return i;
		}
	}
	Chordata::Communicator::_error("No sensor found in Scheduler");
	throw Chordata::Notochord_Error("There are no sensors in Node Scheduler");	
}

void Chordata::Node_Scheduler::add_node(std::shared_ptr<_Node> n) {
    nodelist.push_back(n);
    current = nodelist.end()-1;

    if (n->is_sensor()) {
		ODRModifiers++;	
    }
};

uint64_t Chordata::Node_Scheduler::calculate_target_wait() {
	static long i2c_tick = (1000*1000)/400000;
	static long sensor_lecture = i2c_tick * 8 + i2c_tick * 16 * 3; 

	first_sensor = seek_first_sensor(nodelist);
	target_wait = lap_target_duration / get_ODR_modifiers();
	const int notODRModifiers = length() - get_ODR_modifiers();
	target_wait -= BRANCH_WRITE_uS * notODRModifiers / get_ODR_modifiers();

	if (sensor_lecture > target_wait)	
		Chordata::Communicator::_warn("With this amount of sensors and ODR ({}) the sleep between lectures is bigger than the "
			"necessary: {:6d}usec > {:6d}usec.\n", odr, target_wait, sensor_lecture);

	initialized = true;
	//add_handler(&test_function);
	return target_wait;
}

void Chordata::Node_Scheduler::add_handler(void (*f)()) {
	_handlers.push_back(f);
}

void Chordata::Node_Scheduler::handlers() {
	// _handlers is a list of functions
	for (void(*f)() : _handlers) {
		f();
	}	
	get_runtime()->get_calibration_manager().handle_status();
}

uint64_t Chordata::Node_Scheduler::bang(uint64_t wait) {	

	uint64_t global_lap_micros;

	if(!initialized) {		
		wait = calculate_target_wait();		
	}
	
	do_search();

	global_lap_micros = scheduler_timek->micros();
	

	if ((*current)->first_sensor){
		global_round_micros = scheduler_timek->total_micros();
		handlers();

		if (odr_counter < 1) {
			odr_counter = odr;
			Chordata::Communicator::_trace(" ===== Ellapsed Lap Micros: {} | Hz: {:.2f} =====", global_lap_micros, (float)1000000.0/global_lap_micros);
			
			if (get_runtime()->pending_kc == 0){
				//Leave the first second of execution untouched
				//after that start cheking if the global_lap_micros has to be increased or decreased
				
				if (global_lap_micros > lap_target_duration + LAP_TOLERANCE){
					float mod_factor = std::fabs(LAP_MODIF_FACTOR * ((int64_t)global_lap_micros - lap_target_duration) / lap_target_duration);
					wait -= mod_factor;
					
					if (wait < 0) wait = 0;

					Chordata::Communicator::_trace(" (- ) Read wait reduced, now is {} (target wait = {}, [Lap measured: {}, Lap theoric: {}], factor: {})", 
						wait, target_wait,
						global_lap_micros, lap_target_duration, mod_factor);
				
				} else if (global_lap_micros < lap_target_duration - (LAP_TOLERANCE * 2)){
					float mod_factor = std::fabs(LAP_MODIF_FACTOR * ((int64_t)global_lap_micros - lap_target_duration) / lap_target_duration);
					wait += mod_factor;
					
					Chordata::Communicator::_trace(" ( +) Read wait increased, now is {} (target wait = {}, [Lap measured: {}, Lap theoric: {}], factor: {})", 
						wait, target_wait,
						global_lap_micros, lap_target_duration, mod_factor);
						
				}
			}
		}		
		odr_counter --;
		scheduler_timek->reset();
		
	} // End global_lap_micros duration cheking

	return wait;
}

void Chordata::Node_Scheduler::stop_sensors() {
	// Backwards looping for node_list
	for (auto i = nodelist.end()-1; i != nodelist.begin()-1; --i) {		
		if ((*i)->is_sensor()) {
			(*i)->stop();
		} else {
			(*i)->bang();
		}
	}
}

// ====================================================== //
// ======================== Timer ======================= //
// ====================================================== //

Chordata::_Timer::~_Timer() {
	terminate();
}

void Chordata::Timer::start_timer() {
	running = true;
	try{
		scheduler->get_timekeeper()->reset_start();
		uint64_t wait = scheduler->calculate_target_wait();
		const int notODRModifiers = scheduler->length() - scheduler->get_ODR_modifiers();

		Chordata::Communicator::_info("Starting timer. [NODES < {} , {} > = {} | ODR = {}  | Sleep = {}us]", 
			notODRModifiers, scheduler->get_ODR_modifiers(), scheduler->length(),
			Chordata::get_config()->odr(), wait);

		time_point next_bang = scheduler->get_timekeeper()->get_start();
		while(!stop) {
			next_bang += std::chrono::duration_cast<micro_duration>(micros(wait));
			wait = scheduler->bang(wait);
			std::this_thread::sleep_until(next_bang);
		}

	} catch (...){
		
		t_excp = std::current_exception();
	}
	running = false;
}

bool Chordata::Timer::start(){
	if (scheduler->empty()) {
		Chordata::Communicator::_error("Timer not started, scheduler was empty");
		return false;
	}	

	stop = false;
	t = new std::thread(&Chordata::Timer::start_timer, this);

	if (!running){
		try {
			if (t_excp) {
				std::rethrow_exception(t_excp);
			}
		} catch(const std::exception& e) {
			Chordata::Communicator::_error(e.what());
			return false;
		}
	}

	return true;
}

void Chordata::Timer::terminate(){
	if (t == nullptr || !t->joinable()) return;

	stop = true;
	scheduler->stop_sensors();
	t->join();
	delete t;
	t = nullptr;

}

void Chordata::Timer::handlers() {
	scheduler->handlers();
}