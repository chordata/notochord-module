// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#ifndef __NOTOCHORD_ERROR__
#define __NOTOCHORD_ERROR__

#include <exception>
#include <cstdint>
#include <string>
#include "core.h"
#include "Python.h"

namespace Chordata{

	struct Notochord_Error: public std::exception {
		const std::string msg;
		uint64_t running_millis;

		Notochord_Error(uint64_t time = 0):
			msg("Unknown"),
			running_millis(time)
			{
				if (time == 0){
					try{
						time = runtime_millis();
					} catch (...){}
				}
			};

		Notochord_Error(std::string _msg = "Unknown", uint64_t time = 0): 
			msg(_msg), 
			running_millis(time)
			{};

		virtual const char* what() const noexcept override{
			return msg.c_str();
		}

		uint64_t millis() const {
			return running_millis;
		}

	};

	struct IO_Error: public Notochord_Error{
		static long int count;
		
		IO_Error(std::string _msg = "Unknown", uint64_t time = 0): 
		Notochord_Error(_msg, time) {
			++ count;
		};
	};

	struct Hierarchy_Error: public Notochord_Error{
		
		Hierarchy_Error(std::string _msg = "Unknown", uint64_t time = 0): 
		Notochord_Error(_msg, time) 
		{};
	};

	struct Value_Error: public Notochord_Error{
		
		Value_Error(std::string _msg = "Unknown", uint64_t time = 0): 
		Notochord_Error(_msg, time) 
		{};
	};

	struct Idiot_Error: public Notochord_Error{
		
		Idiot_Error(std::string _msg = "", uint64_t time = 0): 
		Notochord_Error("Idiot, look carefully at your code!\n" +_msg, time) 
		{};
	};

	struct Inmutable_Error: public Notochord_Error{
		
		Inmutable_Error(std::string _msg = "", uint64_t time = 0): 
		Notochord_Error("Tried to modify an inmutable math type. Use .copy() to get a mutable copy\n" +_msg, time) 
		{};
	};

	// struct Logic_Error: public Notochord_Error {
	// 	Logic_Error(std::string _msg = "Unknown", uint64_t time = 0):
	// 	Notochord_Error(_msg, time)
	// 	{};
	// };
	

	// struct System_Error: public Notochord_Error {
	// 	int c;
	// 	System_Error(int ec, std::string _msg = "Unknown", uint64_t time = 0):
	// 	Notochord_Error(_msg, time),
	// 	c(ec)
	// 	{};

	// 	int code() const noexcept{
	// 		return c;
	// 	};

	// };

// ====================================================== //
// ==================== CYTHON ERRORS =================== //
// ====================================================== //
	namespace cy{
		//https://stackoverflow.com/questions/10684983/handling-custom-c-exceptions-in-cython
		//https://docs.python.org/3.7/c-api/exceptions.html

		extern PyObject* notochord_error;
		extern PyObject* io_error;
		extern PyObject* hierarchy_error;
		extern PyObject* idiot_error;
		extern PyObject* logic_error;
		extern PyObject* osc_error;
		extern PyObject* inmutable_error;


		void add_exceptions_to_cython();

		void raise_py_error();
	}

}



#endif
