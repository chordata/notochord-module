// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#include "chord_error.h"
#include "Python.h"
#include <string>
#include "fmt/core.h"
#include "osc/OscException.h"

PyObject* Chordata::cy::notochord_error;
PyObject* Chordata::cy::io_error;
PyObject* Chordata::cy::hierarchy_error;
PyObject* Chordata::cy::idiot_error;
PyObject* Chordata::cy::logic_error;
PyObject* Chordata::cy::osc_error;
PyObject* Chordata::cy::inmutable_error;

long int Chordata::IO_Error::count = 0;

void Chordata::cy::add_exceptions_to_cython(){
	PyObject *module_name = PyUnicode_FromString("notochord.error");
	PyObject *module = PyImport_GetModule(module_name);
    //lambda to create new exception
    auto add_new_cy_exception = [module] (const std::string& name, 
                                        PyObject *base = NULL, PyObject *dict  = NULL) {

        std::string scoped_name = fmt::format("notochord.error.{}", name);
        PyObject *dest = PyErr_NewException(scoped_name.c_str(), base, dict);

        Py_XINCREF(dest);
        if (PyModule_AddObject(module, name.c_str(), dest) < 0) {
            Py_XDECREF(dest);
            Py_CLEAR(dest);
            throw Chordata::Notochord_Error(fmt::format("cannot create python exception {}", scoped_name));
        }

        return dest;
    };

    notochord_error = add_new_cy_exception("Notochord_Error");
    io_error = add_new_cy_exception("IO_Error", notochord_error);
    hierarchy_error = add_new_cy_exception("Hierarchy_Error", notochord_error);
    idiot_error = add_new_cy_exception("Idiot_Error", notochord_error);
    logic_error = add_new_cy_exception("Logic_Error", notochord_error);
    osc_error = add_new_cy_exception("Osc_Error", notochord_error);
    inmutable_error = add_new_cy_exception("Inmutable_Error", notochord_error);

}


void Chordata::cy::raise_py_error(){
    try {
        throw;
    //Notochord specific errors
    } catch (Chordata::IO_Error& e) {
        PyErr_SetString(Chordata::cy::io_error, e.what());
    } catch (Chordata::Hierarchy_Error& e) {
        PyErr_SetString(Chordata::cy::hierarchy_error, e.what());
    } catch (Chordata::Idiot_Error& e) {
        PyErr_SetString(Chordata::cy::idiot_error, e.what());
    } catch (std::logic_error& e) {
        PyErr_SetString(Chordata::cy::logic_error, e.what());
    } catch (osc::Exception& e) {
        PyErr_SetString(Chordata::cy::osc_error, e.what());
    
    //Python errors
    } catch (Chordata::Value_Error& e) {
        PyErr_SetString(PyExc_ValueError, e.what());
    } catch (std::runtime_error& e) {
        PyErr_SetString(PyExc_RuntimeError, e.what());

    //Other errors
    } catch (const std::exception& e) {
        PyErr_SetString(Chordata::cy::notochord_error, e.what() );
    } 
}