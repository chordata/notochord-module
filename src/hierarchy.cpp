// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#include <vector>
#include <memory>

#include "hierarchy.h"

#include <iostream>

using Chordata::link_ptr;
using Chordata::link_wptr;
using Chordata::Link_Info;


Link_Info Chordata::_Hierarchy::get_link_info(link_wptr the_wlink){
    long shared_count = the_wlink.use_count();
    
    if (link_ptr the_link = the_wlink.lock()){
        Link_Info info;

        std::vector<std::string> children_labels;
        std::vector<link_wptr> children;
        
        for (size_t i = 0; i < the_link->get_children_n(); i++)
            {
                children_labels.push_back(the_link->get_child(i)->get_label());
                children.push_back(std::move(the_link->get_weak_child(i)));
            }
        
        std::string parent_label("");
        link_wptr wparent = the_link->get_parent();
        if (link_ptr parent = wparent.lock()){
                parent_label = parent->get_label();
        } 
        
        info = {
            the_link->get_label(),
            parent_label,
            std::move(the_link->get_parent()),
            children, //TODO this is dangling 
            children_labels, //TODO this is dangling
            shared_count
        };

        return info;

    }

    throw Chordata::Hierarchy_Error("Trying to get info of expired Link pointer");

}

void Chordata::_Hierarchy::build_link_index(link_wptr the_wlink, Hierarchy *h){
    Link_Info info = get_link_info(the_wlink);
    h->index[info.label] = std::move(info);
}
