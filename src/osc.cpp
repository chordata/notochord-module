// -- START OF COPYRIGHT & LICENSE NOTICES --
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can
// redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES --

#include "osc.h"
#include "communicator.h"

#include "spdlog/spdlog.h"
#include "spdlog/common.h"
#include "spdlog/sinks/base_sink.h"
#include "osc/OscTypes.h"

#include "timer.h"

#include <mutex>

/*----------  OSC Base  ----------*/
void Chordata::Communicator::OscOutBase::append_message(const spdlog::details::log_msg& msg){
	//msg.payload is fmt::basic_string_view
	//see: https://fmt.dev/dev/api.html#_CPPv2N3fmt17basic_string_viewE
	bool is_info = msg.level < spdlog::level::warn;
	if(is_info) {
		if (i_log_pkt == log_packets.end()){
			log_packets.emplace_back();
			i_log_pkt = --log_packets.end();
		}

	}
	else {
		if(e_log_pkt == log_packets.end()) {
			error_packets.emplace_back();
			e_log_pkt = --error_packets.end();
		}
	}

	osc::OutboundPacketStream& pkt = (is_info) ? i_log_pkt->p: e_log_pkt->p;

	pkt << osc::BeginMessage(
			(msg.level < spdlog::level::warn)?
				msg_address.c_str() : error_address.c_str()
		) << std::string(msg.payload.data(), msg.payload.size()).c_str() << osc::EndMessage;

	if(is_info)	i_log_pkt++;
	else e_log_pkt++;
}


void Chordata::Communicator::OscOutBase::sink_it_(const spdlog::details::log_msg& msg){
	append_message(msg);
}

template<bool use_bundles>
template <typename Packet_list, typename  Iter>
void Chordata::Communicator::OscOutUdp<use_bundles>
::flush_packets(Packet_list& packets, Iter& iter){
	//Template method to send a list of messages and clear it
	for (auto pkt = packets.begin(); pkt != iter; ++pkt){
		transmitSocket.Send( pkt->p.Data(), pkt->p.Size() );
		pkt->p.Clear();
	}

	iter = packets.begin();
}


/*----------  Template specialisation MSG----------*/

#include <exception>
#include <iostream>
#include "fmt/core.h"


std::string Chordata::Communicator::OscOutBase::get_current_packet_hex(){
	fmt::memory_buffer hex_repr;
	fmt::memory_buffer out;
	fmt::format_to(out, "{:03d}: ", 0);

	for (int i = 0; i<pos_bundle.p.Size(); i++ ){
		 char current = (char) *(pos_bundle.p.Data()+i);

		if (i && i%(4*4)==0) {
			fmt::format_to(out, "    {}\n", hex_repr.data());
			hex_repr.clear();
			fmt::format_to(out, "{:03d}: ", i);
		} else if (i && i%4==0) {
			fmt::format_to(hex_repr, " ");
			fmt::format_to(out, " ");
		}

		fmt::format_to(hex_repr, "{:02x}", (unsigned char) current);

		if (current > 127 || current <= 0x20 ) fmt::format_to(out, ".");
		else if (current) fmt::format_to(out, "{}", current);
		if (i == pos_bundle.p.Size() - 1) {
			fmt::format_to(out, "    {}", hex_repr.data());
		}

	}
	return fmt::to_string(out);
}

void Chordata::Communicator::OscOutBase
::append_message(const std::string& subtarget, const Chordata::Quaternion& q){
	//msg.payload is fmt::basic_string_view
	//see: https://fmt.dev/dev/api.html#_CPPv2N3fmt17basic_string_viewE
	{
		std::lock_guard<std::mutex> lock(_p_mutex);
		if (i_q_pkt == q_packets.end()){
			q_packets.emplace_back();
			i_q_pkt = --q_packets.end();
		}
	}

	osc::OutboundPacketStream& pkt = i_q_pkt->p;

	pkt << osc::BeginMessage(subtarget.c_str())
	<< q.w() << q.x() << q.y() << q.z() << osc::EndMessage;

	i_q_pkt++;
}

void Chordata::Communicator::OscOutBase
::append_message(const std::string& subtarget, const Chordata::Vector3& v){
	//msg.payload is fmt::basic_string_view
	//see: https://fmt.dev/dev/api.html#_CPPv2N3fmt17basic_string_viewE
	{
		std::lock_guard<std::mutex> lock(_p_mutex);
		if (i_raw_pkt == raw_packets.end()){

			raw_packets.emplace_back();
			i_raw_pkt = --raw_packets.end();
		}
	}

	osc::OutboundPacketStream& pkt = i_raw_pkt->p;

	pkt << osc::BeginMessage(subtarget.c_str())
	<< v.x() << v.y() << v.z() << osc::EndMessage;

	i_raw_pkt++;
}

void Chordata::Communicator::OscOutBase
::append_message(const std::string& subtarget, const _KCeptor_Base& kc){
	//msg.payload is fmt::basic_string_view
	//see: https://fmt.dev/dev/api.html#_CPPv2N3fmt17basic_string_viewE
	{
		std::lock_guard<std::mutex> lock(_p_mutex);
		if (i_raw_pkt == raw_packets.end()){
			raw_packets.emplace_back();
			i_raw_pkt = --raw_packets.end();
		}
	}
	osc::OutboundPacketStream& pkt = i_raw_pkt->p;

	pkt << osc::BeginMessage(subtarget.c_str())
	<< kc.get_g_raw_read(sensor_axis::X_AXIS) << kc.get_g_raw_read(sensor_axis::Y_AXIS) << kc.get_g_raw_read(sensor_axis::Z_AXIS)
	<< kc.get_a_raw_read(sensor_axis::X_AXIS) << kc.get_a_raw_read(sensor_axis::Y_AXIS) << kc.get_a_raw_read(sensor_axis::Z_AXIS)
	<< kc.get_m_raw_read(sensor_axis::X_AXIS) << kc.get_m_raw_read(sensor_axis::Y_AXIS) << kc.get_m_raw_read(sensor_axis::Z_AXIS)
	<< osc::EndMessage;

	i_raw_pkt++;

}


void Chordata::Communicator::OscOutBase
::append_message_bundle(const std::string& subtarget, const Chordata::Quaternion& q){
	quat_bundle.p << osc::BeginMessage(subtarget.c_str())
	<< q.w() << q.x() << q.y() << q.z() << osc::EndMessage;
}

void Chordata::Communicator::OscOutBase
::append_message_bundle(const std::string& subtarget, const Chordata::Vector3& v){
	pos_bundle.p << osc::BeginMessage(subtarget.c_str())
	<< v.x() << v.y() << v.z() << osc::EndMessage;
}

void Chordata::Communicator::OscOutBase
::append_message_bundle(const std::string& subtarget, const _KCeptor_Base& kc){
	raw_bundle.p << osc::BeginMessage(subtarget.c_str())
	<< kc.get_g_raw_read(sensor_axis::X_AXIS) << kc.get_g_raw_read(sensor_axis::Y_AXIS) << kc.get_g_raw_read(sensor_axis::Z_AXIS)
	<< kc.get_a_raw_read(sensor_axis::X_AXIS) << kc.get_a_raw_read(sensor_axis::Y_AXIS) << kc.get_a_raw_read(sensor_axis::Z_AXIS)
	<< kc.get_m_raw_read(sensor_axis::X_AXIS) << kc.get_m_raw_read(sensor_axis::Y_AXIS) << kc.get_m_raw_read(sensor_axis::Z_AXIS)
	<< osc::EndMessage;
}

void Chordata::Communicator::OscOutBase
::append_to_bundle(const std::string& subtarget, const Chordata::Quaternion& q){
	static std::string q_bundle_header = "/%%" _CHORDATA_DEF_OSC_ADDR "/q";
	std::lock_guard<std::mutex> lock(_p_mutex);

	bool new_bundle = quat_bundle.p.Size();
	if (!new_bundle){
		quat_bundle.p << osc::BeginBundle(Chordata::global_round_micros);
		quat_bundle.p << osc::BeginMessage( q_bundle_header.c_str()  )
		<< osc::OscNil << osc::EndMessage;
	}

	append_message_bundle(subtarget, q);
}

void Chordata::Communicator::OscOutBase
::append_to_bundle(const std::string& subtarget, const Chordata::Vector3& v){
	static std::string pos_bundle_header = "/%%" _CHORDATA_DEF_OSC_ADDR "/pos";
	std::lock_guard<std::mutex> lock(_p_mutex);
	bool new_bundle = pos_bundle.p.Size();
	if (!new_bundle){
		pos_bundle.p << osc::BeginBundle(Chordata::global_round_micros);
		pos_bundle.p << osc::BeginMessage( pos_bundle_header.c_str()  )
		<< osc::OscNil << osc::EndMessage;
	}

	append_message_bundle(subtarget, v);
}

void Chordata::Communicator::OscOutBase
::append_to_bundle(const std::string& subtarget, const _KCeptor_Base& kc){
	static std::string raw_bundle_header = "/%%" _CHORDATA_DEF_OSC_ADDR "/raw";
	std::lock_guard<std::mutex> lock(_p_mutex);
	bool new_bundle = raw_bundle.p.Size();
	if (!new_bundle){
		raw_bundle.p << osc::BeginBundle(Chordata::global_round_micros);
		raw_bundle.p << osc::BeginMessage( raw_bundle_header.c_str()  )
		<< osc::OscNil << osc::EndMessage;
	}

	append_message_bundle(subtarget, kc);
}


template<>
void Chordata::Communicator::OscOutUdp<false>
::thread_handler() {
	is_running = false;
}

template <>
void Chordata::Communicator::OscOutUdp<false>
::transmit(const std::string& subtarget, const Chordata::Quaternion& q){
	append_message(subtarget, q);
}

template <>
void Chordata::Communicator::OscOutUdp<false>
::transmit(const std::string& subtarget, const Chordata::Vector3& v){
	append_message(subtarget, v);
}

template <>
void Chordata::Communicator::OscOutUdp<false>
::transmit(const _Node& n, const Chordata::Quaternion& q){
	append_message(n.get_osc_q_header(), q);
}

template <>
void Chordata::Communicator::OscOutUdp<false>
::transmit(const _Node& n, const Chordata::Vector3& v){
	append_message(n.get_osc_r_header(), v);
}

template <>
void Chordata::Communicator::OscOutUdp<false>
::transmit(const _KCeptor_Base& kc){
	append_message(kc.get_osc_r_header(), kc);
}

template <>
void Chordata::Communicator::OscOutUdp<false>
::transmit(const _Armature& arm){
	for (auto& it: arm.get_bones()) {
		auto& b = it.second;
		append_message(b->get_osc_q_header(), b->get_global_rotation());
	}
}

template <>
void Chordata::Communicator::OscOutUdp<false>
::flush_(){
	flush_packets(log_packets, i_log_pkt);
	flush_packets(error_packets, e_log_pkt);
	flush_packets(q_packets, i_q_pkt);
	flush_packets(raw_packets, i_raw_pkt);
}

/*----------  Template specialization BUNDLES  ----------*/


template <>
void Chordata::Communicator::OscOutUdp<true>
::transmit(const std::string& subtarget, const Chordata::Quaternion& q){
	append_to_bundle(subtarget, q);
}

template <>
void Chordata::Communicator::OscOutUdp<true>
::transmit(const std::string& subtarget, const Chordata::Vector3& v){
	append_to_bundle(subtarget, v);
}

template <>
void Chordata::Communicator::OscOutUdp<true>
::transmit(const _Node& n, const Chordata::Quaternion& q){
	append_to_bundle(n.get_bundle_header(), q);
}

template <>
void Chordata::Communicator::OscOutUdp<true>
::transmit(const _Node& n, const Chordata::Vector3& v){
	append_to_bundle(n.get_bundle_header(), v);
}

template <>
void Chordata::Communicator::OscOutUdp<true>
::transmit(const _KCeptor_Base& kc){
	append_to_bundle(kc.get_bundle_header(), kc);
}

template <>
void Chordata::Communicator::OscOutUdp<true>
::transmit(const _Armature& arm){
	for (auto& it: arm.get_bones()) {
		auto& b = it.second;
		append_to_bundle(b->get_bundle_header(), b->get_global_rotation());
	}
}

template <>
void Chordata::Communicator::OscOutUdp<true>
::thread_handler(){
	keep_running = true;
	std::unique_lock<std::mutex> lock(_p_mutex);
	while (keep_running){
		c.wait_for(lock, std::chrono::milliseconds(_CHORDATA_THREAD_DELAY_MS));
		if (flag_set){
			// Send_packets();
			if (temp_quat_bundle.p.IsBundleInProgress()) {
				temp_quat_bundle.p << osc::EndBundle;
			}

			if (temp_raw_bundle.p.IsBundleInProgress()) {
				temp_raw_bundle.p << osc::EndBundle;
			}

			if (pos_bundle.p.IsBundleInProgress()) {
				pos_bundle.p << osc::EndBundle;
			}

			if (temp_quat_bundle.p.Size()){
				transmitSocket.Send( 	temp_quat_bundle.p.Data(),
										temp_quat_bundle.p.Size() );
				temp_quat_bundle.p.Clear();
			}

			if (temp_raw_bundle.p.Size()){
				transmitSocket.Send( 	temp_raw_bundle.p.Data(),
										temp_raw_bundle.p.Size() );
				temp_raw_bundle.p.Clear();
			}

			if (pos_bundle.p.Size()){
				transmitSocket.Send( 	pos_bundle.p.Data(),
										pos_bundle.p.Size() );
				pos_bundle.p.Clear();
			}
			flag_set = false;
		}
	}
	is_running = false;
}

template <>
void Chordata::Communicator::OscOutUdp<true>
::flush_(){

	flush_packets(log_packets, i_log_pkt);
	flush_packets(error_packets, e_log_pkt);

	{
		std::lock_guard<std::mutex> lock(_p_mutex);
		if (quat_bundle.p.Size() != 0){
			temp_quat_bundle.p.Swap(quat_bundle.p);
			quat_bundle.p.Clear();
		}
		
		if (raw_bundle.p.Size() != 0){
			temp_raw_bundle.p.Swap(raw_bundle.p);
			raw_bundle.p.Clear();
		}

		flag_set = true;
	}
	c.notify_one();	

}

// WEBSOCKET
template<bool use_bundles>
template <typename Packet_list, typename  Iter>
void Chordata::Communicator::OscOutWebsocket<use_bundles>
::flush_packets(Packet_list& packets, Iter& iter, const std::string& p){
	for (auto pkt = packets.begin(); pkt != iter; ++pkt){
		socket.send( pkt->p.Data(), pkt->p.Size(), p );
		pkt->p.Clear();
	}

	iter = packets.begin();
}

template<>
void Chordata::Communicator::OscOutWebsocket<false>
::flush_() {
	flush_packets(log_packets, i_log_pkt, msg_address);
	flush_packets(error_packets, e_log_pkt, error_address);
	flush_packets(q_packets, i_q_pkt, base_address);
	flush_packets(raw_packets, i_raw_pkt, base_address);
};

template<>
void Chordata::Communicator::OscOutWebsocket<false>
::transmit(const std::string& subtarget, const Quaternion& q) {
	append_message(subtarget, q);
}

template<>
void Chordata::Communicator::OscOutWebsocket<false>
::transmit(const std::string& subtarget, const Vector3& v) {
	append_message(subtarget, v);
}

template<>
void Chordata::Communicator::OscOutWebsocket<false>
::transmit(const _Node& n, const Quaternion& q) {
	append_message(n.get_osc_q_header(), q);
}

template<>
void Chordata::Communicator::OscOutWebsocket<false>
::transmit(const _Node& n, const Vector3& v) {
	append_message(n.get_osc_q_header(), v);
}

template<>
void Chordata::Communicator::OscOutWebsocket<false>
::transmit(const _KCeptor_Base& kc) {// For RAW transmit
	append_message(kc.get_osc_q_header(), kc);
}

template<>
void Chordata::Communicator::OscOutWebsocket<false>
::transmit(const _Armature& arm) {
	for (auto& it: arm.get_bones()) {
		auto& b = it.second;
        append_message(b->get_osc_q_header(), b->get_global_rotation());
    }
}

// WEBSOCKET bundle
template<>
void Chordata::Communicator::OscOutWebsocket<false>
::thread_handler() {
	is_running = false;
}

template<>
void Chordata::Communicator::OscOutWebsocket<true>
::thread_handler() { 
	keep_running = true;
	std::unique_lock<std::mutex> lock(_p_mutex);
	while (keep_running){
		c.wait_for(lock, std::chrono::milliseconds(_CHORDATA_THREAD_DELAY_MS));
		if (flag_set){
			// Send_packets();
			if (temp_quat_bundle.p.IsBundleInProgress()) {
				temp_quat_bundle.p << osc::EndBundle;
			}

			if (temp_raw_bundle.p.IsBundleInProgress()) {
				temp_raw_bundle.p << osc::EndBundle;
			}

			if (pos_bundle.p.IsBundleInProgress()) {
				pos_bundle.p << osc::EndBundle;
			}

			if (temp_quat_bundle.p.Size()){
				socket.send( 	temp_quat_bundle.p.Data(),
										temp_quat_bundle.p.Size() );
				temp_quat_bundle.p.Clear();
			}

			if (temp_raw_bundle.p.Size()){
				socket.send( 	temp_raw_bundle.p.Data(),
										temp_raw_bundle.p.Size() );
				temp_raw_bundle.p.Clear();
			}

			if (pos_bundle.p.Size()){
				socket.send( 	pos_bundle.p.Data(),
										pos_bundle.p.Size() );
				pos_bundle.p.Clear();
			}
			flag_set = false;
		}
	}
	is_running = false;
}

template<>
void Chordata::Communicator::OscOutWebsocket<true>
::flush_() {
	flush_packets(log_packets, i_log_pkt, msg_address);
	flush_packets(error_packets, e_log_pkt, error_address);

	{
		std::lock_guard<std::mutex> lock(_p_mutex);
		if (quat_bundle.p.Size() != 0){
			temp_quat_bundle.p.Swap(quat_bundle.p);
			quat_bundle.p.Clear();
		}
		
		if (raw_bundle.p.Size() != 0){
			temp_raw_bundle.p.Swap(raw_bundle.p);
			raw_bundle.p.Clear();
		}

		

		flag_set = true;
	}
	c.notify_all();	
	
};

template<>
void Chordata::Communicator::OscOutWebsocket<true>
::transmit(const std::string& subtarget, const Quaternion& q) {
	append_to_bundle(subtarget, q);
}

template<>
void Chordata::Communicator::OscOutWebsocket<true>
::transmit(const std::string& subtarget, const Vector3& v) {
	append_to_bundle(subtarget, v);
}

template<>
void Chordata::Communicator::OscOutWebsocket<true>
::transmit(const _Node& n, const Quaternion& q) {
	append_to_bundle(n.get_bundle_header(), q);
}

template<>
void Chordata::Communicator::OscOutWebsocket<true>
::transmit(const _Node& n, const Vector3& v) {
	append_to_bundle(n.get_bundle_header(), v);
}

template<>
void Chordata::Communicator::OscOutWebsocket<true>
::transmit(const _KCeptor_Base& kc) {// For RAW transmit
	append_to_bundle(kc.get_bundle_header(), kc);
}

template<>
void Chordata::Communicator::OscOutWebsocket<true>
::transmit(const _Armature& arm) {
    for (auto& it: arm.get_bones()) {
		auto& b = it.second;
        append_to_bundle(b->get_bundle_header(), b->get_global_rotation());
    }
}

