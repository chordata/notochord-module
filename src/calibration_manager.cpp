#include "calibration_manager.h"
#include "phy_nodes.h"

Chordata::_Sensor_Payload::_Sensor_Payload(uint64_t timestamp, std::string label, Quaternion q,
                                           Vector3i gyro, Vector3i accel, Vector3i mag){
    this->timestamp = timestamp;
    this->label = label;
    this->q = q;
    this->gyro = gyro;
    this->accel = accel;
    this->mag = mag;
}

Chordata::Calibration_Manager::Calibration_Manager(){
    status = Calibration_Status::CALIB_IDLE;
    new_status = Calibration_Status::CALIB_IDLE;

    indexes["vert_i"] = 0;
    indexes["vert_s"] = 0;
    indexes["func_arms_i"] = 0;
    indexes["func_arms_s"] = 0;
    indexes["func_trunk_i"] = 0;
    indexes["func_trunk_s"] = 0;
    indexes["func_legs_l_i"] = 0;
    indexes["func_legs_l_s"] = 0;
    indexes["func_legs_r_i"] = 0;
    indexes["func_legs_r_s"] = 0;

    status_changed = false;
    cycle = 0;
}

Chordata::Calibration_Manager::~Calibration_Manager(){}

void Chordata::Calibration_Manager::set_i_index(Calibration_Status status, int index){
    switch (status){
        case Calibration_Status::STATIC:
            indexes["vert_i"] = index;
            break;
        case Calibration_Status::ARMS:
            indexes["func_arms_i"] = index;
            break;
        case Calibration_Status::TRUNK:
            indexes["func_trunk_i"] = index;
            break;
        case Calibration_Status::LEFT_LEG:
            indexes["func_legs_l_i"] = index;
            break;
        case Calibration_Status::RIGHT_LEG:
            indexes["func_legs_r_i"] = index;
            break;
        default:
            break;
    }
}

void Chordata::Calibration_Manager::set_s_index(Calibration_Status status, int index){
    switch (status){
        case Calibration_Status::STATIC:
            indexes["vert_s"] = index;
            break;
        case Calibration_Status::ARMS:
            indexes["func_arms_s"] = index;
            break;
        case Calibration_Status::TRUNK:
            indexes["func_trunk_s"] = index;
            break;
        case Calibration_Status::LEFT_LEG:
            indexes["func_legs_l_s"] = index;
            break;
        case Calibration_Status::RIGHT_LEG:
            indexes["func_legs_r_s"] = index;
            break;
        default:
            break;
    }
}

void Chordata::Calibration_Manager::set_status(int i_status){
    new_status = static_cast<Calibration_Status>(i_status);     
    status_changed = true;  
}

void Chordata::Calibration_Manager::handle_status() {
    if (status_changed) {
        if (new_status != status) {
            set_i_index(new_status, cycle);
            set_s_index(status, cycle - 1);
        } else {
            set_i_index(status, cycle);
        }
        status = new_status;
        status_changed = false;
    }
   
    if (status != CALIB_IDLE) cycle++;
}

void Chordata::Calibration_Manager::add_payload(uint64_t timestamp, _KCeptor_Base* node, Quaternion q){
    if (status == CALIB_IDLE) return;
    /// TODO: We should be able to use timestamp parameter instead of global_round_micros, test this
    Sensor_Payload payload(Chordata::global_round_micros, node->get_label(), q, 
                           Vector3i(node->get_g_raw_read(sensor_axis::X_AXIS), node->get_g_raw_read(sensor_axis::Y_AXIS), node->get_g_raw_read(sensor_axis::Z_AXIS)), 
                           Vector3i(node->get_a_raw_read(sensor_axis::X_AXIS), node->get_a_raw_read(sensor_axis::Y_AXIS), node->get_a_raw_read(sensor_axis::Z_AXIS)),
                           Vector3i(node->get_m_raw_read(sensor_axis::X_AXIS), node->get_m_raw_read(sensor_axis::Y_AXIS), node->get_m_raw_read(sensor_axis::Z_AXIS)));

    buffer.push_back(std::move(payload));
}

const std::vector<Chordata::_Sensor_Payload>& Chordata::Calibration_Manager::get_buffer(){
    return buffer;
}

const std::unordered_map<std::string, uint64_t>& Chordata::Calibration_Manager::get_indexes(){
    return indexes;
}

void Chordata::Calibration_Manager::clear_buffer(){
    buffer.clear();
    indexes["vert_i"] = 0;
    indexes["vert_s"] = 0;
    indexes["func_arms_i"] = 0;
    indexes["func_arms_s"] = 0;
    indexes["func_trunk_i"] = 0;
    indexes["func_trunk_s"] = 0;
    indexes["func_legs_l_i"] = 0;
    indexes["func_legs_l_s"] = 0;
    indexes["func_legs_r_i"] = 0;
    indexes["func_legs_r_s"] = 0;

    cycle = 0;
}