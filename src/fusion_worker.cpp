// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#define DEBUG 0

#include "fusion_worker.h"
#include "communicator.h"
#include "phy_nodes.h"
#include "armature.h"

namespace comm = Chordata::Communicator;

void Chordata::Fusion_Task::run() {
	node->get_kalman()->set_gyro(gx, gy, gz);
	node->get_kalman()->set_accel(ax, ay, az);
	node->get_kalman()->set_mag(mx, my, mz);

	bool filter_has_run = node->get_kalman()->process_and_run();
	if (!filter_has_run){
		return;
	}

	auto the_quat = node->get_kalman()->get_quat();

	Quaternion q(the_quat->q0, the_quat->q1,
			the_quat->q2, the_quat->q3);

	if (Chordata::get_config()->use_armature) {
		Armature_Task *at = new Armature_Task(node, time, q);
		auto t = std::unique_ptr<Armature_Task>(at);

		get_runtime()->get_armature_worker()->enqueue(std::move(t));

	} else {
		comm::_transmit(*node, q);

		if (Chordata::get_config()->raw){
			comm::_transmit(*node);
		}	

		if (node->first_sensor) {
			Chordata::Communicator::flush_loggers();
		}
	}

	Chordata::get_runtime()->get_calibration_manager().add_payload(time, node, std::move(q));

}

void Chordata::Armature_Task::run() {
	Chordata::get_runtime()->get_armature()->process_bone(*node, q);

	if (node->first_sensor) {
		Chordata::get_runtime()->get_armature()->update();
		
		comm::_transmit(Chordata::get_runtime()->get_armature().get()); 

		Chordata::Communicator::flush_loggers();
	}
}

void Chordata::Notochord_Worker::enqueue(Task&& t){
	{
		std::lock_guard<std::mutex> lock(m);
		q.push(std::move(t));
	}
	c.notify_all();
}

void Chordata::Notochord_Worker::consume(){
	Task t;
	{
	    std::unique_lock<std::mutex> lock(m);
	  	while(q.empty()){
			#if DEBUG
				std::cout << "-------- Waiting " << std::this_thread::get_id() << std::endl;
			#endif
	  	  c.wait_for(lock, std::chrono::milliseconds(_CHORDATA_THREAD_DELAY_MS));
	  	  if (q.empty() || !keep_running){

			return;
		  } 
	  	}
		t = std::move(q.front());
	    q.pop();
	}
	t->run();
}

void Chordata::Notochord_Worker::handler(){
	keep_running = true;
	while(keep_running){
		consume();
	}
	is_running = false;
	#if DEBUG
		std::cout << "-------- Handler done " << is_running << std::endl;
	#endif

}

void Chordata::Notochord_Worker::stop(){
	int wait_attempts = 1;
	while (is_running){
		keep_running = false;
		CHORDATA_SLEEP(_CHORDATA_THREAD_DELAY_MS * (wait_attempts++));
		#if DEBUG
			std::cout << "-------- Notify from " << wait_attempts << " " << std::this_thread::get_id() << std::endl;
		#endif
		
		c.notify_all();
	}
}

