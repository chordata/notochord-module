#include "armature.h"
#include <math.h>
#include "communicator.h"

Chordata::_Bone::_Bone(bone_ptr _parent, std::string _label, matrix4_ptr _local_transform, matrix4_ptr _global_transform, std::size_t max):
		        Link(_parent, _label, max)
{
    local_transform = _local_transform;
    global_transform = _global_transform;
    pre = Quaternion::Identity();
    post = Quaternion::Identity();

    auto_update = false; 
    dirty = false;

    if (_parent) {
        parent = _parent;
        //parent->add_child(this->shared_from_this());
    }
}

Chordata::_Bone::~_Bone() {
    this->children.clear();
}

// ==================== Local Space ==================== //

void Chordata::_Bone::set_local_transform(Matrix4 &_local_transform) {
    *local_transform = _local_transform;

    // If auto_update is enabled, update the global transform
    if (auto_update) {
        update_global_transform();
        update_children();
    }
}

void Chordata::_Bone::update_local_transform() {
    if (parent) {
        *local_transform = parent->get_global_transform().inverse() * (*global_transform);
    } else {
        *local_transform = *global_transform;
    }
}

// Translation
void Chordata::_Bone::set_local_translation(Vector3 &_local_translation) {
    local_transform->block<3,1>(0,3) = _local_translation;

    // If auto_update is enabled, update the global transform
    if (auto_update) {
        update_global_transform();
        update_children();
    }
}

void Chordata::_Bone::translate_local(Vector3 &_translation) {
    local_transform->block<3,1>(0,3) += _translation;

    update_global_transform();
    update_children();
}

// Rotation
void Chordata::_Bone::set_local_rotation(Quaternion &_local_rotation) {
    local_transform->block<3,3>(0,0) = _local_rotation.toRotationMatrix();

    // If auto_update is enabled, update the global transform
    if (auto_update) {
        update_global_transform();
        update_children();
    }
}

void Chordata::_Bone::rotate_local(Quaternion &_rotation) {
    local_transform->block<3,3>(0,0) = _rotation.toRotationMatrix() * local_transform->block<3,3>(0,0);

    // If auto_update is enabled, update the global transform
    if (auto_update) {
        update_global_transform();
        update_children();
    }
}


// ==================== Global Space ==================== //

void Chordata::_Bone::set_global_transform(Matrix4 &_global_transform) {
    *global_transform = _global_transform;
    dirty = true;

    // If auto_update is enabled, update the local transform
    if (auto_update) { 
        update_local_transform();
        update_children();
    }
}

void Chordata::_Bone::update_global_transform() {
    if (parent) {
        *global_transform = parent->get_global_transform() * (*local_transform);
    } else {
        *global_transform = *local_transform;
    }
}

// Translation
void Chordata::_Bone::set_global_translation(Vector3 &_global_translation) {
    global_transform->block<3,1>(0,3) = _global_translation;
    dirty = true;

    // If auto_update is enabled, update the local transform
    if (auto_update) { 
        update_local_transform();
        update_children();
    }
}

void Chordata::_Bone::translate_global(Vector3 &_translation) {
    global_transform->block<3,1>(0,3) += _translation;
    dirty = true;

    // If auto_update is enabled, update the local transform
    if (auto_update) { 
        update_local_transform();
        update_children();
    }
}

// Rotation
void Chordata::_Bone::set_global_rotation(Quaternion &_global_rotation, bool use_calibration) {
    if (use_calibration) {
        _global_rotation = pre * _global_rotation * post;
    }

    global_transform->block<3,3>(0,0) = _global_rotation.toRotationMatrix();
    dirty = true;

    // If auto_update is enabled, update the local transform
    if (auto_update) { 
        update_local_transform();
        update_children();
    }
}

void Chordata::_Bone::rotate_global(Quaternion &_rotation) {
    global_transform->block<3,3>(0,0) = _rotation.toRotationMatrix() * global_transform->block<3,3>(0,0);
    dirty = true;

    // If auto_update is enabled, update the local transform
    if (auto_update) { 
        update_local_transform();
        update_children();
    }
}

// ==================== Getters ==================== //

Chordata::Matrix4 Chordata::_Bone::get_local_transform() {
    return *local_transform;
}

Chordata::Matrix4 Chordata::_Bone::get_global_transform() {
    return *global_transform;
}

// Translation
Chordata::Vector3 Chordata::_Bone::get_local_translation() {
    return Vector3((*local_transform)(0, 3), (*local_transform)(1, 3), (*local_transform)(2, 3));
}

Chordata::Vector3 Chordata::_Bone::get_global_translation() {
    return Vector3((*global_transform)(0, 3), (*global_transform)(1, 3), (*global_transform)(2, 3));
}

// Rotation
Chordata::Quaternion Chordata::_Bone::get_local_rotation() {
    return Quaternion(local_transform->block<3,3>(0,0));
}

Chordata::Quaternion Chordata::_Bone::get_global_rotation() {
    return Quaternion(global_transform->block<3,3>(0,0));
}

// ==================== Setters ==================== //
void Chordata::_Bone::set_auto_update(bool _auto_update) {
    auto_update = _auto_update;
}

void Chordata::_Bone::set_pre_quaternion(Quaternion &_pre) { pre = _pre; };

void Chordata::_Bone::set_post_quaternion(Quaternion &_post) { post = _post; };
// ==================== Children ==================== //

void Chordata::_Bone::add_child(bone_ptr child) {
    this->children.push_back(child);
}

void Chordata::_Bone::remove_child(bone_ptr _child) {
    for (std::size_t i = 0; i < this->children.size(); i++) {
        if (this->children[i] == _child) {
            this->children.erase(this->children.begin() + i);
            break;
        }
    }
}

void Chordata::_Bone::update_children(bool update_local) {
    for (auto child : this->children) {
        if (update_local) {
            child->update_local_transform();
        } else {
            child->update_global_transform();
        }
        child->update_children(update_local);
    }
}

void Chordata::_Bone::update() {
    if (dirty) {
        update_local_transform();
        dirty = false;
    } else {
        update_global_transform();
    }
    for (auto child : this->children) {
        child->update();
    }
}


// ====================================================== //
// ======================== Armature ==================== //
// ====================================================== //

Chordata::_Armature::_Armature(std::string _label, bone_ptr _root):
            Hierarchy(_root),
            label(_label),
            root(_root)
{
    build_bone_dict();
}

Chordata::_Armature::~_Armature() {}

// ==================== Build Bone Dictionary ==================== //
void Chordata::_Armature::build_bone_dict_helper(bone_dict &dict, bone_ptr bone) { 
    dict[bone->get_label()] = bone;
    for (auto child : bone->get_children()) {
        build_bone_dict_helper(dict, child);
    }
}

Chordata::bone_dict Chordata::_Armature::build_bone_dict() {
    // std::unordered_map<std::string, std::shared_ptr<_Bone>> bone_dict;
    build_bone_dict_helper(bones, root);
    return bones;
}

std::string Chordata::_Armature::get_label() {
    return label;
}

Chordata::bone_ptr Chordata::_Armature::get_root() {
    return root;
}
namespace comm = Chordata::Communicator;
// ==================== Update ==================== //
void Chordata::_Armature::update(bone_ptr _bone) {
    if (!_bone) {
        _bone = root;
    }
    // Recursively update all bones with this method
    if (_bone->dirty) {
            _bone->update_local_transform();
            _bone->dirty = false;            
    } else {
        _bone->update_global_transform();
    }

    // comm::transmit(_bone->get_osc_q_header(), _bone->get_global_rotation());
    
    for (auto child : _bone->get_children()) {    
        update(child);
    }
}

void Chordata::_Armature::process_bone(const _Node &n, Quaternion &q) {
    bone_ptr bone = bones[n.get_label()];
    bone->set_global_rotation(q);
}
