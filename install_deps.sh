#!/bin/bash

set -eu

_PYTHON=$1

function exists_in_list() {
    LIST=$1
    DELIMITER=$2
    VALUE=$3
    LIST_WHITESPACES=`echo $LIST | tr "$DELIMITER" " "`
    for x in $LIST_WHITESPACES; do
        if [ "$x" = "$VALUE" ]; then
            return 0
        fi
    done
    return 1
}

if which pacman 1>/dev/null 2>&1 ; then
    # Install with pacman
    sudo pacman -S --noconfirm --needed git libxml2 libxslt gcc gdb

    if ! command -v python;then
        AURPY=$(echo $_PYTHON | sed -e "s/\.//")
        AUR_AVAILABLE_PY="python36 python37 python38 python39 python311"

        if exists_in_list $AUR_AVAILABLE_PY " " $AURPY; then
            yay -S $AURPY
        else
            echo "The required version of python ($_PYTHON) is not installed or available as an AUR package."
            echo "Aborting installation"
            exit 1
        fi
    fi

elif which apt-get 1>/dev/null 2>&1 ; then
    # Install with apt-get    
    sudo apt-get update
    sudo apt-get install -y $_PYTHON $_PYTHON-dev python3-venv git libxml2-dev libxslt-dev g++ gcc gdb libcap-dev

else
    # Print the error and explain how to install the dependencies manually
    echo "Could not find a package manager to install dependencies. Please install the following dependencies manually:"
    echo "$_PYTHON $_PYTHON-dev git python3-venv libxml2-dev libxslt-dev g++ gcc"
    echo "Then run the following command to install the sub-modules:"
    echo "make setup-submodules"
    echo "Then run the following command to install pip environment:"
    echo "make setup-venv"
    
    exit 1

fi
