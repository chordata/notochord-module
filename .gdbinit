set history save on

# from https://stackoverflow.com/questions/20380204/how-to-load-multiple-symbol-files-in-gdb
define add-symbol-file-auto
  # Parse .text address to temp file
  shell echo set \$text_address=$(readelf -WS $arg0 | grep .text | awk '{ print "0x"$5 }') >/tmp/temp_gdb_text_address.txt

  # Source .text address
  source /tmp/temp_gdb_text_address.txt

  #  Clean tempfile
  shell rm -f /tmp/temp_gdb_text_address.txt

  # Load symbol table
  add-symbol-file $arg0 $text_address
end

#load notochord library symbols on startup so breakpoints 
#can be added before running the program
add-symbol-file-auto dist/notochord/libchordata.so

set debuginfod enabled off
